//
//  LoginViewController.m
//  Thomson
//
//  Created by 何助金 on 10/18/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "LoginViewController.h"
#import "ForgotPasswordViewController.h"
#import "RegisterViewController.h"
#import "HomeViewController.h"
#define kLeftMark 15
@interface LoginViewController ()
{
    UITextField *_userName;
    UITextField *_password;
    UISwitch *_rememberSwitch;
}
@property (nonatomic, strong) UIScrollView *scrollview;
@end

@implementation LoginViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createViews];
    
}

- (void)createViews{
    self.scrollview = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    self.scrollview.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.scrollview];
    
    UIImageView *headImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 353*320/414)];
    headImageView.backgroundColor = KClearColor;
    headImageView.image = [UIImage imageNamed:@"headView"];
    [self.scrollview addSubview:headImageView];
    
    
    UILabel *title = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark, kScreenHeight - 350*kScale + 20, kScreenWidth - 2*kLeftMark, 30) backgroundColor:KClearColor text:NSLocalizedStringFromTable(@"LOGIN", LanguageFileName, nil) textColor:kBlackLightColor font:kHelveticaLightFont(16) textAlignment:NSTextAlignmentLeft lineBreakMode:NSLineBreakByWordWrapping numberOfLines:0];
    [self.scrollview addSubview:title];
    
    
    _userName = [[ObjectCTools shared] getACustomTextFiledFrame:CGRectMake(kLeftMark, title.bottom + 10, (kScreenWidth - 2*kLeftMark), 35)
                                                backgroundColor:[UIColor clearColor]
                                                    placeholder:NSLocalizedStringFromTable(@"UserName", LanguageFileName, nil)
                                                      textColor:kHexRGB(0x054f7a)
                                                           font:kHelveticaRegularFont(15)
                                                    borderStyle:UITextBorderStyleRoundedRect
                                                  textAlignment:NSTextAlignmentLeft
                                             accessibilityLabel:nil
                                             autocorrectionType:UITextAutocorrectionTypeDefault
                                                clearButtonMode:UITextFieldViewModeWhileEditing
                                                            tag:100
                                                 withIsPassword:NO];
    _userName.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, _userName.height)];
    _userName.leftViewMode = UITextFieldViewModeAlways;
    _userName.tintColor = [UIColor blackColor];
    [_scrollview addSubview:_userName];
    
    
    
    _password = [[ObjectCTools shared] getACustomTextFiledFrame:CGRectMake(kLeftMark, _userName.bottom + 10, (kScreenWidth - 2*kLeftMark), 35)
                                                backgroundColor:[UIColor clearColor]
                                                    placeholder:NSLocalizedStringFromTable(@"Password", LanguageFileName, nil)
                                                      textColor:kHexRGB(0x054f7a)
                                                           font:kHelveticaRegularFont(15)
                                                    borderStyle:UITextBorderStyleRoundedRect
                                                  textAlignment:NSTextAlignmentLeft
                                             accessibilityLabel:nil
                                             autocorrectionType:UITextAutocorrectionTypeDefault
                                                clearButtonMode:UITextFieldViewModeWhileEditing
                                                            tag:101
                                                 withIsPassword:YES];
    _password.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, _password.height)];
    _password.leftViewMode = UITextFieldViewModeAlways;
    _password.tintColor = [UIColor blackColor];
    [_scrollview addSubview:_password];
    
    //remember me
    
    _rememberSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(kLeftMark, _password.bottom +15, 40, 25)];
    _rememberSwitch.onTintColor = kBlueColor;
   
    [_rememberSwitch addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventValueChanged];
    
    if ([kUserDef boolForKey:@"kRemeberMe"]) {
        _rememberSwitch.on = YES;
        _userName.text = [kUserDef objectForKey:kUserName];
        _password.text = [kUserDef objectForKey:kPassword];
    }else
    {
        _rememberSwitch.on = NO;
    }
    
    [self.scrollview addSubview:_rememberSwitch];
    
    UILabel *rememberTitle = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(_rememberSwitch.right + 2 , _rememberSwitch.y , (kScreenWidth - 2*kLeftMark)*0.5 - _rememberSwitch.width - 2, 25) backgroundColor:KClearColor text:NSLocalizedStringFromTable(NSLocalizedStringFromTable(@"Remember Me", LanguageFileName, nil), LanguageFileName, nil) textColor:kBlackLightColor font:kHelveticaLightFont(14) textAlignment:NSTextAlignmentLeft lineBreakMode:NSLineBreakByWordWrapping numberOfLines:1];
    [rememberTitle sizeToFit];
    rememberTitle.centerY = _rememberSwitch.centerY;
    rememberTitle.contentMode = UIViewContentModeCenter;
    
    [self.scrollview addSubview:rememberTitle];

    
    UIButton *forgotButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake((kScreenWidth - 2*kLeftMark)*0.5 + kLeftMark, _rememberSwitch.y - 5, (kScreenWidth - 2*kLeftMark)*0.5, 35)
                                                                backgroudColor:KClearColor
                                                              titleNormalColor:kBlueColor
                                                         titleHighlightedColor:[UIColor grayColor]
                                                                         title:NSLocalizedStringFromTable(@"Forgot Password?", LanguageFileName, nil)
                                                                          font:kHelveticaLightFont(14)
                                                                  cornerRadius:0
                                                                   borderWidth:0
                                                                   borderColor:KClearColor.CGColor
                                                            accessibilityLabel:nil];
    [forgotButton addTarget: self action:@selector(forgotButton:) forControlEvents:UIControlEventTouchUpInside];
    forgotButton.centerY = _rememberSwitch.centerY;
    [forgotButton setContentMode:UIViewContentModeRight];
    [_scrollview addSubview:forgotButton];
    
    
    //login button
    UIButton *loginButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake(kLeftMark, _rememberSwitch.bottom + 15, (kScreenWidth - kLeftMark * 2), 45)
                                                                       backgroudColor:kBlueColor
                                                                     titleNormalColor:[UIColor whiteColor]
                                                                titleHighlightedColor:[UIColor grayColor]
                                                                                title:NSLocalizedStringFromTable(@"LOGIN", LanguageFileName, nil)
                                                                                 font:kHelveticaLightFont(20)
                                                                         cornerRadius:5
                                                                          borderWidth:0.5
                                                                          borderColor:kBlueColor.CGColor
                                                                   accessibilityLabel:nil];
    [loginButton addTarget: self action:@selector(loginButton:) forControlEvents:UIControlEventTouchUpInside];
    [_scrollview addSubview:loginButton];

    
    UIButton *registerButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake(kLeftMark,loginButton.bottom + 10, (kScreenWidth - 2*kLeftMark), 35)
                                                                       backgroudColor:KClearColor
                                                                     titleNormalColor:kBlueColor
                                                                titleHighlightedColor:[UIColor grayColor]                                      title:NSLocalizedStringFromTable(@"Don't have account? REGISTER", LanguageFileName, nil)
                                                                                       font:kHelveticaLightFont(16)
                                                                                       cornerRadius:0
                                                                                       borderWidth:0
                                                                                       borderColor:KClearColor.CGColor
                                                                                       accessibilityLabel:nil];
    [registerButton addTarget: self action:@selector(registerButton:) forControlEvents:UIControlEventTouchUpInside];
                              
    NSString *string = [NSString stringWithFormat:@"%@%@",NSLocalizedStringFromTable(@"Don't have account? ", LanguageFileName, nil),NSLocalizedStringFromTable(@"REGISTER", LanguageFileName, nil)];
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:string];
    NSRange range = [string rangeOfString:NSLocalizedStringFromTable(@"REGISTER", LanguageFileName, nil)];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:kBlueColor range:range];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:kBlackLightColor range:NSMakeRange(0, range.location)];


    [registerButton setAttributedTitle:attributeStr forState:UIControlStateNormal];
    
    NSMutableAttributedString *attributeStrH = [[NSMutableAttributedString alloc] initWithString:string];
    [attributeStrH addAttribute:NSForegroundColorAttributeName value:kBlackLightColor range:NSMakeRange(0, string.length)];
    [registerButton setAttributedTitle:attributeStrH forState:UIControlStateHighlighted];

    [registerButton setContentMode:UIViewContentModeLeft];
    registerButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    [registerButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -60, 0, 0)];
    
    [_scrollview addSubview:registerButton];
    
    
    if (registerButton.bottom + 40 > kScreenHeight) {
        _scrollview.contentSize = CGSizeMake(kScreenWidth, registerButton.bottom + 40);
    }
    
}

#pragma mark login
- (void)loginButton:(UIButton *)sender
{
    BOOL ok = YES;
    if (_userName.text.length<1) {
        [_userName shake];
        ok = NO;
    }
    
    if (_password.text.length<6) {
        [_password shake];
        ok = NO;
    }
    
    if (ok) {
        HomeViewController *homeVC = [[HomeViewController alloc]init];
        [self.navigationController pushViewController:homeVC animated:YES];
        
        if (_rememberSwitch.on) {
            [kUserDef setObject:_userName.text forKey:kUserName];
            [kUserDef setObject:_password.text forKey:kPassword];
            [kUserDef synchronize];
        }
        
    }else
    {
        return;
    }

}
#pragma mark remember me

- (void)changeValue:(UISwitch *)sender
{
    if (sender.on) {
        [kUserDef setBool:YES forKey:@"kRemeberMe"];
    }else
    {
        [kUserDef setBool:NO forKey:@"kRemeberMe"];
    }
    
    [kUserDef synchronize];
}

#pragma mark forgot password

- (void)forgotButton:(UIButton *)sender
{
    ForgotPasswordViewController *forgotVC = [[ForgotPasswordViewController alloc]init];
    [self.navigationController pushViewController:forgotVC animated:YES];
}

#pragma mark register

- (void)registerButton:(UIButton *)sender
{
    RegisterViewController *registerVC = [[RegisterViewController alloc]init];
    [self.navigationController pushViewController:registerVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
