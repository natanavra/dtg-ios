//
//  AutoAnswerViewController.m
//  Thomson
//
//  Created by 何助金 on 10/22/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "AutoAnswerViewController.h"
#import "GetMNumberTableViewCell.h"
#import "HistoryTableViewCell.h"
#import "AutoAnswerModel.h"
#define kAuto @"Auto"
@interface AutoAnswerViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UIView *_setBgView;
    UIView *_getBgView;
    UITableView *_tableView;
    NSMutableArray *_dataArray;
}


@end

@implementation AutoAnswerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _dataArray = [NSMutableArray array];
    
    _setBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 )];
    _setBgView.backgroundColor = kWhiteColor;
    _setBgView.hidden = NO;
    [self.view addSubview:_setBgView];
    
    _getBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 - 64)];
    _getBgView.backgroundColor = kWhiteColor;
    _getBgView.hidden = YES;
    [self.view addSubview:_getBgView];

    UILabel *infoLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,0, kScreenWidth/2 - kLeftMark , 35)
                                                     backgroundColor:[UIColor clearColor]
                                                                text:NSLocalizedStringFromTable(@"Auto-Answer", LanguageFileName, nil)
                                                           textColor:kBlackLightColor
                                                                font:kHelveticaLightFont(16)
                                                       textAlignment:NSTextAlignmentLeft
                                                       lineBreakMode:NSLineBreakByWordWrapping
                                                       numberOfLines:0];
    
    infoLabel.adjustsFontSizeToFitWidth = YES;
    [_setBgView addSubview:infoLabel];
    
    UILabel *enableLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kScreenWidth/2.0,0, kScreenWidth/2 - kLeftMark - 55 , 35)
                                                       backgroundColor:[UIColor clearColor]
                                                                  text:NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil)
                                                             textColor:kBlackLightColor
                                                                  font:kHelveticaLightFont(13)
                                                         textAlignment:NSTextAlignmentRight
                                                         lineBreakMode:NSLineBreakByWordWrapping
                                                         numberOfLines:0];
    
    enableLabel.adjustsFontSizeToFitWidth = YES;
    [_setBgView addSubview:enableLabel];
    
    
    UISwitch *enableSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(kScreenWidth - kLeftMark - 50,5, 50 , 20)];
    enableSwitch.onTintColor = kBlueColor;
    
    [enableSwitch addTarget:self action:@selector(enableSwitch:) forControlEvents:UIControlEventValueChanged];
    enableSwitch.on = [kUserDef boolForKey:kAuto];
    enableSwitch.tag = 300;
    [_setBgView addSubview:enableSwitch];
    
    enableSwitch.centerY = infoLabel.centerY;
    enableLabel.centerY = infoLabel.centerY;
    [self addTableView];
}


- (void) addTableView
{
    _tableView = [[UITableView alloc] initWithFrame:_getBgView.bounds style:UITableViewStylePlain];
    
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    _tableView.separatorColor = [UIColor grayColor];
    [_getBgView addSubview:_tableView];
}
#pragma mark -tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1 + _dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0){
        
        GetMNumberTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GetMNumberTableViewCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"GetMNumberTableViewCell" owner:self options:nil] lastObject];
        }
        
        cell.mlable.text = NSLocalizedStringFromTable(@"Update History", LanguageFileName, nil);
        cell.numberLable.text = nil;
        return cell;
        
    }else
    {
        HistoryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"HistoryTableViewCell" owner:self options:nil] lastObject];
            UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2 *15, 1.0)];
            [cell.contentView addSubview:line];
            line.y = 29;
            line.x = 15;
        }
        
        if (_dataArray.count) {
            
            AutoAnswerModel *model = _dataArray[indexPath.row - 1];
            cell.mLable.text = model.name;
            cell.dateLabel.text = [DateHelper timeSPToTimeStringWithString:model.updateTime];
            
        }else {
            cell.mLable.text = @"";
            cell.dateLabel.text = @"";
        }
        return cell;
    }
    
}



- (void)enableSwitch:(UISwitch *)sender
{
    if (sender.on) {
        [[ObjectCTools shared] showAlertViewAndDissmissAutomatic:NSLocalizedStringFromTable(@"Auto-Answer", LanguageFileName, nil) andMessage:NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil) withDissmissTime:KDissmissTime withDelegate:nil withAction:nil];
    }
    [kUserDef setBool:sender.on forKey:kAuto];
    [kUserDef synchronize];
}

#pragma mark 切换 SET GET

- (void)setGetSegmet:(UISegmentedControl *)sender
{
    [super setGetSegmet:sender];
    if (sender.selectedSegmentIndex == 0) {
        _setBgView.hidden = NO;
        _getBgView.hidden = YES;
    }else
    {
        _setBgView.hidden = YES;
        _getBgView.hidden = NO;
        
        LKDBHelper* globalHelper = [AutoAnswerModel getUsingLKDBHelper];
        
        //异步 asynchronous
        [globalHelper search:[AutoAnswerModel class] where:nil orderBy:@"updateTime desc" offset:0 count:50 callback:^(NSMutableArray *array) {
            for (AutoAnswerModel *model in array) {
                NSLog(@"%@ -- %@",model.name,model.updateTime);
            }
            _dataArray.count?([_dataArray removeAllObjects]):nil;
            [_dataArray addObjectsFromArray:array];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_tableView reloadData];
            });
            
            
        }];
        
    }
    

}

- (void)checkButton:(UIButton *)sender
{
    [super checkButton:sender];

    UISwitch *enableSwitch = (UISwitch *)[self.view viewWithTag:300];
    
    if (self.setGetSegmet.selectedSegmentIndex == 0) {
        NSString *body = [NSString stringWithFormat:@"SET#SWECI#%d#",enableSwitch.on];
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Set Auto-Answer", LanguageFileName, nil) body:body withDelegate:self];
//        #warning Test to DB
//        UISwitch *enableSwitch = (UISwitch *)[self.view viewWithTag:300];
//
//        AutoAnswerModel *model = [[AutoAnswerModel alloc]init];
//        model.name = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTable(@"Auto-Answer", LanguageFileName, nil),enableSwitch.on ? NSLocalizedStringFromTable(@"Enabled", LanguageFileName, nil):NSLocalizedStringFromTable(@"Disabled", LanguageFileName, nil)];
//        model.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
//        [model saveToDB];
//        #warning Test To DB
    }else{
        NSString *body = [NSString stringWithFormat:@"GET#SWECI#"];
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Get Auto-Answer", LanguageFileName, nil) body:body withDelegate:self];
    }
    
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    switch (result) {
        case MessageComposeResultSent:{
            //信息传送成功
            NSLog(@"信息传送成功");
            UISwitch *enableSwitch = (UISwitch *)[self.view viewWithTag:300];
            
            AutoAnswerModel *model = [[AutoAnswerModel alloc]init];
            model.name = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTable(@"Auto-Answer", LanguageFileName, nil),enableSwitch.on ? NSLocalizedStringFromTable(@"Enabled", LanguageFileName, nil):NSLocalizedStringFromTable(@"Disabled", LanguageFileName, nil)];
            model.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
            [model saveToDB];

            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case MessageComposeResultFailed:
            //信息传送失败
            NSLog(@"信息传送失败");
            [self.navigationController popViewControllerAnimated:YES];
            
            break;
        case MessageComposeResultCancelled:
            //信息被用户取消传送
            NSLog(@"信息被用户取消传送");
            break;
        default:
            break;
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
