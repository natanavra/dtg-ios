//
//  ThomsonConstant.h
//  Thomson
//
//  Created by 何助金 on 10/18/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#ifndef ThomsonConstant_h
#define ThomsonConstant_h


#define kLan @"Lan"
//本地化语言设置 使用 NSLocalizedStringFromTable
#define KLanguageFileName @"LanguageFileName"

//导航栏背景颜色值
#define kNavigationBarColor kHexRGB(0x010103)
//导航栏字体颜色值
#define kNavigationBarTitleColor kHexRGB(0xFFFFFF)

#define KClearColor [UIColor clearColor]

#define kWhiteColor kHexRGB(0xFFFFFF)

#define kBlackColor kHexRGB(0x000000)

#define kBlackLightColor kHexRGB(0x3E3F43)

#define kBlueColor kHexRGB(0x162982)

#define kGreenColor kHexRGB(0x14C8C1)
//分割线
#define kGrayColor kHexRGB(0xC1C1C1)
//HeadView BG
#define kGrayDarkColor kHexRGB(0x959595)

#define kGrayLightColor kHexRGB(0xF5F5F5)

#define kHelveticaRegularFont(kSize) [UIFont fontWithName:@"Helvetica" size:kSize] //正常笔画
#define kHelveticaBoldFont(kSize)    [UIFont fontWithName:@"Helvetica Bold" size:kSize] //加粗
#define kHelveticaLightFont(kSize)   [UIFont fontWithName:@"Helvetica Light" size:kSize] //特细笔画

#define kNavigationTitleFont   kHelveticaBoldFont(19)//导航条标题字体大小
#define kNavigationBarItemFont kHelveticaLightFont(17)//导航栏button 字体大小

#define KGoogleAppKey @"AIzaSyCmE14jD2EJUCt8duTNbt02FJ268ZBOdv8"//@"AIzaSyB7JqsjZw0aWvr_jNhOvbUC6QuoSW7eDWA" AIzaSyAgWVbkqrnkJt-rBRHO9I6aI9P2dGRIvjU
#define kUserName @"userName"
#define kPassword @"password"
#define kEmail    @"email"
#define kHadAccount @"HadAccount"
#define kPhoneNumeber @"13246138837"
#define kSendNumber   @"sendNumber"
#define kSendPassword @"sendPassword"
#define kSendPhoneName @"sendPhoneName"


#define kLeftMark 20
#define KDissmissTime 1.5



#endif /* ThomsonConstant_h */
