//
//  SOSMessageTableViewCell.h
//  Thomson
//
//  Created by 何助金 on 10/24/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

@class SOSMessageTableViewCell;
@protocol SOSMessageTableViewCellDelegate <NSObject>

- (void)SOSMessageTableViewCell:(SOSMessageTableViewCell *)cell withSelectedArray:(NSMutableArray *)selectedArray;
- (void)SOSMessageTableViewCellUpdateMessage:(SOSMessageTableViewCell *)cell  withSelectedArray:(NSMutableArray *)selectedArray;
;



@end
#import <UIKit/UIKit.h>

@interface SOSMessageTableViewCell : UITableViewCell<UITextFieldDelegate>
@property (nonatomic, weak) id <SOSMessageTableViewCellDelegate> delegate;
@property (nonatomic, assign) BOOL isEdit;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) NSMutableArray *selectedArray;
@property (weak, nonatomic) IBOutlet UILabel *messageTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UITextField *messageField;


- (IBAction)editButtonPress:(UIButton *)sender;

@end
