//
//  HelpViewController.m
//  Thomson
//
//  Created by 何助金 on 10/22/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "HelpViewController.h"
#import "HelpTableViewCell.h"
#import "HelpModel.h"
@interface HelpViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    NSMutableArray *_dataArray;
}
@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _dataArray = [NSMutableArray array];
    [self _initData];
    
    self.setGetSegmet.hidden = YES;
    
    UIImageView *lineView = (UIImageView *)[self.view viewWithTag:999];
    lineView.y -=30;
    [self addTableView];
    
}


- (void)_initData
{
    HelpModel *mode0 = [[HelpModel alloc]init];
    mode0.title = NSLocalizedStringFromTable(@"SOS Number", LanguageFileName, nil);
    mode0.detailTitle = NSLocalizedStringFromTable(@"SOS Number help info coming soon SOS Number help info coming soon SOS Number help info coming soon", LanguageFileName, nil);
    mode0.isOpen = NO;
    [_dataArray addObject:mode0];
    
    HelpModel *mode1 = [[HelpModel alloc]init];
    mode1.title = NSLocalizedStringFromTable(@"SOS Message", LanguageFileName, nil);
    mode1.detailTitle = NSLocalizedStringFromTable(@"SOS Message help info coming soon", LanguageFileName, nil);
    mode1.isOpen = NO;
    [_dataArray addObject:mode1];

    HelpModel *mode2 = [[HelpModel alloc]init];
    mode2.title = NSLocalizedStringFromTable(@"M1/M2 number", LanguageFileName, nil);
    mode2.detailTitle = NSLocalizedStringFromTable(@"help info coming soon", LanguageFileName, nil);
    mode2.isOpen = NO;
    [_dataArray addObject:mode2];

    HelpModel *mode3 = [[HelpModel alloc]init];
    mode3.title = NSLocalizedStringFromTable(@"GPS Setting", LanguageFileName, nil);
    mode3.detailTitle = NSLocalizedStringFromTable(@"help info coming soon", LanguageFileName, nil);
    mode3.isOpen = NO;
    [_dataArray addObject:mode3];

    HelpModel *mode4 = [[HelpModel alloc]init];
    mode4.title = NSLocalizedStringFromTable(@"Fall detect", LanguageFileName, nil);
    mode4.detailTitle = NSLocalizedStringFromTable(@"help info coming soon", LanguageFileName, nil);
    mode4.isOpen = NO;
    [_dataArray addObject:mode4];

    HelpModel *mode5 = [[HelpModel alloc]init];
    mode5.title = NSLocalizedStringFromTable(@"Tone & Profile", LanguageFileName, nil);
    mode5.detailTitle = NSLocalizedStringFromTable(@"help info coming soon", LanguageFileName, nil);
    mode5.isOpen = NO;
    [_dataArray addObject:mode5];

    HelpModel *mode6 = [[HelpModel alloc]init];
    mode6.title = NSLocalizedStringFromTable(@"Low Battery number", LanguageFileName, nil);
    mode6.detailTitle = NSLocalizedStringFromTable(@"help info coming soon", LanguageFileName, nil);
    mode6.isOpen = NO;
    [_dataArray addObject:mode6];

    HelpModel *mode7 = [[HelpModel alloc]init];
    mode7.title = NSLocalizedStringFromTable(@"Auto-Answer", LanguageFileName, nil);
    mode7.detailTitle = NSLocalizedStringFromTable(@"help info coming soon", LanguageFileName, nil);
    mode7.isOpen = NO;
    [_dataArray addObject:mode7];

    HelpModel *mode8 = [[HelpModel alloc]init];
    mode8.title = NSLocalizedStringFromTable(@"Language", LanguageFileName, nil);
    mode8.detailTitle = NSLocalizedStringFromTable(@"help info coming soon", LanguageFileName, nil);
    mode8.isOpen = NO;
    [_dataArray addObject:mode8];

    HelpModel *mode9 = [[HelpModel alloc]init];
    mode9.title = NSLocalizedStringFromTable(@"Alerts Notification", LanguageFileName, nil);
    mode9.detailTitle = NSLocalizedStringFromTable(@"help info coming soon", LanguageFileName, nil);
    mode9.isOpen = NO;
    [_dataArray addObject:mode9];

    for (HelpModel *model in _dataArray) {
   
        CGRect temp = [model.detailTitle boundingRectWithSize:CGSizeMake(kScreenWidth - 60, 1000) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : kHelveticaLightFont(12)} context:nil];
      
        if (temp.size.height > 15) {
            model.detailHeight = temp.size.height + 5;
        }else{
            if (model.detailTitle.length > 2) {
                model.detailHeight = temp.size.height;
            }else
            {
                model.detailHeight = 0;
            }

        }
        
    }

}
- (void) addTableView
{
    UIImageView *lineView = (UIImageView *)[self.view viewWithTag:999];

    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,lineView.bottom ,kScreenWidth, kScreenHeight- 64 - lineView.bottom) style:UITableViewStylePlain];
    
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    _tableView.separatorColor = [UIColor grayColor];
    [self.view addSubview:_tableView];
}
#pragma mark -tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    HelpModel *model = _dataArray[indexPath.row];
    if (model.isOpen) {
        return 50 + model.detailHeight;
    }else
    {
        return 50;
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HelpTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[HelpTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    HelpModel *model = _dataArray[indexPath.row];

    cell.textLabel.text = model.title;
    if (model.isOpen) {
        cell.detailTextLabel.text = model.detailTitle;
    }else
    {
        cell.detailTextLabel.text = nil;
    }
    cell.isOpen = model.isOpen;

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    HelpModel *model = _dataArray[indexPath.row];
    model.isOpen = !model.isOpen;
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)checkButton:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
