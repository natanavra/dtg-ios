//
//  AddDeviceViewController.m
//  Thomson
//
//  Created by 何助金 on 10/19/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "AddDeviceViewController.h"
#import "AddDeviceTableViewCell.h"
#import "KTSContactsManager.h"
#import "ContactModel.h"
//#import <ContactsUI/ContactsUI.h>

@interface AddDeviceViewController ()<UITableViewDataSource,UITableViewDelegate,KTSContactsManagerDelegate,AddDeviceTableViewCellDelegate,UIAlertViewDelegate,UISearchDisplayDelegate,UISearchBarDelegate>
{
    /**
     *  创建CNContactStore对象,用与获取和保存通讯录信息
     */
//    CNContactStore *contactStore;
}
@property (strong, nonatomic) KTSContactsManager *contactsManager;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *modelArray;

@property (nonatomic,strong)NSMutableArray *resultsArray;
@property (nonatomic,strong)UISearchBar *searchBar;
@property (nonatomic,strong)UISearchDisplayController *searchDisplayVC;
@end

@implementation AddDeviceViewController

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_searchBar resignFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _modelArray = [NSMutableArray array];
    [self addTableView];
    self.contactsManager = [KTSContactsManager sharedManager];
    self.contactsManager.delegate = self;
    self.contactsManager.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"firstName" ascending:YES] ];
//    if (kIOSVersions >= 9.0) {
//        [self getAllContactsByContact];
//        
//    }else
    {
        [self loadData];
    }
    [self addSearchView];

}

- (void)addSearchView
{
    _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 44)];
    _searchBar.placeholder = NSLocalizedStringFromTable(@"Search", LanguageFileName, nil);
//    [_searchBar setBackgroundImage:[UIImage imageWithColor:kRGB(44, 167, 89)]];
    _searchBar.delegate = self;
    
    _searchDisplayVC = [[UISearchDisplayController alloc] initWithSearchBar:_searchBar contentsController:self];
    _searchDisplayVC.searchResultsDataSource = self;
    _searchDisplayVC.searchResultsDelegate = self;
    _searchDisplayVC.delegate = self;
    _searchDisplayVC.searchResultsTableView.tableFooterView = [UIView new];
    _searchDisplayVC.searchResultsTableView.height = _tableView.height;
    //    [LineView addLineViewAtTopForView:_searchBar withColor:[UIColor whiteColor]];
    [self.view addSubview:_searchBar];
    //    _listTableView.tableHeaderView = _searchBar;
    
}


/**
 *  @author Jason He, 15-11-12
 *
 *  @brief  For IOS 9
 */
//- (void)getAllContactsByContact {
//    /**
//     * 首次访问通讯录会调用
//     */
//    contactStore = [[CNContactStore alloc] init];
//    
//    if ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts] == CNAuthorizationStatusNotDetermined )
//    {
//        [contactStore requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error)
//         {
//             if (error) return;
//             if (granted)
//             {
//                 NSLog(@"授权访问通讯录");
//                 [self fetchContactWithContactStore:contactStore];
//             }
//             else
//             {
//                 NSLog(@"拒绝访问通讯录");
//             }
//         }];
//    }
//    else
//    {
//        [self fetchContactWithContactStore:contactStore];
//    }
//}
//
//- (void)fetchContactWithContactStore:(CNContactStore *)cnContactStore {
//    
//    [_modelArray removeAllObjects];
//    /**
//     *  有权限访问
//     */
//    if ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts] == CNAuthorizationStatusAuthorized)
//    {
//        NSError *error = nil;
//        
//        /**
//         *  关键:创建数组,必须遵守CNKeyDescriptor协议,放入相应的字符串常量来获取对应的联系人信息(用户的信息都有对应的key，选取指定的key获取对应信息)
//         */
//        NSArray <id<CNKeyDescriptor>> *keysToFetch = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey,CNContactImageDataKey];
//        
//        
//        /**
//         * 创建获取联系人的请求
//         */
//        CNContactFetchRequest *fetchRequest = [[CNContactFetchRequest alloc] initWithKeysToFetch:keysToFetch];
//        
//        
//        /**
//         *  遍历查询通讯录所有联系人
//         */
//        [contactStore enumerateContactsWithFetchRequest:fetchRequest error:&error usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop)
//         {
//             if (!error)
//             {
//                 NSLog(@"familyName = %@", contact.familyName);//姓
//                 NSLog(@"givenName = %@", contact.givenName);//名字
//                 NSLog(@"phoneNumber = %@", ((CNPhoneNumber *)(contact.phoneNumbers.lastObject.value)).stringValue);//电话
//                 
//                     ContactModel *contactModel = [[ContactModel alloc]init];
//                     NSString *firstName = contact.familyName;
//                     contactModel.name = [firstName stringByAppendingString:[NSString stringWithFormat:@" %@", contact.givenName]];
//                     contactModel.password = @"9999";
//                     contactModel.isOpen = NO;
//                     contactModel.number = ((CNPhoneNumber *)(contact.phoneNumbers.lastObject.value)).stringValue;
//                     
//                     if (![NSString isNilOrEmpty: contactModel.number ]) {
//                         [_modelArray addObject:contactModel];
//                     }
//                 
//             }
//             else
//             {
//                 NSLog(@"error:%@", error.localizedDescription);
//             }
//         }];
//        [self.tableView reloadData];
//
//    }
//    else
//    {
//        NSLog(@"拒绝访问通讯录");
//    }
//}


/**
 *  @author Jason He, 15-11-12
 *
 *  @brief  For IOS 8
 */
- (void)loadData
{
//    //新建一个通讯录类
//    ABAddressBookRef addressBooks = nil;
//    
//    if ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0)
//        
//    {
//        addressBooks =  ABAddressBookCreateWithOptions(NULL, NULL);
//        
//        //获取通讯录权限
//        
//        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
//        
//        ABAddressBookRequestAccessWithCompletion(addressBooks, ^(bool granted, CFErrorRef error){
//            dispatch_semaphore_signal(sema);
//        });
//        
//        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
//        
//    }
//    
//    else
//        
//    {
//        addressBooks = ABAddressBookCreate();
//        
//    }
//    
//    //获取通讯录中的所有人
//    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBooks);
//    
//    //通讯录中人数
//    CFIndex nPeople = ABAddressBookGetPersonCount(addressBooks);
//    
//    //循环，获取每个人的个人信息
//    for (NSInteger i = 0; i < nPeople; i++)
//    {
//        //新建一个addressBook model类
//        ContactModel *addressBook = [[ContactModel alloc] init];
//        //获取个人
//        ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
//        //获取个人名字
//        CFTypeRef abName = ABRecordCopyValue(person, kABPersonFirstNameProperty);
//        CFTypeRef abLastName = ABRecordCopyValue(person, kABPersonLastNameProperty);
//        CFStringRef abFullName = ABRecordCopyCompositeName(person);
//        NSString *nameString = (__bridge NSString *)abName;
//        NSString *lastNameString = (__bridge NSString *)abLastName;
//        
//        if ((__bridge id)abFullName != nil) {
//            nameString = (__bridge NSString *)abFullName;
//        } else {
//            if ((__bridge id)abLastName != nil)
//            {
//                nameString = [NSString stringWithFormat:@"%@ %@", nameString, lastNameString];
//            }
//        }
//        addressBook.name = nameString;
//        
//        ABPropertyID multiProperties[] = {
//            kABPersonPhoneProperty,
//            kABPersonEmailProperty
//        };
//        NSInteger multiPropertiesTotal = sizeof(multiProperties) / sizeof(ABPropertyID);
//        for (NSInteger j = 0; j < multiPropertiesTotal; j++) {
//            ABPropertyID property = multiProperties[j];
//            ABMultiValueRef valuesRef = ABRecordCopyValue(person, property);
//            NSInteger valuesCount = 0;
//            if (valuesRef != nil) valuesCount = ABMultiValueGetCount(valuesRef);
//            
//            if (valuesCount == 0) {
//                CFRelease(valuesRef);
//                continue;
//            }
//            //获取电话号码和email
//            for (NSInteger k = 0; k < valuesCount; k++) {
//                CFTypeRef value = ABMultiValueCopyValueAtIndex(valuesRef, k);
//                switch (j) {
//                    case 0: {// Phone number
//                        addressBook.number = (__bridge NSString*)value;
//                        addressBook.password = @"9999";
//                        addressBook.isOpen = NO;
//
//                        break;
//                    }
//                    case 1: {// Email
////                        addressBook.email = (__bridge NSString*)value;
//                        break;
//                    }
//                }
//                CFRelease(value);
//            }
//            CFRelease(valuesRef);
//        }
//        //将个人信息添加到数组中，循环完成后addressBookTemp中包含所有联系人的信息
//        [_modelArray addObject:addressBook];
//        
//        if (abName) CFRelease(abName);
//        if (abLastName) CFRelease(abLastName);
//        if (abFullName) CFRelease(abFullName);
//        [self.tableView reloadData];
//
//    }
//
//    
//    
//    //
//    return;
    [self.contactsManager importContacts:^(NSArray *contacts)
     {
         if (contacts.count) {
             for (NSDictionary *contact in contacts) {
                 ContactModel *contactModel = [[ContactModel alloc]init];
                 NSString *firstName = contact[@"firstName"];
                 contactModel.name = [firstName stringByAppendingString:[NSString stringWithFormat:@" %@", contact[@"lastName"]]];
                 contactModel.password = @"";
                 contactModel.isOpen = NO;
                 NSArray *phones = contact[@"phones"];
                 
                 if ([phones count] > 0) {
                     NSDictionary *phoneItem = phones[0];
                     contactModel.number = phoneItem[@"value"];
                     
                     if (![NSString isNilOrEmpty: contactModel.number ]) {
                         [_modelArray addObject:contactModel];
                     }

                 }
             }
         }
         
         [self.tableView reloadData];
//         NSLog(@"contacts: %@",contacts);
     }];
}

-(BOOL)filterToContact:(NSDictionary *)contact
{
    return YES;
    return ![contact[@"company"] isEqualToString:@""];
}


- (void) addTableView
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,44,kScreenWidth, kScreenHeight- 64 -44) style:UITableViewStylePlain];
    
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.separatorColor = kGrayColor;
    [self.view addSubview:_tableView];
}
#pragma mark -tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //显示搜索结果，同时展开确定搜索结果的行数
    if (_tableView != tableView)
    {
        if (_resultsArray == nil)
        {
            _resultsArray = [[NSMutableArray alloc]initWithCapacity:0];
        }
        else
        {
            [_resultsArray removeAllObjects];//清空
        }
        //根据搜索关键字去匹配 searchWord
        NSString *searchWord = _searchBar.text;
        for ( ContactModel *contactModel  in _modelArray)
        {
//            for (NSString *name in subarray)
//            {
                NSRange foundRang = [contactModel.name rangeOfString:searchWord options:NSCaseInsensitiveSearch];// 不区分大小写
                if (foundRang.length == 0)
                {
                    continue;
                }
                else
                {
                    [_resultsArray addObject:contactModel];
                }
//            }
        }
//        返回结果的行数
        return [_resultsArray count];
        
    }

    
    return self.modelArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (tableView == _tableView) {
        ContactModel *contact = [self.modelArray objectAtIndex:indexPath.row];
        
        if (contact.isOpen) {
            return 120;
        }else{
            return 50;
        }
    }else
    {
        ContactModel *contact = [_resultsArray objectAtIndex:indexPath.row];
        
        if (contact.isOpen) {
            return 120;
        }else{
            return 50;
        }
    }
    

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tableView) {
        AddDeviceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil) {
            cell = [[AddDeviceTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
            cell.textLabel.textColor = kBlueColor;
        }
        
        ContactModel *contact = [self.modelArray objectAtIndex:indexPath.row];
        cell.textLabel.text = contact.name;
        cell.phoneNumber = contact.number;
        cell.isOpen = contact.isOpen;
        cell.passwordField.text = nil;
        cell.delegate = self;
        cell.indexPath = indexPath;
        return cell;

    }else {
        AddDeviceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil) {
            cell = [[AddDeviceTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
            cell.textLabel.textColor = kBlueColor;
        }
        
        ContactModel *contact = [_resultsArray objectAtIndex:indexPath.row];
        cell.textLabel.text = contact.name;
        cell.phoneNumber = contact.number;
        cell.isOpen = contact.isOpen;
        cell.passwordField.text = nil;
        cell.delegate = self;
        cell.indexPath = indexPath;
        return cell;


    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_tableView == tableView) {
        ContactModel *contact = [self.modelArray objectAtIndex:indexPath.row];
        contact.isOpen = !contact.isOpen;
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
     
    }else
    {
        ContactModel *contact = [_resultsArray objectAtIndex:indexPath.row];
        contact.isOpen = !contact.isOpen;
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
   
}

//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
//{
//    NSMutableArray *array = [NSMutableArray array];
//    [array addObject:UITableViewIndexSearch];//搜索图标
//    for (int i = 'A'; i <= 'Z'; ++ i) {
//        NSString *string = [NSString stringWithFormat:@"%c",i];
//        [array addObject:string];
//    }
//    
//    return array;
//}

#pragma mark 索引号与段号相差1 返回段号

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (index == 0) {
        [tableView scrollRectToVisible:CGRectMake(0, tableView.y, kScreenWidth, tableView.height) animated:YES];
        return -1;
    }
    return index - 1;
}

//

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [_searchDisplayVC setActive:NO animated:YES];
    _searchDisplayVC.displaysSearchBarInNavigationBar = NO;
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    
    [_searchDisplayVC setActive:YES animated:YES];
    _searchDisplayVC.displaysSearchBarInNavigationBar = NO;
//    [searchBar setBackgroundImage:[UIImage imageWithColor:kRGB(44, 167, 89)]];
//    searchBar.backgroundColor = kRGB(44, 167, 89);
    
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    return YES;
}
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    return YES;
}
static bool isResultTable = NO;
- (void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView NS_DEPRECATED_IOS(3_0,8_0){
    isResultTable = YES;
}
- (void)searchDisplayController:(UISearchDisplayController *)controller willHideSearchResultsTableView:(UITableView *)tableView {
    isResultTable = NO;
}
//- (void)searchDisplayController:(UISearchDisplayController *)controller didHideSearchResultsTableView:(UITableView *)tableView NS_DEPRECATED_IOS(3_0,8_0);


static NSIndexPath *indexPath = nil;
- (void)addDeviceTableViewCell:(AddDeviceTableViewCell *)cell
{
    indexPath = cell.indexPath;
    
    NSString *message = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTable(@"Are you sure add", LanguageFileName, nil),cell.textLabel.text];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedStringFromTable(@"Add Device", LanguageFileName, nil) message:message delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"Cancel", LanguageFileName, nil) otherButtonTitles:NSLocalizedStringFromTable(@"Sure", LanguageFileName, nil), nil];
    [alert show];

   }


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
    }else
    {
        ContactModel *model = nil;
        AddDeviceTableViewCell *cell = nil;
        NSLog(@"isResultTable:%d",isResultTable);
        
        if (isResultTable) {
            model = _resultsArray[indexPath.row];
            //NSLog(@"model:%@",model.name);
            cell = [_searchDisplayVC.searchResultsTableView cellForRowAtIndexPath:indexPath];
        }else{
            model = _modelArray[indexPath.row];
            cell = [_tableView cellForRowAtIndexPath:indexPath];
        }

        //TODO: Temp!!
        if ([NSString isNilOrEmpty:cell.passwordField.text]) {
            //set new password 再add
            
            //NSString *body = [NSString stringWithFormat:@"SET%@#SMSPW#%@#",kMessagePassword,cell.passwordField.text];
            NSString *body = @"Go Conecto";
            [self showMessageView:@[cell.phoneNumber] title:NSLocalizedStringFromTable(@"Set new password", LanguageFileName, nil) body:body withDelegate:self];
            model.password = cell.passwordField.text;
            
            //[model saveToDB];
            
        }else
        {
            //直接add
            [model saveToDB];
            
        }

    }
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    switch (result) {
        case MessageComposeResultSent:{
            //信息传送成功
            NSLog(@"信息传送成功");
            //            [self.navigationController popViewControllerAnimated:YES];
//            ContactModel *model = _modelArray[indexPath.row];
//            [model saveToDB];
            
            ContactModel *model = nil;
            if (isResultTable) {
                model = _resultsArray[indexPath.row];
            }else{
                model = _modelArray[indexPath.row];
            }
            [model saveToDB];

        }
            
            break;
        case MessageComposeResultFailed:
            //信息传送失败
            NSLog(@"信息传送失败");
//            [self.navigationController popViewControllerAnimated:YES];
            
            break;
        case MessageComposeResultCancelled:
            //信息被用户取消传送
            NSLog(@"信息被用户取消传送");
            break;
        default:
            break;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addressBookDidChange {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
