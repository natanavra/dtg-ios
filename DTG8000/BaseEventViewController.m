//
//  BaseEventViewController.m
//  Thomson
//
//  Created by 何助金 on 10/20/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "BaseEventViewController.h"

@interface BaseEventViewController ()

@end

@implementation BaseEventViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
    
        self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(kLeftMark, kLeftMark, 40, 40)];
        self.icon.contentMode = UIViewContentModeCenter;
        self.icon.backgroundColor = KClearColor;
        
        if (self.iconName) {
            self.icon.image = [UIImage imageNamed:self.iconName];
        }
        
        [self.view addSubview:self.icon];
        
        self.titleLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(self.icon.right + 5, 0, kScreenWidth - self.icon.right + 2 - 80 , 30)
                                                      backgroundColor:[UIColor clearColor]
                                                                 text:self.titleString
                                                            textColor:kBlackLightColor
                                                                 font:kHelveticaLightFont(18)
                                                        textAlignment:NSTextAlignmentLeft
                                                        lineBreakMode:NSLineBreakByWordWrapping
                                                        numberOfLines:1];
        
        _titleLabel.adjustsFontSizeToFitWidth = YES;
        self.titleLabel.centerY = self.icon.centerY;
        
        [self.view addSubview: self.titleLabel];

    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.checkButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake(kScreenWidth - 70 , 0, 60, 35)
                                                                             backgroudColor:KClearColor
                                                                           titleNormalColor:[UIColor whiteColor]
                                                                      titleHighlightedColor:[UIColor grayColor]
                                                                                      title:nil
                                                                                       font:kHelveticaLightFont(11)
                                                                               cornerRadius:0
                                                                                borderWidth:0
                                                                                borderColor:KClearColor.CGColor
                                                                         accessibilityLabel:nil];
    [self.checkButton addTarget: self action:@selector(checkButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.checkButton setImage:[UIImage imageNamed:@"back_icon"] forState:UIControlStateNormal];
    [self.checkButton setImage:[UIImage imageNamed:@"back_icon_p"] forState:UIControlStateHighlighted];

    self.checkButton.centerY = self.icon.centerY;
    [self.view addSubview:self.checkButton];
    
    
    //
    self.setGetSegmet = [[UISegmentedControl alloc]initWithItems:@[NSLocalizedStringFromTable(@"Set", LanguageFileName, nil),NSLocalizedStringFromTable(@"Get", LanguageFileName, nil)]];
    self.setGetSegmet.frame = CGRectMake(0, self.icon.bottom + 5, 200, 30);
    self.setGetSegmet.centerX = self.view.centerX;
    [self.setGetSegmet setTintColor:kBlueColor];
    [self.setGetSegmet setSelectedSegmentIndex:0];
    [self.setGetSegmet addTarget:self action:@selector(setGetSegmet:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview: self.setGetSegmet];
    
    UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 10, kScreenWidth, 15)];
    lineView.tag = 999;
    lineView.image = [UIImage imageNamed:@"top_header_line"];
    [self.view addSubview: lineView];
    
    
}

- (void)setIconName:(NSString *)iconName
{
    _iconName = iconName;
    
    if (self.icon) {
        self.icon.image = [UIImage imageNamed:iconName];
    }

}

- (void)setTitleString:(NSString *)titleString{
    _titleString = titleString;
    if (self.titleLabel) {
        self.titleLabel.text = titleString;
    }
}
- (void)checkButton:(UIButton *)sender{
    [self.view endEditing:YES];

}

- (void)setGetSegmet:(UISegmentedControl *)sender
{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
