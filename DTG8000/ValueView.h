//
//  ValueView.h
//  Thomson
//
//  Created by 何助金 on 10/25/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ValueView : UIImageView
@property(nonatomic,strong) NSString *value;
@property(nonatomic,strong) UILabel *valueLabel;
@end
