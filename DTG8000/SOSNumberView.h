//
//  SOSNumberView.h
//  Thomson
//
//  Created by 何助金 on 10/21/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//
@class SOSNumberView;
@protocol SOSNumberViewDelegate <NSObject>

- (void)sosNumberView:(SOSNumberView *)sosNumber;

@end
#import <UIKit/UIKit.h>

@interface SOSNumberView : UIView <UITextFieldDelegate>
@property (nonatomic, weak) id <SOSNumberViewDelegate> delegate;
@property (nonatomic, assign) BOOL isEdit;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *numberLabel;
@property (nonatomic, strong) UITextField *numberField;
@property (nonatomic, strong) UIButton *editButton;
@end
