//
//  AddDeviceTableViewCell.h
//  Thomson
//
//  Created by 何助金 on 10/25/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

@class AddDeviceTableViewCell;
@protocol AddDeviceTableViewCellDelegate <NSObject>

- (void)addDeviceTableViewCell:(AddDeviceTableViewCell *)cell;

@end
#import <UIKit/UIKit.h>

@interface AddDeviceTableViewCell : UITableViewCell
@property (nonatomic,weak)id <AddDeviceTableViewCellDelegate>delegate;
@property (nonatomic,assign)BOOL isOpen;
@property (nonatomic,strong) NSString *phoneNumber;
@property (nonatomic,strong)UITextField *passwordField;
@property (nonatomic,strong)UIButton *addButton;
@property (nonatomic,strong)NSIndexPath *indexPath;
@end
