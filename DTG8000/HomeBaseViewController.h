//
//  HomeBaseViewController.h
//  Thomson
//
//  Created by 何助金 on 10/20/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "BaseViewController.h"

@interface HomeBaseViewController : BaseViewController

@property (nonatomic, strong) UIScrollView *scrollview;
@property (nonatomic, strong) UIView *headBaseView;
@property (nonatomic, strong) UILabel *titleLable;

- (void)createHeadViews;

@end
