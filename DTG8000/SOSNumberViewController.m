//
//  SOSNumberViewController.m
//  Thomson
//
//  Created by 何助金 on 10/22/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "SOSNumberViewController.h"
#import "SOSNumberView.h"
#import "GetMNumberTableViewCell.h"
#import "HistoryTableViewCell.h"
#import "SOSNumberModel.h"

@interface SOSNumberViewController ()<SOSNumberViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIScrollView *_setBgView;
    UIView *_getBgView;
    UITableView *_tableView;
    NSMutableArray *_dataArray;
}
@end

@implementation SOSNumberViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArray = [NSMutableArray array];
    
    _setBgView  = [[UIScrollView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 - 64)];
    _setBgView.backgroundColor = kWhiteColor;
    _setBgView.hidden = NO;
    [self.view addSubview:_setBgView];
    
    _getBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 - 64)];
    _getBgView.backgroundColor = kWhiteColor;
    _getBgView.hidden = YES;
    [self.view addSubview:_getBgView];
    
    
    UILabel *infoLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,0, kScreenWidth - 2*kLeftMark , 35)
                                                     backgroundColor:[UIColor clearColor]
                                                                text:NSLocalizedStringFromTable(@"Setup Energency Call SOS Number", LanguageFileName, nil)
                                                           textColor:kBlackLightColor
                                                                font:kHelveticaLightFont(16)
                                                       textAlignment:NSTextAlignmentLeft
                                                       lineBreakMode:NSLineBreakByWordWrapping
                                                       numberOfLines:0];
    
    infoLabel.adjustsFontSizeToFitWidth = YES;
    
    [_setBgView addSubview: infoLabel];
    
    NSString *phoneStr = NSLocalizedStringFromTable(@"PHONE", LanguageFileName, nil);
    
    int n = 0;
    for (int i = 0; i < 4; i ++) {
        
        for ( int j = 0 ; j < 2; j ++) {
            
            SOSNumberView *m  = [[SOSNumberView alloc]initWithFrame:CGRectMake(kLeftMark + j *(0.5*(kScreenWidth - 3*kLeftMark)+ kLeftMark), infoLabel.bottom + i *(100 + 20), 0.5*(kScreenWidth - 3*kLeftMark), 100)];
            m.delegate = self;
            m.tag = 200 + n ;
            m.titleLabel.text = [NSString stringWithFormat:@"%@ %d",phoneStr,n+1];
            
            
            
            NSString *keydata =[NSString stringWithFormat:@"%@%d",phoneStr,n+1];
            //保存临时数据
            if([kUserDef stringForKey:keydata])
            {
                 m.numberField.text =[kUserDef objectForKey:keydata];
            }
           
            NSLog(@"add userdef %@  %@",keydata,[kUserDef objectForKey:keydata]);

        
            [_setBgView addSubview:m];
             n++;
            if (n == 7 && m.bottom + 64 + 20 > _setBgView.height) {
                _setBgView.contentSize = CGSizeMake(kScreenWidth, m.bottom + 64 +20);
            }
        }
    }
    
    [self addTableView];
}


- (void) addTableView
{
    _tableView = [[UITableView alloc] initWithFrame:_getBgView.bounds style:UITableViewStylePlain];
    
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    _tableView.separatorColor = [UIColor grayColor];
    [_getBgView addSubview:_tableView];
}
#pragma mark -tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0){
        
        GetMNumberTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GetMNumberTableViewCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"GetMNumberTableViewCell" owner:self options:nil] lastObject];
        }
        
        cell.mlable.text = NSLocalizedStringFromTable(@"Update History", LanguageFileName, nil);
        cell.numberLable.text = nil;
        return cell;
        
    }else
    {
        HistoryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"HistoryTableViewCell" owner:self options:nil] lastObject];
            UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2 *15, 1.0)];
            [cell.contentView addSubview:line];
            line.y = 29;
            line.x = 15;
        }
        
        if (_dataArray.count) {
            
            SOSNumberModel *model = _dataArray[indexPath.row - 1];
            cell.mLable.text = model.name;
            cell.dateLabel.text = [DateHelper timeSPToTimeStringWithString:model.updateTime];
            
        }else {
            cell.mLable.text = @"";
            cell.dateLabel.text = @"";
        }
        return cell;
    }
    
}


static long setIndex = 0;

#pragma mark - 单发 SET
- (void)sosNumberView:(SOSNumberView *)sosNumber
{
    //send set number SET：SET_PW_#PHONE1#XXXXXX#

    setIndex = sosNumber.tag - 200 + 1;
    NSString *body = [NSString stringWithFormat:@"SET#PHONE%ld#%@#",setIndex,sosNumber.numberLabel.text];
    
    NSString *keydata =[NSString stringWithFormat:@"PHONE%ld",setIndex];
    //保存临时数据
    [kUserDef setObject:sosNumber.numberField.text forKey:keydata];
    [kUserDef synchronize];
    
    [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Setup M1 number", LanguageFileName, nil) body:body withDelegate:self];
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    switch (result) {
        case MessageComposeResultSent:
            //信息传送成功
            NSLog(@"信息传送成功");
            if (setIndex > 0) {
            // 单发 成功提示
                SOSNumberView *m = (SOSNumberView *)[self.view viewWithTag:200+setIndex-1];

                [[ObjectCTools shared] showAlertViewAndDissmissAutomatic:m.numberLabel.text andMessage:NSLocalizedStringFromTable(@"Added to the Emergency Call SOS numbers", LanguageFileName, nil) withDissmissTime:KDissmissTime withDelegate:nil withAction:nil];
               
                SOSNumberModel *model = [[SOSNumberModel alloc]init];
                model.name = [NSString stringWithFormat:@"%@:%@",m.titleLabel.text,m.numberField.text];
                model.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
                [model saveToDB];

                
            }else{
                
                for (int i = 0 ; i < 8; i ++) {
                    SOSNumberView *sosNumber = (SOSNumberView *)[self.view viewWithTag:200 + i];
                    if (sosNumber.numberField.text.length > 0) {
                        SOSNumberModel *model = [[SOSNumberModel alloc]init];
                        model.name = [NSString stringWithFormat:@"%@:%@",sosNumber.titleLabel.text,sosNumber.numberField.text];
                        model.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
                        [model saveToDB];
                    }
                    
                }
                // 多发返回
                [self.navigationController popViewControllerAnimated:YES];
                
            }
            
            break;
        case MessageComposeResultFailed:
            //信息传送失败
            NSLog(@"信息传送失败");
            if (setIndex == 0) {
                [self.navigationController popViewControllerAnimated:YES];
            }
            
            break;
        case MessageComposeResultCancelled:
            //信息被用户取消传送
            NSLog(@"信息被用户取消传送");
            break;
        default:
            break;
    }
    
}

- (void)setGetSegmet:(UISegmentedControl *)sender
{
    [super setGetSegmet:sender];
    setIndex  = 0;
    if (sender.selectedSegmentIndex == 0) {
        _setBgView.hidden = NO;
        _getBgView.hidden = YES;
    }else
    {
        _setBgView.hidden = YES;
        _getBgView.hidden = NO;
        
        LKDBHelper* globalHelper = [SOSNumberModel getUsingLKDBHelper];
        
        //异步 asynchronous
        [globalHelper search:[SOSNumberModel class] where:nil orderBy:@"updateTime desc" offset:0 count:50 callback:^(NSMutableArray *array) {
            for (SOSNumberModel *model in array) {
                NSLog(@"%@ -- %@",model.name,model.updateTime);
            }
            _dataArray.count?([_dataArray removeAllObjects]):nil;
            [_dataArray addObjectsFromArray:array];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_tableView reloadData];
            });
            
            
        }];

    }
}

#pragma mark 多发 SET /GET
- (void)checkButton:(UIButton *)sender
{
    [super checkButton:sender];
    
    if (self.setGetSegmet.selectedSegmentIndex == 0) {
        
        NSString *body = [NSString stringWithFormat:@"SET#"];
        
        for (int i = 0 ; i < 8; i ++) {
            SOSNumberView *sosNumber = (SOSNumberView *)[self.view viewWithTag:200 + i];
            if (sosNumber.numberField.text.length > 0) {
                NSString *s = [NSString stringWithFormat:@"PHONE%d#%@#",i+1,sosNumber.numberField.text];
                body = [body stringByAppendingString:s];
                
                NSString *keydata =[NSString stringWithFormat:@"PHONE%d",i+1];
                //保存临时数据
                [kUserDef setObject:sosNumber.numberField.text forKey:keydata];
                [kUserDef synchronize];
                
                NSLog(@"保存临时数据成功");
            }
            
        }
        
        if ([body rangeOfString:@"PHONE"].length) {
          [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Setup  SOS number", LanguageFileName, nil) body:body withDelegate:self];
        }

    }else
    {
        NSString *body = [NSString stringWithFormat:@"GET#PHONE1#PHONE2#PHONE3#PHONE4#PHONE5#PHONE6#PHONE7#PHONE8#"];
    
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Setup  SOS number", LanguageFileName, nil) body:body withDelegate:self];

    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
