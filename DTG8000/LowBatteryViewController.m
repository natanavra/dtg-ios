//
//  LowBatteryViewController.m
//  Thomson
//
//  Created by 何助金 on 10/22/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "LowBatteryViewController.h"
#import "GetMNumberTableViewCell.h"
#import "HistoryTableViewCell.h"
#import "LowBatteryModel.h"
#define kLow @"Low"
@interface LowBatteryViewController ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>

{
    UIView *_setBgView;
    UIView *_getBgView;
    UITableView *_tableView;
    NSMutableArray *_dataArray;
}
@end

@implementation LowBatteryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _dataArray = [NSMutableArray array];
    
    _setBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 -64)];
    _setBgView.backgroundColor = kWhiteColor;
    _setBgView.hidden = NO;
    [self.view addSubview:_setBgView];
    
    _getBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 - 64)];
    _getBgView.backgroundColor = kWhiteColor;
    _getBgView.hidden = YES;
    [self.view addSubview:_getBgView];

    
    UILabel *infoLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,0, kScreenWidth/2 - kLeftMark , 35)
                                                     backgroundColor:[UIColor clearColor]
                                                                text:NSLocalizedStringFromTable(@"Low Battery Warning", LanguageFileName, nil)
                                                           textColor:kBlackLightColor
                                                                font:kHelveticaLightFont(16)
                                                       textAlignment:NSTextAlignmentLeft
                                                       lineBreakMode:NSLineBreakByWordWrapping
                                                       numberOfLines:0];
    
    infoLabel.adjustsFontSizeToFitWidth = YES;
    [_setBgView addSubview:infoLabel];
    
    UILabel *enableLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kScreenWidth/2.0,0, kScreenWidth/2 - kLeftMark - 55 , 35)
                                                       backgroundColor:[UIColor clearColor]
                                                                  text:NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil)
                                                             textColor:kBlackLightColor
                                                                  font:kHelveticaLightFont(13)
                                                         textAlignment:NSTextAlignmentRight
                                                         lineBreakMode:NSLineBreakByWordWrapping
                                                         numberOfLines:0];
    
    enableLabel.adjustsFontSizeToFitWidth = YES;
    [_setBgView addSubview:enableLabel];
    
    
    UISwitch *enableSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(kScreenWidth - kLeftMark - 50,5, 50 , 20)];
    enableSwitch.onTintColor = kBlueColor;
    
    [enableSwitch addTarget:self action:@selector(enableSwitch:) forControlEvents:UIControlEventValueChanged];
    enableSwitch.on = [kUserDef boolForKey:kLow];
    enableSwitch.tag = 300;
    [_setBgView addSubview:enableSwitch];
    
    enableSwitch.centerY = infoLabel.centerY;
    enableLabel.centerY = infoLabel.centerY;
    
    UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2 *kLeftMark, 1.0)];
    line.y = infoLabel.bottom + 10;
    line.x = kLeftMark;
    [_setBgView addSubview:line];
    
    //Number
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(kLeftMark, line.bottom + 10, kScreenWidth - 2 *kLeftMark, 80)];
    bgView.layer.cornerRadius = 3;
    bgView.layer.backgroundColor = kGrayLightColor.CGColor;
    bgView.layer.borderColor = KClearColor.CGColor;
    bgView.layer.masksToBounds = YES;
    [_setBgView addSubview:bgView];
    
    UILabel *titleLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(5 ,5, bgView.width - 10, 35)
                                                       backgroundColor:[UIColor clearColor]
                                                                  text:NSLocalizedStringFromTable(@"Low Battery Alert Phone Number", LanguageFileName, nil)
                                                             textColor:kBlackLightColor
                                                                  font:kHelveticaLightFont(16)
                                                         textAlignment:NSTextAlignmentLeft
                                                         lineBreakMode:NSLineBreakByWordWrapping
                                                         numberOfLines:0];
    
    titleLabel.adjustsFontSizeToFitWidth = YES;
    [bgView addSubview:titleLabel];
    
    self.numberField = [[ObjectCTools shared]getACustomTextFiledFrame:CGRectMake(5, titleLabel.bottom , bgView.width - 2*5 - 80, 35)
                                                      backgroundColor:kWhiteColor placeholder:NSLocalizedStringFromTable(@"Insert phone number", LanguageFileName, nil)
                                                            textColor:kBlackLightColor
                                                                 font:kHelveticaLightFont(16)
                                                          borderStyle:UITextBorderStyleRoundedRect
                                                        textAlignment:NSTextAlignmentLeft
                                                   accessibilityLabel:nil autocorrectionType:UITextAutocorrectionTypeDefault clearButtonMode:UITextFieldViewModeWhileEditing tag:100 withIsPassword:NO];
    self.numberField.delegate = self;
    self.numberField.keyboardType = UIKeyboardTypeNumberPad;
    self.numberField.text = [kUserDef objectForKey:@"lownumber"];
    [bgView addSubview:self.numberField];
    
    self.editButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake(self.numberField.right + 5, _numberField.y  ,75, 35)
                                                                backgroudColor:kBlueColor
                                                              titleNormalColor:[UIColor whiteColor]
                                                         titleHighlightedColor:[UIColor grayColor]
                                                                         title:NSLocalizedStringFromTable(@"Set", LanguageFileName, nil)
                                                                          font:kHelveticaLightFont(14)
                                                                  cornerRadius:5
                                                                   borderWidth:0.5
                                                                   borderColor:KClearColor.CGColor
                                                            accessibilityLabel:nil];
    [self.editButton addTarget: self action:@selector(editButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [bgView addSubview:self.editButton];
    self.isEdit = YES;
    
    [self addTableView];
}


- (void) addTableView
{
    _tableView = [[UITableView alloc] initWithFrame:_getBgView.bounds style:UITableViewStylePlain];
    
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    _tableView.separatorColor = [UIColor grayColor];
    [_getBgView addSubview:_tableView];
}
#pragma mark -tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1 + _dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0){
        
        GetMNumberTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GetMNumberTableViewCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"GetMNumberTableViewCell" owner:self options:nil] lastObject];
        }
        
        cell.mlable.text = NSLocalizedStringFromTable(@"Update History", LanguageFileName, nil);
        cell.numberLable.text = nil;
        return cell;
        
    }else
    {
        HistoryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"HistoryTableViewCell" owner:self options:nil] lastObject];
            UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2 *15, 1.0)];
            [cell.contentView addSubview:line];
            line.y = 29+20;
            line.x = 15;
        }
        
        if (_dataArray.count) {
            
            LowBatteryModel *model = _dataArray[indexPath.row - 1];
            cell.mLable.text = model.name;
            cell.dateLabel.text = [DateHelper timeSPToTimeStringWithString:model.updateTime];
            
        }else {
            cell.mLable.text = @"";
            cell.dateLabel.text = @"";
        }
        return cell;
    }
    
}


- (void)editButton:(UIButton *)sender{
    
    if (_isEdit) {
        //send set low battery warning number
        
        if (self.numberField.text.length) {
            NSLog(@"set Low Battery number");
            
            [self.editButton setTitle:NSLocalizedStringFromTable(@"Change", LanguageFileName, nil) forState:UIControlStateNormal];
            [self.editButton setBackgroundColor:kGrayDarkColor];
            [self.numberField resignFirstResponder];
//            NSString *body = [NSString stringWithFormat:@"SET9999#SWECI#%d#",enableSwitch.on];
//            [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Set Detect", LanguageFileName, nil) body:body withDelegate:self];

            [kUserDef setObject:self.numberField.text forKey:@"lownumber"];
            [kUserDef synchronize];
            
        }else
        {
            [self.numberField shake];
            return;
        }
    }else
    {//change number
        NSLog(@"change  number");
        if (self.numberField.text.length) {
            [self.editButton setTitle:NSLocalizedStringFromTable(@"Set", LanguageFileName, nil) forState:UIControlStateNormal];
            [self.editButton setBackgroundColor:kBlueColor];
            [self.numberField becomeFirstResponder];

        }

    }
    
    self.isEdit = !_isEdit;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField.text.length) {
        self.isEdit = YES;
    }
    return YES;
}

- (void)setIsEdit:(BOOL)isEdit
{
    _isEdit = isEdit;
    if (isEdit) {
        [self.editButton setBackgroundColor:kBlueColor];
    }else
    {
        [self.editButton setBackgroundColor:kGrayDarkColor];
        
    }
}



- (void)enableSwitch:(UISwitch *)sender
{
    if (sender.on) {
        [[ObjectCTools shared] showAlertViewAndDissmissAutomatic:NSLocalizedStringFromTable(@"Low Battery Warning", LanguageFileName, nil) andMessage:NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil) withDissmissTime:KDissmissTime withDelegate:nil withAction:nil];
    }
    
    [kUserDef setBool:sender.on forKey:kLow];
    [kUserDef synchronize];
}

#pragma mark 切换 SET GET

- (void)setGetSegmet:(UISegmentedControl *)sender
{
    [super setGetSegmet:sender];
    if (sender.selectedSegmentIndex == 0) {
        _setBgView.hidden = NO;
        _getBgView.hidden = YES;
    }else
    {
        _setBgView.hidden = YES;
        _getBgView.hidden = NO;
        
        LKDBHelper* globalHelper = [LowBatteryModel getUsingLKDBHelper];
        
        //异步 asynchronous
        [globalHelper search:[LowBatteryModel class] where:nil orderBy:@"updateTime desc" offset:0 count:50 callback:^(NSMutableArray *array) {
            for (LowBatteryModel *model in array) {
                NSLog(@"%@ -- %@",model.name,model.updateTime);
            }
            _dataArray.count?([_dataArray removeAllObjects]):nil;
            [_dataArray addObjectsFromArray:array];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_tableView reloadData];
            });
            
            
        }];
        
    }
    
    
}


- (void)checkButton:(UIButton *)sender
{
    [super checkButton:sender];

    UISwitch *enableSwitch = (UISwitch *)[self.view viewWithTag:300];
    
    if (self.setGetSegmet.selectedSegmentIndex == 0) {
        NSString *body = nil;
        if (self.numberField.text.length) {
            body = [NSString stringWithFormat:@"SET#BATTPHONE#%@#SWLOWBATT#%d#",self.numberField.text,enableSwitch.on];
//#warning <#message#>
//
//            LowBatteryModel *model = [[LowBatteryModel alloc]init];
//            model.name = [NSString stringWithFormat:@"%@\n%@",NSLocalizedStringFromTable(@"New Alert Phone Number", LanguageFileName, nil),self.numberField.text];
//            model.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
//            [model saveToDB];
            
        }else
        {
            body = [NSString stringWithFormat:@"SET#SWLOWBATT#%d#",enableSwitch.on];
        }
        
        [kUserDef setObject:self.numberField.text forKey:@"lownumber"];
        [kUserDef synchronize];
        
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Set Low Battery Warning", LanguageFileName, nil) body:body withDelegate:self];
    }else{
        NSString *body = [NSString stringWithFormat:@"GET#SWLOWBATT#"];
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Get Low Battery Warning", LanguageFileName, nil) body:body withDelegate:self];
    }
    
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    switch (result) {
        case MessageComposeResultSent:{
            LowBatteryModel *model = [[LowBatteryModel alloc]init];
            model.name = [NSString stringWithFormat:@"%@:%@",NSLocalizedStringFromTable(@"New Alert Phone Number", LanguageFileName, nil),self.numberField.text];
            model.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
            [model saveToDB];

            //信息传送成功
            NSLog(@"信息传送成功");
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case MessageComposeResultFailed:
            //信息传送失败
            NSLog(@"信息传送失败");
            [self.navigationController popViewControllerAnimated:YES];
            
            break;
        case MessageComposeResultCancelled:
            //信息被用户取消传送
            NSLog(@"信息被用户取消传送");
            break;
        default:
            break;
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
