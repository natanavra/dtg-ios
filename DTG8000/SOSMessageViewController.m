//
//  SOSMessageViewController.m
//  Thomson
//
//  Created by 何助金 on 10/22/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "SOSMessageViewController.h"
#import "SOSMessageTableViewCell.h"
#import "GetMNumberTableViewCell.h"
#import "HistoryTableViewCell.h"
#import "SOSMessageModel.h"
@interface SOSMessageViewController ()<UITableViewDataSource,UITableViewDelegate,SOSMessageTableViewCellDelegate>
{
    UITableView *_tableView;
    NSMutableArray *_dataArray;
    
    UITableView *_getTableView;
    NSMutableArray *_getDataArray;
    
    UIView *_setBgView;
    UIView *_getBgView;
}
@end

@implementation SOSMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArray = [NSMutableArray array];
    _getDataArray = [NSMutableArray array];
    
    int seqIndex = 0;
    
    NSArray *nameArray = @[NSLocalizedStringFromTable(@"Message 1", LanguageFileName, nil),NSLocalizedStringFromTable(@"Message 2", LanguageFileName, nil),NSLocalizedStringFromTable(@"Message 3", LanguageFileName, nil),NSLocalizedStringFromTable(@"Message 4", LanguageFileName, nil),NSLocalizedStringFromTable(@"Message 5", LanguageFileName, nil)];
    for (NSString *name in nameArray) {
        SOSMessageModel *model = [[SOSMessageModel alloc]init];
        model.name = name;
        
        
        //初始值
        
        NSString *keydata =[NSString stringWithFormat:@"sosmessage%d",seqIndex+1];
        //保存临时数据
        if([kUserDef stringForKey:keydata])
        {
            model.message = [kUserDef objectForKey:keydata];
            
            NSArray *arrays= [[kUserDef objectForKey:[NSString stringWithFormat:@"sosmessage_chk%d",seqIndex+1]] componentsSeparatedByString:@","];
           
            NSMutableArray *mutArray = [[NSMutableArray alloc] init];
            for(int i=0;i<[arrays count];i++)
            {
                [mutArray addObject:[[arrays objectAtIndex:i] copy]];
            }
            
            model.selectArray =mutArray;
        }
        
        //NSLog([kUserDef objectForKey:[NSString stringWithFormat:@"sosmessage_chk%d",seqIndex+1]]);
        
        
        seqIndex++;
        
        [_dataArray addObject:model];
    
    }
    
    _setBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 -64)];
    _setBgView.backgroundColor = kWhiteColor;
    _setBgView.hidden = NO;
    [self.view addSubview:_setBgView];
    
    _getBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 - 64)];
    _getBgView.backgroundColor = kWhiteColor;
    _getBgView.hidden = YES;
   
    [self.view addSubview:_getBgView];
    
    [self addTableView];
    [self addTableViewHeadView];
    [self addGetTableView];
}

- (void) addTableView
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,kScreenWidth , _setBgView.height) style:UITableViewStylePlain];
    
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_setBgView addSubview:_tableView];

}

- (void) addGetTableView
{
    _getTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,kScreenWidth , _getBgView.height) style:UITableViewStylePlain];
    
    [_getTableView setBackgroundColor:[UIColor clearColor]];
    [_getTableView setDelegate:self];
    [_getTableView setDataSource:self];
    _getTableView.tableFooterView = [UIView new];
    _getTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_getBgView addSubview:_getTableView];
    
}

- (void)addTableViewHeadView
{
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 35)];
    bgView.backgroundColor = KClearColor;
    
    UILabel *infoLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,0, kScreenWidth - 2*kLeftMark , 35)
                                                     backgroundColor:[UIColor clearColor]
                                                                text:NSLocalizedStringFromTable(@"Setup SOS Message", LanguageFileName, nil)
                                                           textColor:kBlackLightColor
                                                                font:kHelveticaLightFont(16)
                                                       textAlignment:NSTextAlignmentLeft
                                                       lineBreakMode:NSLineBreakByWordWrapping
                                                       numberOfLines:0];
    
    infoLabel.adjustsFontSizeToFitWidth = YES;

    [bgView addSubview:infoLabel];
    _tableView.tableHeaderView = bgView;
}

#pragma mark -tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _tableView) {
        return 5;
    }else
    {
        return _getDataArray.count + 1;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (tableView == _tableView) {
        return 180;
    }else
    {
        return 50;
    }

    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tableView) {
        SOSMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SOSMessageTableViewCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SOSMessageTableViewCell" owner:self options:nil] lastObject];
        }
        
        [cell.editButton setTitle:NSLocalizedStringFromTable(@"Set", LanguageFileName, nil) forState:UIControlStateNormal];
        
        SOSMessageModel *model = _dataArray[indexPath.row];
        cell.messageTitleLabel.text = model.name;
        cell.messageField.text = model.message;
        cell.selectedArray = model.selectArray;
    //[cell setSelectedArray:model.selectArray];
        cell.indexPath = indexPath;
        cell.delegate = self;
        //cell.textLabel.text = [NSString stringWithFormat:@"%@ %d",NSLocalizedStringFromTable(@"PHONE", LanguageFileName, nil),i+1];
        
//        UIButton *button = [cell.contentView viewWithTag:100 + indexPath.row ];
//        button.selected = YES;
//        if ([model.selectArray containsObject:@(button.tag-100+1)]) {
//            button.selected = YES;
//            NSLog(@"选中了：%ld",button.tag);
//        }else
//        {
//            button.selected = NO;
//        }
        
        NSLog(@"array1:%@",model.selectArray);
        
        for(int j=0;j<[model.selectArray count];j++)
        {
            int v = [[model.selectArray objectAtIndex:j] intValue];
            UIButton *button = [cell.contentView viewWithTag:100 +  v-1];
            button.selected = YES;
        }
        
        
    
        return cell;
  
    }else
    {
        if (indexPath.row == 0){
            
            GetMNumberTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GetMNumberTableViewCell"];
            if (cell == nil) {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"GetMNumberTableViewCell" owner:self options:nil] lastObject];
            }
            
            cell.mlable.text = NSLocalizedStringFromTable(@"Update History", LanguageFileName, nil);
            cell.numberLable.text = nil;
            return cell;
            
        }else
        {
            HistoryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryCell"];
            if (cell == nil) {
                cell = [[[NSBundle mainBundle] loadNibNamed:@"HistoryTableViewCell" owner:self options:nil] lastObject];
                UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2 *15, 1.0)];
                [cell.contentView addSubview:line];
                line.y = 29+20;
                line.x = 15;
            }
            
            if (_getDataArray.count) {
                
                SOSMessageModel *model = _getDataArray[indexPath.row - 1];
                cell.mLable.text = [NSString stringWithFormat:@"%@\n%@",model.message,model.phoneNames];
                cell.dateLabel.text = [DateHelper timeSPToTimeStringWithString:model.updateTime];
                
            }else {
                cell.mLable.text = @"";
                cell.dateLabel.text = @"";
            }
            return cell;
        }
        

    }
}

- (void)SOSMessageTableViewCellUpdateMessage:(SOSMessageTableViewCell *)cell withSelectedArray:(NSMutableArray *)selectedArray{
    
    NSLog(@"updateselect %@",selectedArray);
    
    SOSMessageModel *model = _dataArray[cell.indexPath.row];
    model.message = cell.messageField.text;
    model.selectArray = selectedArray;
}

#pragma mark - SOSmesage delegate 单发 SET
- (void)SOSMessageTableViewCell:(SOSMessageTableViewCell *)cell withSelectedArray:(NSMutableArray *)selectedArray
{
    SOSMessageModel *model = _dataArray[cell.indexPath.row];
    model.message = cell.messageField.text;
    model.selectArray = selectedArray;
    NSString *phoneStr = @"";
    
    if (!cell.isEdit) {
        NSString *body = [NSString stringWithFormat:@"SET#"];
        NSString *body1 = @"";

            if (cell.messageField.text.length && selectedArray.count) {
                body = [body stringByAppendingString:[NSString stringWithFormat:@"SOSMSG%d#%@",cell.indexPath.row + 1,cell.messageField.text]];
              
                NSString *phoneName = @"";
               
            
                
                for (id s in selectedArray) {
                    phoneStr = [phoneStr stringByAppendingString:[NSString stringWithFormat:@"%@,",s]];
                    phoneName = [phoneName stringByAppendingString:[NSString stringWithFormat:@"%@%@/",NSLocalizedStringFromTable(@"PHONE", LanguageFileName, nil),s]];
                }
                
                phoneName = [phoneName substringToIndex:phoneName.length-1];
                model.phoneNames = phoneName;
                model.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
                [model saveToDB];
                
                phoneStr = [phoneStr substringToIndex:phoneStr.length-1];
                body1 =[body1 stringByAppendingString:[NSString stringWithFormat:@"#CONFIGSMS%d#%@",cell.indexPath.row + 1 ,phoneStr]];
                
                
            }
        
        body1 = [body1 stringByAppendingString:@"#"];
        body = [body stringByAppendingString:body1];
        
        if ([body rangeOfString:@"SOSMSG"].length) {
            [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"SET SOS Message", LanguageFileName, nil) body:body withDelegate:self];
            
        }
    }
    
    NSString *keydata =[NSString stringWithFormat:@"sosmessage%ld",cell.indexPath.row + 1];
    //保存临时数据
    [kUserDef setObject:cell.messageField.text forKey:keydata];
    [kUserDef setObject:phoneStr forKey:[NSString stringWithFormat:@"sosmessage_chk%ld",cell.indexPath.row+1]];
    [kUserDef synchronize];
    

}



#pragma mark - send GET SET

- (void)checkButton:(UIButton *)sender
{
    [super checkButton:sender];

    if (self.setGetSegmet.selectedSegmentIndex == 0) {
        
        NSString *body = [NSString stringWithFormat:@"SET#"];
        NSString *body1 = @"";
        for (int i = 0 ;i < 5; i ++) {

            SOSMessageModel *model = _dataArray[i];
            NSString *phoneStr =@"";
            if (model.message.length && model.selectArray.count) {
                body = [body stringByAppendingString:[NSString stringWithFormat:@"SOSMSG%d#%@#",i + 1,model.message]];
                
//                NSString *phoneName = @"";

                for (id s in model.selectArray) {
                    phoneStr = [phoneStr stringByAppendingString:[NSString stringWithFormat:@"%@,",s]];
//                    phoneName = [phoneName stringByAppendingString:[NSString stringWithFormat:@"Phone%@/",s]];

                }
//                NSLog(@">>>>>>>>>>>>>>%@",model.name);
//                phoneName = [phoneName substringToIndex:phoneName.length-1];
//                model.phoneNames = phoneName;
//                model.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
//                [model saveToDB];

                phoneStr = [phoneStr substringToIndex:phoneStr.length-1];
                body1 =[body1 stringByAppendingString:[NSString stringWithFormat:@"#CONFIGSMS%d#%@",i + 1 ,phoneStr]];
                
                
                
                
            }
            
            NSLog(@"chk_%@",phoneStr);
            
            NSString *keydata =[NSString stringWithFormat:@"sosmessage%d",i + 1];
            //保存临时数据
            [kUserDef setObject:model.message forKey:keydata];
            [kUserDef setObject:phoneStr forKey:[NSString stringWithFormat:@"sosmessage_chk%d",i+1]];
            [kUserDef synchronize];
            
        }
        body1 = [body1 stringByAppendingString:@"#"];
        body1 = [body1 substringFromIndex:1];
        body = [body stringByAppendingString:body1];
//        SET_PW_#SOSMSG1#5ggfdd#SOSMSG2#Hgdf #CONFIGSMS1#Y1,Y3#CONFIGSMS2#Y2,Y6#
        
        if ([body rangeOfString:@"SOSMSG"].length) {
            [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"SET SOS Message", LanguageFileName, nil) body:body withDelegate:self];
    
        }
        
    }else
    {
        NSString *body = [NSString stringWithFormat:@"GET#SOSMSG1#SOSMSG2#SOSMSG3#SOSMSG4#SOSMSG5#CONFIGSMS1#CONFIGSMS2#CONFIGSMS3#CONFIGSMS4#CONFIGSMS5#"];

        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"GET SOS Message", LanguageFileName, nil) body:body withDelegate:self];
    }
    
    
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    switch (result) {
        case MessageComposeResultSent:{
           
            for (int i = 0 ;i < 5; i ++) {
                
                SOSMessageModel *model = _dataArray[i];
                
                if (model.message.length && model.selectArray.count) {
                    
                    NSString *phoneName = @"";
                    for (id s in model.selectArray) {
                        phoneName = [phoneName stringByAppendingString:[NSString stringWithFormat:@"Phone%@/",s]];
                        
                    }
                    NSLog(@">>>>>>>>>>>>>>%@",model.name);
                    phoneName = [phoneName substringToIndex:phoneName.length-1];
                    model.phoneNames = phoneName;
                    model.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
                    [model saveToDB];

                }
            }
            //信息传送成功
            NSLog(@"信息传送成功");
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case MessageComposeResultFailed:
            //信息传送失败
            NSLog(@"信息传送失败");
            [self.navigationController popViewControllerAnimated:YES];
            
            break;
        case MessageComposeResultCancelled:
            //信息被用户取消传送
            NSLog(@"信息被用户取消传送");
            break;
        default:
            break;
    }
    
}



#pragma mark 切换 SET GET

- (void)setGetSegmet:(UISegmentedControl *)sender
{
    [super setGetSegmet:sender];
    if (sender.selectedSegmentIndex == 0) {
        _setBgView.hidden = NO;
        _getBgView.hidden = YES;
    }else
    {
        _setBgView.hidden = YES;
        _getBgView.hidden = NO;
        
        LKDBHelper* globalHelper = [SOSMessageModel getUsingLKDBHelper];
        
        //异步 asynchronous
        [globalHelper search:[SOSMessageModel class] where:nil orderBy:@"updateTime desc" offset:0 count:50 callback:^(NSMutableArray *array) {
            for (SOSMessageModel *model in array) {
                NSLog(@"%@ -- %@",model.message,model.updateTime);
            }
            _getDataArray.count?([_getDataArray removeAllObjects]):nil;
            [_getDataArray addObjectsFromArray:array];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_getTableView reloadData];
            });
            
            
        }];
        
    }
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
