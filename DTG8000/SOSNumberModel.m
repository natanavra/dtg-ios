//
//  SOSNumberModel.m
//  Thomson
//
//  Created by 何助金 on 11/9/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "SOSNumberModel.h"

@implementation SOSNumberModel
+(NSString *)getTableName
{
    return @"SOSNumberTable";
}
//主键
+(NSString *)getPrimaryKey
{
    return @"updateTime";
}
///复合主键  这个优先级最高
+(NSArray *)getPrimaryKeyUnionArray
{
    return @[@"updateTime",@"name"];
}
//重载选择 使用的LKDBHelper
+(LKDBHelper *)getUsingLKDBHelper
{
    static LKDBHelper* db;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //        NSString* dbpath = [NSHomeDirectory() stringByAppendingPathComponent:@"asd/asd.db"];
        //        db = [[LKDBHelper alloc]initWithDBPath:dbpath];
        //or
        db = [[LKDBHelper alloc]init];
    });
    return db;
}
// 将要插入数据库
+(BOOL)dbWillInsert:(NSObject *)entity
{
    LKErrorLog(@"will insert : %@",NSStringFromClass(self));
    return YES;
}
//已经插入数据库
+(void)dbDidInserted:(NSObject *)entity result:(BOOL)result
{
    LKErrorLog(@"did insert : %@",NSStringFromClass(self));
}
@end
