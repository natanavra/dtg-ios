//
//  HelpModel.h
//  Thomson
//
//  Created by 何助金 on 10/25/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HelpModel : NSObject
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *detailTitle;
@property (nonatomic,assign) BOOL isOpen;
@property (nonatomic,assign) float detailHeight;
@end
