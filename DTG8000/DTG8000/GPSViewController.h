//
//  GPSViewController.h
//  Thomson
//
//  Created by 何助金 on 10/21/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "BaseEventViewController.h"

@interface GPSViewController : BaseEventViewController
@property (nonatomic, assign) BOOL isEdit;
@property (nonatomic, strong) UITextField *numberField;
@property (nonatomic, strong) UIButton *editButton;

@end
