//
//  BaseViewController.m
//  Payou
//
//  Created by 何助金 on 15/7/13.
//  Copyright (c) 2015年 MissionSky. All rights reserved.
//

#import "BaseViewController.h"
#import "HomeViewController.h"
@implementation BaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = kHexRGB(0xFFFFFF);
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = YES;
    
    UIBarButtonItem *leftItem = nil;
    
    if ( [[self.navigationController.viewControllers lastObject] isKindOfClass:[HomeViewController class]]) {
        leftItem = [[ObjectCTools shared] createLeftBarButtonItem:nil target:self selector:@selector(leftBarButtonPress:) ImageName:@"top_menu_sign"];
    }else {
        leftItem = [[ObjectCTools shared] createLeftBarButtonItem:nil target:self selector:@selector(leftBarButtonPress:) ImageName:@"nav_back"];
    }    [self.navigationItem setLeftBarButtonItem:leftItem];
    
    //中间imageView
    UIImage *titleImage = [UIImage imageNamed:@"thomson_logo"];
    UIImageView *titleView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 0,0)];
    titleView.frame = self.navigationItem.titleView.bounds;
    titleView.height -= 20;
    [titleView setImage:titleImage];
    titleView.contentMode = UIViewContentModeCenter;
//    titleView.clipsToBounds = YES;
    [self.navigationItem setTitleView:titleView];

}

- (void)leftBarButtonPress:(UIBarButtonItem *)sender
{
    if (self.navigationController.viewControllers.count > 1) {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        //测滑窗打开或关闭
    }

}


- (void)showMessageView:(NSArray *)phones title:(NSString *)title body:(NSString *)body withDelegate:(id <MFMessageComposeViewControllerDelegate>)vc
{
    if ([NSString isNilOrEmpty:phones[0]]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil
                                                       message:NSLocalizedStringFromTable(@"Please choose a device first", LanguageFileName, nil)delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedStringFromTable(@"Sure", LanguageFileName, nil), nil];
        
        [alert show];

        return;
    }
    
    if( [MFMessageComposeViewController canSendText] )
    {
        MFMessageComposeViewController * controller = [[MFMessageComposeViewController alloc] init];
        controller.recipients = phones;
        controller.navigationBar.tintColor = [UIColor redColor];
        controller.body = body;
        controller.messageComposeDelegate = vc;
        [self presentViewController:controller animated:YES completion:nil];
        [[[[controller viewControllers] lastObject] navigationItem] setTitle:title];//修改短信界面标题
    }
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

@end
