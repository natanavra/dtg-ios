//
//  M1M2Model.h
//  Thomson
//
//  Created by 何助金 on 10/27/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface M1M2Model : ObjectCTools
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *number;
@property (nonatomic,strong) NSString *updateTime;
@end
