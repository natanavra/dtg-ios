//
//  SOSMessageModel.h
//  Thomson
//
//  Created by 何助金 on 11/9/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SOSMessageModel : NSObject
@property (nonatomic,strong) NSString *name; //message1-5
@property (nonatomic,strong) NSString *message;
@property (nonatomic,strong) NSString *phoneNames;

@property (nonatomic,strong) NSMutableArray  *selectArray;
@property (nonatomic,strong) NSString *updateTime;
@end
