//
//  ToneProfileViewController.m
//  Thomson
//
//  Created by 何助金 on 10/22/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "ToneProfileViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "GetMNumberTableViewCell.h"
#import "HistoryTableViewCell.h"
#import "RangToneModel.h"
#define kRing     @"Ring"
#define kRingType @"RingType"
#define KVol      @"Vol"
@interface ToneProfileViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UIView *_setBgView;
    UIView *_getBgView;
    UITableView *_tableView;
    NSMutableArray *_dataArray;
    SystemSoundID soundID;

}
@property (nonatomic ,strong) UIScrollView *scrollView;
@property (nonatomic ,strong) UISlider *volumeSlider;

@end

@implementation ToneProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArray = [NSMutableArray array];
    
    _setBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 -64)];
    _setBgView.backgroundColor = kWhiteColor;
    _setBgView.hidden = NO;
    [self.view addSubview:_setBgView];
    
    _getBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 - 64)];
    _getBgView.backgroundColor = kWhiteColor;
    _getBgView.hidden = YES;
    [self.view addSubview:_getBgView];

    
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0,kScreenWidth ,  _setBgView.height)];
    [_setBgView addSubview:self.scrollView];
    
    //
    UILabel *infoLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,0, kScreenWidth - 2*kLeftMark , 35)
                                                     backgroundColor:[UIColor clearColor]
                                                                text:NSLocalizedStringFromTable(@" Ring Scene Setup", LanguageFileName, nil)
                                                           textColor:kBlackLightColor
                                                                font:kHelveticaLightFont(16)
                                                       textAlignment:NSTextAlignmentLeft
                                                       lineBreakMode:NSLineBreakByWordWrapping
                                                       numberOfLines:0];
    
    infoLabel.adjustsFontSizeToFitWidth = YES;
    
    [self.scrollView addSubview: infoLabel];
    
    NSArray *titleArray = @[NSLocalizedStringFromTable(@" Ring Only", LanguageFileName, nil),NSLocalizedStringFromTable(@" Vibration", LanguageFileName, nil),NSLocalizedStringFromTable(@" Ring&Vibration", LanguageFileName, nil),NSLocalizedStringFromTable(@" Mute", LanguageFileName, nil)];
    int n = 0;
    for (int i = 0 ; i < 2; ++i) {
       
        for (int j = 0; j < 2; ++ j) {
           
            UIButton *ringSceneButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake( kLeftMark + j * (kScreenWidth/2), 40+ i * 40, kScreenWidth/2 - 2*kLeftMark, 35)
                                                                         backgroudColor:KClearColor
                                                                       titleNormalColor:kBlackLightColor
                                                                  titleHighlightedColor:[UIColor grayColor]
                                                                                  title:titleArray[n ]
                                                                                   font:kHelveticaLightFont(11)
                                                                           cornerRadius:0
                                                                            borderWidth:0
                                                                            borderColor:KClearColor.CGColor
                                                                     accessibilityLabel:nil];
            [ringSceneButton addTarget: self action:@selector(ringSceneButton:) forControlEvents:UIControlEventTouchUpInside];
            [ringSceneButton setImage:[UIImage imageNamed:@"point"] forState:UIControlStateNormal];
            [ringSceneButton setImage:[UIImage imageNamed:@"point_p"] forState:UIControlStateSelected];
            ringSceneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [ringSceneButton setContentEdgeInsets:(UIEdgeInsetsMake(0, 0, 0, 0))];
            ringSceneButton.tag = 200 + n;
            [kUserDef integerForKey:kRing];
            if (n== [kUserDef integerForKey:kRing]) {
                ringSceneButton.selected = YES;
            }
            n ++;
            [self.scrollView  addSubview:ringSceneButton];
            
            if (n == 4) {
                UIView *lineView = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2*kLeftMark, 1)];
                lineView.y = ringSceneButton.bottom + 5;
                lineView.x = kLeftMark;
                [self.scrollView addSubview:lineView];
            }

        }
        
    }
    
    //ringType setupint
    UILabel *ringTypeLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,40 *3 + 5 + 5, kScreenWidth - 2*kLeftMark , 35)
                                                     backgroundColor:[UIColor clearColor]
                                                                text:NSLocalizedStringFromTable(@" Ring Type Setup", LanguageFileName, nil)
                                                           textColor:kBlackLightColor
                                                                font:kHelveticaLightFont(16)
                                                       textAlignment:NSTextAlignmentLeft
                                                       lineBreakMode:NSLineBreakByWordWrapping
                                                       numberOfLines:0];
    
    ringTypeLabel.adjustsFontSizeToFitWidth = YES;
    
    [self.scrollView addSubview: ringTypeLabel];
    
    
    for (int i = 0 ; i < 8 ; ++ i ) {
        NSString *title = [NSString stringWithFormat:@"%d",i+1];
        UIButton *ringTypeButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake( kLeftMark + i * (kScreenWidth - 2*kLeftMark)/8, ringTypeLabel.bottom + 5,  (kScreenWidth - 2*kLeftMark)/8 - 3 , 25)
                                                                              backgroudColor:KClearColor
                                                                            titleNormalColor:kBlackLightColor
                                                                       titleHighlightedColor:kWhiteColor
                                                                                       title:title
                                                                                        font:kHelveticaLightFont(14)
                                                                                cornerRadius:3
                                                                                 borderWidth:1
                                                                                 borderColor:kGrayColor.CGColor
                                                                          accessibilityLabel:nil];
        [ringTypeButton addTarget: self action:@selector(ringTypeButton:) forControlEvents:UIControlEventTouchUpInside];


        ringTypeButton.tag = 300 + i;
        if ( i == [kUserDef integerForKey:kRingType]) {
            ringTypeButton.selected = YES;
            ringTypeButton.layer.backgroundColor = kRedColor.CGColor;

        }else
        {
            ringTypeButton.selected = NO;
        }
        
        [self.scrollView  addSubview:ringTypeButton];
        
        if (i == 7) {
            UIView *lineView = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2*kLeftMark, 1)];
            lineView.y = ringTypeButton.bottom + 15;
            lineView.x = kLeftMark;
            [self.scrollView addSubview:lineView];
        }

    }

    
    //Volume setupint
    UILabel *volumeLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,ringTypeLabel.bottom + 50, kScreenWidth - 2*kLeftMark , 35)
                                                         backgroundColor:[UIColor clearColor]
                                                                    text:NSLocalizedStringFromTable(@" Volume Setup", LanguageFileName, nil)
                                                               textColor:kBlackLightColor
                                                                    font:kHelveticaLightFont(16)
                                                           textAlignment:NSTextAlignmentLeft
                                                           lineBreakMode:NSLineBreakByWordWrapping
                                                           numberOfLines:0];
    
    volumeLabel.adjustsFontSizeToFitWidth = YES;
    
    [self.scrollView addSubview: volumeLabel];

    self.volumeSlider = [[UISlider alloc]initWithFrame:CGRectMake(kLeftMark, volumeLabel.bottom + 15, kScreenWidth -2*kLeftMark, 10)];

    self.volumeSlider.value = 2;

    self.volumeSlider.minimumValue = 1;
    self.volumeSlider.maximumValue = 5;
    self.volumeSlider.minimumTrackTintColor = kRedColor;
    self.volumeSlider.maximumTrackTintColor = kBlackLightColor;
    self.volumeSlider.continuous = NO;

    [self.volumeSlider setThumbImage: [UIImage imageNamed:@"point_red_big"] forState:UIControlStateNormal];
    [self.scrollView addSubview:self.volumeSlider];
    
    
    for (int i = 0; i < 5; i ++) {
        
        UILabel *valueLabe = [[UILabel alloc]initWithFrame:CGRectMake((self.volumeSlider.width/4.0)*i - 7 , 0, 10, 15)];
        valueLabe.text = [NSString stringWithFormat:@"%d",i + 1];
        valueLabe.backgroundColor = KClearColor;
        valueLabe.font = kHelveticaLightFont(13);
        
        UIImageView *pointView = [[UIImageView alloc]initWithFrame:CGRectMake((self.volumeSlider.width/4.0)*i -7, 0, 7, 7)];
        pointView.centerY = self.volumeSlider.height/2.0;
        valueLabe.y = self.volumeSlider.height/2.0 + 8;

        if (i == 0) {
            pointView.x = 0;
            valueLabe.x = 2;
            pointView.image = [UIImage imageNamed:@"point_red_small"];
        }else
        {
            pointView.image = [UIImage imageNamed:@"point_black"];
            
        }
        if (i == 1) {
            pointView.x += 4;
            valueLabe.x += 4;
        }
        
        if (i == 2) {
            pointView.x += 2;
            valueLabe.x += 2;
        }
        pointView.tag = 400 + i;
        [self.volumeSlider addSubview:pointView];
        [self.volumeSlider bringSubviewToFront:pointView];
        
        [self.volumeSlider addSubview:valueLabe];
        [self.volumeSlider bringSubviewToFront:valueLabe];
        
        
    }

    [self.volumeSlider addTarget:self action:@selector(volumeSlider:) forControlEvents:UIControlEventValueChanged];
    
    if (self.volumeSlider.bottom + 40 + 64> self.scrollView.height) {
        self.scrollView.contentSize = CGSizeMake(kScreenWidth, self.volumeSlider.bottom + 40 + 64);
    }
    self.volumeSlider.value = [kUserDef floatForKey:KVol];
    [self volumeSlider:self.volumeSlider];
    [self addTableView];
}


- (void) addTableView
{
    _tableView = [[UITableView alloc] initWithFrame:_getBgView.bounds style:UITableViewStylePlain];
    
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    _tableView.separatorColor = [UIColor grayColor];
    [_getBgView addSubview:_tableView];
}
#pragma mark -tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1 + _dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0){
        
        GetMNumberTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GetMNumberTableViewCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"GetMNumberTableViewCell" owner:self options:nil] lastObject];
        }
        
        cell.mlable.text = NSLocalizedStringFromTable(@"Update History", LanguageFileName, nil);
        cell.numberLable.text = nil;
        return cell;
        
    }else
    {
        HistoryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"HistoryTableViewCell" owner:self options:nil] lastObject];
            UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2 *15, 1.0)];
            [cell.contentView addSubview:line];
            line.y = 29;
            line.x = 15;
        }
        
        if (_dataArray.count) {
            
            RangToneModel *model = _dataArray[indexPath.row - 1];
            cell.mLable.text = model.name;
            cell.dateLabel.text = [DateHelper timeSPToTimeStringWithString:model.updateTime];
            
        }else {
            cell.mLable.text = @"";
            cell.dateLabel.text = @"";
        }
        return cell;
    }
    
}


- (void)volumeSlider:(UISlider *)sender
{
    
    NSLog(@"%ld -- %f",lroundf(sender.value),sender.value);
    float f = lroundf(sender.value);
    [UIView animateWithDuration:0.5 animations:^{
        sender.value = f;
        
    }];
    [kUserDef setFloat:f forKey:KVol];
    [kUserDef synchronize];
    
    if (sender.value >= 1 && sender.value < 1.5 ) {
        for (int i = 0 ; i < 5; i ++) {
            UIImageView *pointView = [self.scrollView viewWithTag:400 + i];
            if (i+1 <= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];

            }
        }
    }else if (sender.value >= 1.5 && sender.value < 2.5 ) {
        for (int i = 0 ; i < 5; i ++) {
            UIImageView *pointView = [self.scrollView viewWithTag:400 + i];
            if (i+1<= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];
                
            }
        }

    }else if(sender.value >= 2.5 && sender.value < 3.5){
        for (int i = 0 ; i < 5; i ++) {
            UIImageView *pointView = [self.scrollView viewWithTag:400 + i];
            if (i+1<= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];
                
            }
        }

    }else if(sender.value >= 3.5 && sender.value < 4.5){
        for (int i = 0 ; i < 5; i ++) {
            UIImageView *pointView = [self.scrollView viewWithTag:400 + i];
            if (i+1<= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];
                
            }
        }

    }else if(sender.value >= 4.5){
        for (int i = 0 ; i < 5; i ++) {
            UIImageView *pointView = [self.scrollView viewWithTag:400 + i];
            if (i+1<= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];
                
            }
        }

    }

        

}

- (void)playSoundWith:(int)index
{
    
    if (soundID ) {
        AudioServicesDisposeSystemSoundID(soundID);
    }
    NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"ring%d",index] ofType:@"wav"];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:path], &soundID);
    
    AudioServicesPlaySystemSound(soundID);

}
static int ringType = 0;
- (void)ringTypeButton:(UIButton *)sender
{
    for (int i = 0 ; i < 8 ; ++i) {
        UIButton *button  = (UIButton *)[self.scrollView viewWithTag:300 + i];
        if (button.tag == sender.tag ) {
            button.selected = YES;
            button.layer.backgroundColor = kRedColor.CGColor;
            ringType = i;
            [self playSoundWith:i+1];
            [kUserDef setInteger:i forKey:kRingType];
            [kUserDef synchronize];
        }else{
            button.selected = NO;
            button.layer.backgroundColor = kWhiteColor.CGColor;
        }
    }

}

static int ringScene = 0;
- (void)ringSceneButton:(UIButton *)sender{
    
    for (int i = 0 ; i < 4 ; ++i) {
        UIButton *button  = (UIButton *)[self.scrollView viewWithTag:200 + i];
        if (button.tag == sender.tag ) {
            button.selected = YES;
            ringScene = i;
            [[ObjectCTools shared] showAlertViewAndDissmissAutomatic:NSLocalizedStringFromTable(@"Ring Scene", LanguageFileName, nil) andMessage:button.titleLabel.text withDissmissTime:KDissmissTime withDelegate:nil withAction:nil];
            [kUserDef setInteger:i forKey:kRing];
            [kUserDef synchronize];
        }else{
            button.selected = NO;
        }
    }
}

#pragma mark 切换 SET GET

- (void)setGetSegmet:(UISegmentedControl *)sender
{
    [super setGetSegmet:sender];
    
    if (sender.selectedSegmentIndex == 0) {
        _setBgView.hidden = NO;
        _getBgView.hidden = YES;
    }else
    {
        _setBgView.hidden = YES;
        _getBgView.hidden = NO;
        
        LKDBHelper* globalHelper = [RangToneModel getUsingLKDBHelper];
        
        //异步 asynchronous
        [globalHelper search:[RangToneModel class] where:nil orderBy:@"updateTime desc" offset:0 count:50 callback:^(NSMutableArray *array) {
            for (RangToneModel *model in array) {
                NSLog(@"%@ -- %@",model.name,model.updateTime);
            }
            _dataArray.count?([_dataArray removeAllObjects]):nil;
            [_dataArray addObjectsFromArray:array];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_tableView reloadData];
            });
            
            
        }];
        
    }
    
    
}


- (void)checkButton:(UIButton *)sender
{
    [super checkButton:sender];

    if (self.setGetSegmet.selectedSegmentIndex == 0) {
        NSString *body = [NSString stringWithFormat:@"SET%@#RING#%d#VOL#%ld#SCENE#%d#",kMessagePassword,ringType,lroundf(self.volumeSlider.value)-1,ringScene];
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"SET About RING", LanguageFileName, nil) body:body withDelegate:self];
        
//        RangToneModel *sceneModel = [[RangToneModel alloc]init];
//        switch (ringScene) {
//            case 0:
//                sceneModel.name = [NSString stringWithFormat:@"%@:%@",NSLocalizedStringFromTable(@"Ring Scene", LanguageFileName, nil),NSLocalizedStringFromTable(@"Ring Only", LanguageFileName, nil)];
//                break;
//            case 1:
//                sceneModel.name = [NSString stringWithFormat:@"%@:%@",NSLocalizedStringFromTable(@"Ring Scene", LanguageFileName, nil),NSLocalizedStringFromTable(@"Vibration", LanguageFileName, nil)];
//                break;
//            case 2:
//                sceneModel.name = [NSString stringWithFormat:@"%@:%@",NSLocalizedStringFromTable(@"Ring Scene", LanguageFileName, nil),NSLocalizedStringFromTable(@"Ring&Vibration", LanguageFileName, nil)];
//                break;
//            case 3:
//                
//                sceneModel.name = [NSString stringWithFormat:@"%@:%@",NSLocalizedStringFromTable(@"Ring Scene", LanguageFileName, nil),NSLocalizedStringFromTable(@"Mute", LanguageFileName, nil)];
//                
//                break;
//                
//            default:
//                break;
//        }
//        sceneModel.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
//        [sceneModel saveToDB];
//        
//        RangToneModel *typeModel = [[RangToneModel alloc]init];
//        typeModel.name = [NSString stringWithFormat:@"%@:%ld" ,NSLocalizedStringFromTable(@"Type", LanguageFileName, nil),(long)(ringType + 1)];
//        typeModel.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
//        [typeModel saveToDB];
//        
//        RangToneModel *volModel = [[RangToneModel alloc]init];
//        volModel.name = [NSString stringWithFormat:@"%@:%ld",NSLocalizedStringFromTable(@"Volume", LanguageFileName, nil),lroundf(self.volumeSlider.value)];
//        volModel.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
//        [volModel saveToDB];
        
    }else
    {
        NSString *body = [NSString stringWithFormat:@"GET%@#RING#VOL#SCENE#",kMessagePassword];
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"GET About RING", LanguageFileName, nil) body:body withDelegate:self];
    }


}



-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    switch (result) {
        case MessageComposeResultSent:{
            
            RangToneModel *sceneModel = [[RangToneModel alloc]init];
            switch (ringScene) {
                case 0:
                    sceneModel.name = [NSString stringWithFormat:@"%@:%@",NSLocalizedStringFromTable(@"Ring Scene", LanguageFileName, nil),NSLocalizedStringFromTable(@"Ring Only", LanguageFileName, nil)];
                    break;
                case 1:
                    sceneModel.name = [NSString stringWithFormat:@"%@:%@",NSLocalizedStringFromTable(@"Ring Scene", LanguageFileName, nil),NSLocalizedStringFromTable(@"Vibration", LanguageFileName, nil)];
                    break;
                case 2:
                    sceneModel.name = [NSString stringWithFormat:@"%@:%@",NSLocalizedStringFromTable(@"Ring Scene", LanguageFileName, nil),NSLocalizedStringFromTable(@"Ring&Vibration", LanguageFileName, nil)];
                    break;
                case 3:
                    
                    sceneModel.name = [NSString stringWithFormat:@"%@:%@",NSLocalizedStringFromTable(@"Ring Scene", LanguageFileName, nil),NSLocalizedStringFromTable(@"Mute", LanguageFileName, nil)];
                    
                    break;
                    
                default:
                    break;
            }
            sceneModel.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
            [sceneModel saveToDB];
            
            RangToneModel *typeModel = [[RangToneModel alloc]init];
            typeModel.name = [NSString stringWithFormat:@"%@:%ld" ,NSLocalizedStringFromTable(@"Type", LanguageFileName, nil),(long)(ringType + 1)];
            typeModel.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
            [typeModel saveToDB];
            
            RangToneModel *volModel = [[RangToneModel alloc]init];
            volModel.name = [NSString stringWithFormat:@"%@:%ld",NSLocalizedStringFromTable(@"Volume", LanguageFileName, nil),lroundf(self.volumeSlider.value)];
            volModel.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
            [volModel saveToDB];
            
            //信息传送成功
            NSLog(@"信息传送成功");
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case MessageComposeResultFailed:
            //信息传送失败
            NSLog(@"信息传送失败");
            [self.navigationController popViewControllerAnimated:YES];
            
            break;
        case MessageComposeResultCancelled:
            //信息被用户取消传送
            NSLog(@"信息被用户取消传送");
            break;
        default:
            break;
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
