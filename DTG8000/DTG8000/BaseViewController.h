//
//  BaseViewController.h
//  Payou
//
//  Created by 何助金 on 15/7/13.
//  Copyright (c) 2015年 MissionSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface BaseViewController : UIViewController<MFMessageComposeViewControllerDelegate>
- (void)leftBarButtonPress:(UIBarButtonItem *)sender;
- (void)showMessageView:(NSArray *)phones title:(NSString *)title body:(NSString *)body withDelegate:(id <MFMessageComposeViewControllerDelegate>)vc;
@end

