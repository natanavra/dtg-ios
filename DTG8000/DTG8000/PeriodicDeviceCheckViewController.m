//
//  PeriodicDeviceCheckViewController.m
//  Thomson
//
//  Created by 何助金 on 10/22/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "PeriodicDeviceCheckViewController.h"
#import "GetMNumberTableViewCell.h"
#import "HistoryTableViewCell.h"
#import "PeriodicModel.h"
#define kPeriodicEnable @"PeriodicEnable"
#define kTimeRange @"TimeRange"

@interface PeriodicDeviceCheckViewController ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>

{
    UIView *_setBgView;
    UIView *_getBgView;
    UITableView *_tableView;
    NSMutableArray *_dataArray;
}
@property (nonatomic ,strong) UISlider *volumeSlider;


@end

@implementation PeriodicDeviceCheckViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArray = [NSMutableArray array];
    
    _setBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 -64)];
    _setBgView.backgroundColor = kWhiteColor;
    _setBgView.hidden = NO;
    [self.view addSubview:_setBgView];
    
    _getBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 - 64)];
    _getBgView.backgroundColor = kWhiteColor;
    _getBgView.hidden = YES;
    [self.view addSubview:_getBgView];
    
    
    UILabel *infoLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,0, kScreenWidth/2 - kLeftMark , 35)
                                                     backgroundColor:[UIColor clearColor]
                                                                text:NSLocalizedStringFromTable(@"Periodic Device Check", LanguageFileName, nil)
                                                           textColor:kBlackLightColor
                                                                font:kHelveticaLightFont(16)
                                                       textAlignment:NSTextAlignmentLeft
                                                       lineBreakMode:NSLineBreakByWordWrapping
                                                       numberOfLines:0];
    
    infoLabel.adjustsFontSizeToFitWidth = YES;
    [_setBgView addSubview:infoLabel];
    
    UILabel *enableLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kScreenWidth/2.0,0, kScreenWidth/2 - kLeftMark - 55 , 35)
                                                       backgroundColor:[UIColor clearColor]
                                                                  text:NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil)
                                                             textColor:kBlackLightColor
                                                                  font:kHelveticaLightFont(13)
                                                         textAlignment:NSTextAlignmentRight
                                                         lineBreakMode:NSLineBreakByWordWrapping
                                                         numberOfLines:0];
    
    enableLabel.adjustsFontSizeToFitWidth = YES;
    [_setBgView addSubview:enableLabel];
    
    
    UISwitch *enableSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(kScreenWidth - kLeftMark - 50,5, 50 , 20)];
    enableSwitch.onTintColor = kRedColor;
    
    [enableSwitch addTarget:self action:@selector(enableSwitch:) forControlEvents:UIControlEventValueChanged];
    enableSwitch.on = [kUserDef boolForKey:kPeriodicEnable];
    enableSwitch.tag = 300;
    [_setBgView addSubview:enableSwitch];
    
    enableSwitch.centerY = infoLabel.centerY;
    enableLabel.centerY = infoLabel.centerY;
    
    UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2 *kLeftMark, 1.0)];
    line.y = infoLabel.bottom + 10;
    line.x = kLeftMark;
    [_setBgView addSubview:line];
    
    //Number
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(kLeftMark, line.bottom + 10, kScreenWidth - 2 *kLeftMark, 80)];
    bgView.layer.cornerRadius = 3;
    bgView.layer.backgroundColor = kGrayLightColor.CGColor;
    bgView.layer.borderColor = KClearColor.CGColor;
    bgView.layer.masksToBounds = YES;
    [_setBgView addSubview:bgView];
    
    UILabel *titleLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(5 ,5, bgView.width - 10, 35)
                                                      backgroundColor:[UIColor clearColor]
                                                                 text:NSLocalizedStringFromTable(@"Phone Number For Verification", LanguageFileName, nil)
                                                            textColor:kBlackLightColor
                                                                 font:kHelveticaLightFont(16)
                                                        textAlignment:NSTextAlignmentLeft
                                                        lineBreakMode:NSLineBreakByWordWrapping
                                                        numberOfLines:0];
    
    titleLabel.adjustsFontSizeToFitWidth = YES;
    [bgView addSubview:titleLabel];
    
    self.numberField = [[ObjectCTools shared]getACustomTextFiledFrame:CGRectMake(5, titleLabel.bottom , bgView.width - 2*5 - 55, 35)
                                                      backgroundColor:kWhiteColor placeholder:NSLocalizedStringFromTable(@"Insert phone number", LanguageFileName, nil)
                                                            textColor:kBlackLightColor
                                                                 font:kHelveticaLightFont(16)
                                                          borderStyle:UITextBorderStyleRoundedRect
                                                        textAlignment:NSTextAlignmentLeft
                                                   accessibilityLabel:nil autocorrectionType:UITextAutocorrectionTypeDefault clearButtonMode:UITextFieldViewModeWhileEditing tag:100 withIsPassword:NO];
    self.numberField.delegate = self;
    self.numberField.keyboardType = UIKeyboardTypeNumberPad;
    self.numberField.text= [kUserDef objectForKey:@"periodicnumber"];
    [bgView addSubview:self.numberField];
    
    self.editButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake(self.numberField.right + 5, _numberField.y  ,50, 35)
                                                                backgroudColor:kRedColor
                                                              titleNormalColor:[UIColor whiteColor]
                                                         titleHighlightedColor:[UIColor grayColor]
                                                                         title:NSLocalizedStringFromTable(@"Set", LanguageFileName, nil)
                                                                          font:kHelveticaLightFont(14)
                                                                  cornerRadius:5
                                                                   borderWidth:0.5
                                                                   borderColor:KClearColor.CGColor
                                                            accessibilityLabel:nil];
    [self.editButton addTarget: self action:@selector(editButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [bgView addSubview:self.editButton];
    self.isEdit = YES;
    
    //time Range
    UIView *lineView = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2*kLeftMark, 1)];
    lineView.y = bgView.bottom + 15;
    lineView.x = kLeftMark;
    [_setBgView addSubview:lineView];

    UILabel *volumeLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,bgView.bottom + 35, kScreenWidth - 2*kLeftMark , 35)
                                                       backgroundColor:[UIColor clearColor]
                                                                  text:NSLocalizedStringFromTable(@" Time Range(days)", LanguageFileName, nil)
                                                             textColor:kBlackLightColor
                                                                  font:kHelveticaLightFont(16)
                                                         textAlignment:NSTextAlignmentLeft
                                                         lineBreakMode:NSLineBreakByWordWrapping
                                                         numberOfLines:0];

    volumeLabel.adjustsFontSizeToFitWidth = YES;

    [_setBgView addSubview: volumeLabel];

    self.volumeSlider = [[UISlider alloc]initWithFrame:CGRectMake(kLeftMark, volumeLabel.bottom + 15, kScreenWidth -2*kLeftMark, 10)];

    self.volumeSlider.value = 2;

    self.volumeSlider.minimumValue = 1;
    self.volumeSlider.maximumValue = 7;
    self.volumeSlider.minimumTrackTintColor = kRedColor;
    self.volumeSlider.maximumTrackTintColor = kBlackLightColor;
    self.volumeSlider.continuous = NO;

    [self.volumeSlider setThumbImage: [UIImage imageNamed:@"point_red_big"] forState:UIControlStateNormal];
    [_setBgView addSubview:self.volumeSlider];


    for (int i = 0; i < 7; i ++) {
        
        UILabel *valueLabe = [[UILabel alloc]initWithFrame:CGRectMake((self.volumeSlider.width/6.0)*i - 5 , 0, 10, 15)];
        valueLabe.text = [NSString stringWithFormat:@"%d",i + 1];
        valueLabe.backgroundColor = KClearColor;
        valueLabe.font = kHelveticaLightFont(13);
        
        UIImageView *pointView = [[UIImageView alloc]initWithFrame:CGRectMake((self.volumeSlider.width/6.0)*i -7, 0, 7, 7)];
        pointView.centerY = self.volumeSlider.height/2.0;
        valueLabe.y = self.volumeSlider.height/2.0 + 8;
        
        if (i == 0) {
            pointView.x = 0;
            valueLabe.x = 2;
            pointView.image = [UIImage imageNamed:@"point_red_small"];
        }else
        {
            pointView.image = [UIImage imageNamed:@"point_black"];
            
        }
        if (i == 1) {
            pointView.x += 4;
            valueLabe.x += 4;
        }
        
        if (i == 2) {
            pointView.x += 2;
            valueLabe.x += 2;
        }
        pointView.tag = 400 + i;
        [self.volumeSlider addSubview:pointView];
        [self.volumeSlider bringSubviewToFront:pointView];
        
        [self.volumeSlider addSubview:valueLabe];
        [self.volumeSlider bringSubviewToFront:valueLabe];
        
        
    }

    [self.volumeSlider addTarget:self action:@selector(volumeSlider:) forControlEvents:UIControlEventValueChanged];

//    if (self.volumeSlider.bottom + 40 + 64> _setBgView.height) {
//        _setBgView.contentSize = CGSizeMake(kScreenWidth, self.volumeSlider.bottom + 40 + 64);
//    }
    self.volumeSlider.value = [kUserDef floatForKey:kTimeRange];
    [self volumeSlider:self.volumeSlider];
    [self addTableView];
}

- (void)volumeSlider:(UISlider *)sender
{
    
    NSLog(@"%ld -- %f",lroundf(sender.value),sender.value);
    float f = lroundf(sender.value);
    [UIView animateWithDuration:0.5 animations:^{
        sender.value = f;
        
    }];
    [kUserDef setFloat:f forKey:kTimeRange];
    [kUserDef synchronize];
    
    if (sender.value >= 1 && sender.value < 1.5 ) {
        for (int i = 0 ; i < 7; i ++) {
            UIImageView *pointView = [_setBgView viewWithTag:400 + i];
            if (i+1 <= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];
                
            }
        }
    }else if (sender.value >= 1.5 && sender.value < 2.5 ) {
        for (int i = 0 ; i < 7; i ++) {
            UIImageView *pointView = [_setBgView viewWithTag:400 + i];
            if (i+1<= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];
                
            }
        }
        
    }else if(sender.value >= 2.5 && sender.value < 3.5){
        for (int i = 0 ; i < 7; i ++) {
            UIImageView *pointView = [_setBgView viewWithTag:400 + i];
            if (i+1<= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];
                
            }
        }
        
    }else if(sender.value >= 3.5 && sender.value < 4.5){
        for (int i = 0 ; i < 7; i ++) {
            UIImageView *pointView = [_setBgView viewWithTag:400 + i];
            if (i+1<= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];
                
            }
        }
        
    }else if(sender.value >= 4.5 && sender.value < 5.5){
        for (int i = 0 ; i < 7; i ++) {
            UIImageView *pointView = [_setBgView viewWithTag:400 + i];
            if (i+1<= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];
                
            }
        }
        
    }else if(sender.value >= 5.5 && sender.value < 6.5){
        for (int i = 0 ; i < 7; i ++) {
            UIImageView *pointView = [_setBgView viewWithTag:400 + i];
            if (i+1<= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];
                
            }
        }
        
    }else if(sender.value >= 6.5){
        for (int i = 0 ; i < 7; i ++) {
            UIImageView *pointView = [_setBgView viewWithTag:400 + i];
            if (i+1<= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];
                
            }
        }
        
    }


    
    
    
}

- (void) addTableView
{
    _tableView = [[UITableView alloc] initWithFrame:_getBgView.bounds style:UITableViewStylePlain];
    
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    _tableView.separatorColor = [UIColor grayColor];
    [_getBgView addSubview:_tableView];
}
#pragma mark -tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1 + _dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
        return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0){
        
        GetMNumberTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GetMNumberTableViewCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"GetMNumberTableViewCell" owner:self options:nil] lastObject];
        }
        
        cell.mlable.text = NSLocalizedStringFromTable(@"Update History", LanguageFileName, nil);
        cell.numberLable.text = nil;
        return cell;
        
    }else
    {
        HistoryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"HistoryTableViewCell" owner:self options:nil] lastObject];
            UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2 *15, 1.0)];
            [cell.contentView addSubview:line];
            line.y = 29+20;
            line.x = 15;
        }
        
        if (_dataArray.count) {
            
            PeriodicModel *model = _dataArray[indexPath.row - 1];
            cell.mLable.text = model.name;
            cell.dateLabel.text = [DateHelper timeSPToTimeStringWithString:model.updateTime];
            
        }else {
            cell.mLable.text = @"";
            cell.dateLabel.text = @"";
        }
        return cell;
    }
    
}


- (void)editButton:(UIButton *)sender{
    
    if (_isEdit) {
        //send set low battery warning number
        
        if (self.numberField.text.length) {
            NSLog(@"set  number");
            
            [self.editButton setTitle:NSLocalizedStringFromTable(@"Change", LanguageFileName, nil) forState:UIControlStateNormal];
            [self.editButton setBackgroundColor:kGrayDarkColor];
            [self.numberField resignFirstResponder];
            //            NSString *body = [NSString stringWithFormat:@"SET9999#SWECI#%d#",enableSwitch.on];
            //            [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Set Detect", LanguageFileName, nil) body:body withDelegate:self];
            
            [kUserDef setObject:self.numberField.text forKey:@"periodicnumber"];
            [kUserDef synchronize];
        }else
        {
            [self.numberField shake];
            return;
        }
    }else
    {//change number
        NSLog(@"change  number");
        if (self.numberField.text.length) {
            [self.editButton setTitle:NSLocalizedStringFromTable(@"Set", LanguageFileName, nil) forState:UIControlStateNormal];
            [self.editButton setBackgroundColor:kRedColor];
            [self.numberField becomeFirstResponder];
            
        }
        
    }
    
    self.isEdit = !_isEdit;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField.text.length) {
        self.isEdit = YES;
    }
    return YES;
}

- (void)setIsEdit:(BOOL)isEdit
{
    _isEdit = isEdit;
    if (isEdit) {
        [self.editButton setBackgroundColor:kRedColor];
    }else
    {
        [self.editButton setBackgroundColor:kGrayDarkColor];
        
    }
}



- (void)enableSwitch:(UISwitch *)sender
{
    if (sender.on) {
        [[ObjectCTools shared] showAlertViewAndDissmissAutomatic:NSLocalizedStringFromTable(@"Periodic Device Check", LanguageFileName, nil) andMessage:NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil) withDissmissTime:KDissmissTime withDelegate:nil withAction:nil];
    }
    
    [kUserDef setBool:sender.on forKey:kPeriodicEnable];
    [kUserDef synchronize];
}

#pragma mark 切换 SET GET

- (void)setGetSegmet:(UISegmentedControl *)sender
{
    [super setGetSegmet:sender];
    if (sender.selectedSegmentIndex == 0) {
        _setBgView.hidden = NO;
        _getBgView.hidden = YES;
    }else
    {
        _setBgView.hidden = YES;
        _getBgView.hidden = NO;
        
        LKDBHelper* globalHelper = [PeriodicModel getUsingLKDBHelper];
        
        //异步 asynchronous
        [globalHelper search:[PeriodicModel class] where:nil orderBy:@"updateTime desc" offset:0 count:50 callback:^(NSMutableArray *array) {
            for (PeriodicModel *model in array) {
                NSLog(@"%@ -- %@",model.name,model.updateTime);
            }
            _dataArray.count?([_dataArray removeAllObjects]):nil;
            [_dataArray addObjectsFromArray:array];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_tableView reloadData];
            });
            
            
        }];
        
    }
    
    
}


- (void)checkButton:(UIButton *)sender
{
    [super checkButton:sender];
    
    UISwitch *enableSwitch = (UISwitch *)[self.view viewWithTag:300];
    
    if (self.setGetSegmet.selectedSegmentIndex == 0) {
        NSString *body = nil;
        if (self.numberField.text.length) {
//            SET_PW_#SWPERIODIC#1#PERIODICTIME#2# PERIODICPHONE#13600000000#
            body = [NSString stringWithFormat:@"SET%@#PERIODICPHONE#%@#SWPERIODIC#%d#PERIODICTIME#%ld#",kMessagePassword,self.numberField.text,enableSwitch.on,(long)lroundf(self.volumeSlider.value)];
////#warning <#message#>
//            
//            PeriodicModel *model = [[PeriodicModel alloc]init];
//            model.name = [NSString stringWithFormat:@"%@\n%@",NSLocalizedStringFromTable(@"New Number For Verification", LanguageFileName, nil),self.numberField.text];
//            model.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
//            [model saveToDB];
//            
//            PeriodicModel *model1 = [[PeriodicModel alloc]init];
//            model1.name = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTable(@"Periodic Device Check", LanguageFileName, nil),[kUserDef boolForKey:kPeriodicEnable]?NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil):NSLocalizedStringFromTable(@"Disable", LanguageFileName, nil)];
//            model1.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
//            [model1 saveToDB];
//            
//            PeriodicModel *model2 = [[PeriodicModel alloc]init];
//            model2.name = [NSString stringWithFormat:@"%@ %ld %@",NSLocalizedStringFromTable(@"Time Range", LanguageFileName, nil),lroundf(self.volumeSlider.value),NSLocalizedStringFromTable(@"days", LanguageFileName, nil)];
//            model2.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
//            [model2 saveToDB];
            

            
        }else
        {
            body = [NSString stringWithFormat:@"SET%@#SWPERIODIC#%d#PERIODICTIME#%ld#",kMessagePassword,enableSwitch.on,(long)lroundf(self.volumeSlider.value)];
        }
        [kUserDef setObject:self.numberField.text forKey:@"periodicnumber"];
        [kUserDef synchronize];
        
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Set Number For Verification", LanguageFileName, nil) body:body withDelegate:self];
    }else{
        NSString *body = [NSString stringWithFormat:@"GET%@#SWLOWBATT#",kMessagePassword];
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Get Number For Verification", LanguageFileName, nil) body:body withDelegate:self];
    }
    
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    switch (result) {
        case MessageComposeResultSent:{
            if (self.numberField.text.length) {
                //            SET_PW_#SWPERIODIC#1#PERIODICTIME#2# PERIODICPHONE#13600000000#
                PeriodicModel *model = [[PeriodicModel alloc]init];
                model.name = [NSString stringWithFormat:@"%@\n%@",NSLocalizedStringFromTable(@"New Number For Verification", LanguageFileName, nil),self.numberField.text];
                model.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
                [model saveToDB];
            }
                PeriodicModel *model1 = [[PeriodicModel alloc]init];
                model1.name = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTable(@"Periodic device check", LanguageFileName, nil),[kUserDef boolForKey:kPeriodicEnable]?NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil):NSLocalizedStringFromTable(@"Disable", LanguageFileName, nil)];
                model1.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
                [model1 saveToDB];
                
                PeriodicModel *model2 = [[PeriodicModel alloc]init];
                model2.name = [NSString stringWithFormat:@"%@ %ld %@",NSLocalizedStringFromTable(@"Time range changed to", LanguageFileName, nil),lroundf(self.volumeSlider.value),NSLocalizedStringFromTable(@"days", LanguageFileName, nil)];
                model2.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
                [model2 saveToDB];
            
            //信息传送成功
            NSLog(@"信息传送成功");
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case MessageComposeResultFailed:
            //信息传送失败
            NSLog(@"信息传送失败");
            [self.navigationController popViewControllerAnimated:YES];
            
            break;
        case MessageComposeResultCancelled:
            //信息被用户取消传送
            NSLog(@"信息被用户取消传送");
            break;
        default:
            break;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
