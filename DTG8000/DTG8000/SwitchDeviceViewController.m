//
//  SwitchDeviceViewController.m
//  Thomson
//
//  Created by 何助金 on 10/20/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "SwitchDeviceViewController.h"
#import "SwitchDeviceTableViewCell.h"
#import "ContactModel.h"
@interface SwitchDeviceViewController ()<UITableViewDataSource,UITableViewDelegate,SwitchDeviceTableViewCellDelegate,UIAlertViewDelegate>
{

    UITableView *_tableView;
    NSMutableArray *_dataArray;
}
@end

@implementation SwitchDeviceViewController

- (void)viewDidLoad {
    _dataArray = [NSMutableArray array];
    [super viewDidLoad];
    [self addTableView];
    
    LKDBHelper* globalHelper = [ContactModel getUsingLKDBHelper];

    //异步 asynchronous
    [globalHelper search:[ContactModel class] where:nil orderBy:nil offset:0 count:200 callback:^(NSMutableArray *array) {
        for (ContactModel *model in array) {
            NSLog(@"password?%@",model.password);
        }
        [_dataArray addObjectsFromArray:array];
    }];
    
    //TEST
//    for(int i = 0 ; i < 2 ; i ++) {
//        ContactModel *dummy = [[ContactModel alloc] init];
//        dummy.name = [NSString stringWithFormat: @"Natan %i", i + 1];
//        dummy.number = @"123";
//        dummy.password = @"zaq12345";
//        dummy.isOpen = (i % 2 == 0);
//        [_dataArray addObject: dummy];
//    }
}


- (void) addTableView
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0 ,kScreenWidth, kScreenHeight- 64) style:UITableViewStylePlain];
    
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.separatorColor = kGrayColor;
    [self.view addSubview:_tableView];
}
#pragma mark -tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SwitchDeviceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SwitchDeviceTableViewCell" owner:self options:nil] lastObject];
        cell.textLabel.textColor = kBlackLightColor;

        UIView *lineView = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2*kLeftMark, 1)];
        [cell.contentView addSubview:lineView];
        lineView.x = kLeftMark;
        lineView.y = 43;
        
        UIView *lineView1 = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(1 ,24)];
        lineView1.x = kScreenWidth - 42-7;
        lineView1.y = 10;
        [cell.contentView addSubview:lineView1];
    }
    ContactModel *model = _dataArray[indexPath.row];
    cell.nameLable.text = model.name;
    cell.delegate = self;
    cell.indexPath = indexPath;
    
    NSLog(@"ssss%@----%@",kMessageName,cell.nameLable.text);
    
    
    if([kMessageName isEqualToString:cell.nameLable.text])
    {
        [cell.changeButton setImage:[UIImage imageNamed:@"hand_g"] forState:nil];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
static NSIndexPath *indexPath = nil;

- (void)deleteDeviceWithTableViewCell:(SwitchDeviceTableViewCell *)cell{
    indexPath = cell.indexPath;
    
    NSString *message = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTable(@"Are you sure to delete", LanguageFileName, nil),cell.nameLable.text];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedStringFromTable(@"Delete Device", LanguageFileName, nil) message:message delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"Cancel", LanguageFileName, nil) otherButtonTitles:NSLocalizedStringFromTable(@"Sure", LanguageFileName, nil), nil];
    alert.tag = 1000;
    [alert show];
}

- (void)changeDeviceWithTableViewCell:(SwitchDeviceTableViewCell *)cell
{   indexPath = cell.indexPath;
    NSString *message = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTable(@"Are you sure change to", LanguageFileName, nil),cell.nameLable.text];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedStringFromTable(@"Change Device", LanguageFileName, nil) message:message delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"Cancel", LanguageFileName, nil) otherButtonTitles:NSLocalizedStringFromTable(@"Sure", LanguageFileName, nil), nil];
    alert.tag = 1001;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1000) {
       
        if (buttonIndex == 0) {
            
        }else
        {
            ContactModel *model = _dataArray[indexPath.row];
            NSString *number = model.number;

            [ContactModel deleteToDB:model];
            [_dataArray removeObjectAtIndex:indexPath.row];
            [_tableView deleteRowsAtIndexPaths:@[indexPath]withRowAnimation:UITableViewRowAnimationFade];
          
            [_tableView reloadData];
            
            //正好删除当前使用的设备提示指定新设备
            if ([number isEqualToString:kMessageNumber]) {
                kMessageNumber = @"";
                kMessagePassword = @"";
                kMessageName = @"";
                
                [kUserDef removeObjectForKey:kSendNumber];
                [kUserDef removeObjectForKey:kSendPassword];
                [kUserDef removeObjectForKey:kSendPhoneName];
                [kUserDef synchronize];
                

                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil
                                                               message:NSLocalizedStringFromTable(@"Please choose new device", LanguageFileName, nil)delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedStringFromTable(@"Sure", LanguageFileName, nil), nil];

                [alert show];

            }
        }
    }else
    {
        if (buttonIndex == 0) {
            
        }else
        {
            //更新发送信息的号码
            ContactModel *model = _dataArray[indexPath.row];
            kMessageNumber = model.number;
            kMessagePassword = model.password;
            kMessageName = model.name;
            
            [kUserDef setObject:model.number forKey:kSendNumber];
            [kUserDef setObject:model.password forKey:kSendPassword];
            [kUserDef setObject:model.name forKey:kSendPhoneName];
            [kUserDef synchronize];
             [_tableView reloadData];
            
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
