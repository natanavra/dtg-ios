//
//  SwitchDeviceTableViewCell.h
//  Thomson
//
//  Created by 何助金 on 10/25/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//
@class SwitchDeviceTableViewCell;
@protocol SwitchDeviceTableViewCellDelegate <NSObject>

- (void)changeDeviceWithTableViewCell:(SwitchDeviceTableViewCell *)cell;
- (void)deleteDeviceWithTableViewCell:(SwitchDeviceTableViewCell *)cell;
@end


#import <UIKit/UIKit.h>

@interface SwitchDeviceTableViewCell : UITableViewCell
@property(nonatomic,weak) id <SwitchDeviceTableViewCellDelegate>delegate;
@property (weak, nonatomic) IBOutlet UIButton *changeButton;

@property (nonatomic,strong)NSIndexPath *indexPath;
- (IBAction)delDeviceButton:(UIButton *)sender;
- (IBAction)chooseDeviceButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;

@end
