//
//  RegisterViewController.m
//  Thomson
//
//  Created by 何助金 on 10/18/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "RegisterViewController.h"
#import "HomeViewController.h"

#define kLeftMark 30*kScale

@interface RegisterViewController ()
{
    UITextField *_name;
    UITextField *_userName;
    UITextField *_email;
    UITextField *_password;
    UIButton *_registerButton;
    
}
@property (nonatomic, strong) UIScrollView *scrollview;

@end

@implementation RegisterViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createViews];
    
}

- (void)createViews{
    self.scrollview = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    self.scrollview.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.scrollview];
    
    UIImageView *headImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 353*320/414)];
    headImageView.backgroundColor = KClearColor;
    headImageView.image = [UIImage imageNamed:@"headView"];
    [self.scrollview addSubview:headImageView];
    
    
    UILabel *title = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark, kScreenHeight - 350*kScale+10, kScreenWidth - 2*kLeftMark, 30) backgroundColor:KClearColor text:NSLocalizedStringFromTable(@"REGISTRATION", LanguageFileName, nil) textColor:kBlackLightColor font:kHelveticaLightFont(16) textAlignment:NSTextAlignmentLeft lineBreakMode:NSLineBreakByWordWrapping numberOfLines:0];
    [self.scrollview addSubview:title];
    
    _name = [[ObjectCTools shared] getACustomTextFiledFrame:CGRectMake(kLeftMark, title.bottom + 15, (kScreenWidth - 2*kLeftMark), 35)
                                                         backgroundColor:[UIColor clearColor]
                                                             placeholder:NSLocalizedStringFromTable(@"Name", LanguageFileName, nil)
                                                               textColor:kHexRGB(0x054f7a)
                                                                    font:kHelveticaRegularFont(15)
                                                             borderStyle:UITextBorderStyleRoundedRect
                                                           textAlignment:NSTextAlignmentLeft
                                                      accessibilityLabel:nil
                                                      autocorrectionType:UITextAutocorrectionTypeDefault
                                                         clearButtonMode:UITextFieldViewModeWhileEditing
                                                                     tag:100
                                                          withIsPassword:NO];
    _name.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, _name.height)];
    _name.leftViewMode = UITextFieldViewModeAlways;
    _name.tintColor = [UIColor blackColor];
    [_scrollview addSubview:_name];
    
    _userName = [[ObjectCTools shared] getACustomTextFiledFrame:CGRectMake(kLeftMark, _name.bottom + 10, (kScreenWidth - 2*kLeftMark), 35)
                                            backgroundColor:[UIColor clearColor]
                                                placeholder:NSLocalizedStringFromTable(@"UserName", LanguageFileName, nil)
                                                  textColor:kHexRGB(0x054f7a)
                                                       font:kHelveticaRegularFont(15)
                                                borderStyle:UITextBorderStyleRoundedRect
                                              textAlignment:NSTextAlignmentLeft
                                         accessibilityLabel:nil
                                         autocorrectionType:UITextAutocorrectionTypeDefault
                                            clearButtonMode:UITextFieldViewModeWhileEditing
                                                        tag:101
                                             withIsPassword:NO];
    _userName.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, _name.height)];
    _userName.leftViewMode = UITextFieldViewModeAlways;
    _userName.tintColor = [UIColor blackColor];
    [_scrollview addSubview:_userName];
    
    
    _email = [[ObjectCTools shared] getACustomTextFiledFrame:CGRectMake(kLeftMark, _userName.bottom + 10, (kScreenWidth - 2*kLeftMark), 35)
                                            backgroundColor:[UIColor clearColor]
                                                placeholder:NSLocalizedStringFromTable(@"Email", LanguageFileName, nil)
                                                  textColor:kHexRGB(0x054f7a)
                                                       font:kHelveticaRegularFont(15)
                                                borderStyle:UITextBorderStyleRoundedRect
                                              textAlignment:NSTextAlignmentLeft
                                         accessibilityLabel:nil
                                         autocorrectionType:UITextAutocorrectionTypeDefault
                                            clearButtonMode:UITextFieldViewModeWhileEditing
                                                        tag:102
                                             withIsPassword:NO];
    _email.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, _email.height)];
    _email.leftViewMode = UITextFieldViewModeAlways;
    _email.tintColor = [UIColor blackColor];
    [_scrollview addSubview:_email];
    
    _password = [[ObjectCTools shared] getACustomTextFiledFrame:CGRectMake(kLeftMark, _email.bottom + 10, (kScreenWidth - 2*kLeftMark), 35)
                                                backgroundColor:[UIColor clearColor]
                                                    placeholder:NSLocalizedStringFromTable(@"Password", LanguageFileName, nil)
                                                      textColor:kHexRGB(0x054f7a)
                                                           font:kHelveticaRegularFont(15)
                                                    borderStyle:UITextBorderStyleRoundedRect
                                                  textAlignment:NSTextAlignmentLeft
                                             accessibilityLabel:nil
                                             autocorrectionType:UITextAutocorrectionTypeDefault
                                                clearButtonMode:UITextFieldViewModeWhileEditing
                                                            tag:103
                                                 withIsPassword:YES];
    _password.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, _password.height)];
    _password.leftViewMode = UITextFieldViewModeAlways;
    _password.tintColor = [UIColor blackColor];
    [_scrollview addSubview:_password];

    _registerButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake(kLeftMark, _password.bottom + 15, (kScreenWidth - kLeftMark * 2), 45)
                                                                        backgroudColor:kRedColor
                                                                      titleNormalColor:[UIColor whiteColor]
                                                                 titleHighlightedColor:[UIColor grayColor]
                                                                                 title:NSLocalizedStringFromTable(@"REGISTER", LanguageFileName, nil)
                                                                                  font:kHelveticaLightFont(20)
                                                                          cornerRadius:5
                                                                           borderWidth:0.5
                                                                           borderColor:kRedColor.CGColor
                                                                    accessibilityLabel:nil];
    [_registerButton addTarget: self action:@selector(registerButton:) forControlEvents:UIControlEventTouchUpInside];
    [_scrollview addSubview:_registerButton];
    
    if (_registerButton.bottom + 40 > kScreenHeight) {
        _scrollview.contentSize = CGSizeMake(kScreenWidth, _registerButton.bottom + 40);
    }

}

- (void)registerButton:(UIButton *)sender
{
    BOOL ok = YES;
    if (_name.text.length<1) {
        [_name shake];
        ok = NO;
    }
    if (_userName.text.length<1) {
        [_userName shake];
        ok = NO;
    }
    if (![[ObjectCTools shared] checkEmail:_email.text]) {
        [_email shake];
        ok = NO;
    }
    if (_password.text.length<6) {
        [_password shake];
        ok = NO;
    }
    
    if (ok) {
        HomeViewController *homeVC = [[HomeViewController alloc]init];
        [self.navigationController pushViewController:homeVC animated:YES];
        [kUserDef setObject:_userName.text forKey:kUserName];
        [kUserDef setObject:_password.text forKey:kPassword];
        [kUserDef setObject:kHadAccount forKey:kHadAccount];
        [kUserDef synchronize];
        
    }else
    {
        return;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
