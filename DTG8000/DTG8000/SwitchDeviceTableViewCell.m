//
//  SwitchDeviceTableViewCell.m
//  Thomson
//
//  Created by 何助金 on 10/25/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "SwitchDeviceTableViewCell.h"

@implementation SwitchDeviceTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Configure the view for the selected state
}

- (IBAction)delDeviceButton:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(deleteDeviceWithTableViewCell:)]) {
        [self.delegate deleteDeviceWithTableViewCell:self];
    }
}

- (IBAction)chooseDeviceButton:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(changeDeviceWithTableViewCell:)]) {
        
        [self.delegate changeDeviceWithTableViewCell:self];
    }
}

- (void)setIndexPath:(NSIndexPath *)indexPath
{
    _indexPath = indexPath;
}
@end
