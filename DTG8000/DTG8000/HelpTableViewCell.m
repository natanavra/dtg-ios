//
//  HelpTableViewCell.m
//  Thomson
//
//  Created by 何助金 on 10/25/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "HelpTableViewCell.h"

@implementation HelpTableViewCell

- (void)awakeFromNib {
    // Initialization code
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.textLabel.textColor = kRedColor;
        self.textLabel.font = kHelveticaLightFont(16);
        self.detailTextLabel.font = kHelveticaLightFont(12);
        self.detailTextLabel.textColor = kBlackLightColor;
        UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2*15, 1)];
        line.x = 15;
        line.y = self.contentView.bottom-1;
        line.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        line.tag = 99;
        [self.contentView addSubview:line];

        self.imageView.image = [UIImage imageNamed:@"add"];
        self.imageView.contentMode = UIViewContentModeCenter;
        self.detailTextLabel.numberOfLines = 0;


    }
    return self;
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    self.imageView.x = 15;
    self.imageView.y = 20;
    self.imageView.bounds = CGRectMake(0, 0, 20, 20);
    self.textLabel.x =  self.imageView.right + 10;
    self.textLabel.y =  self.imageView.y;
    self.textLabel.height = 20;
    self.detailTextLabel.x = self.textLabel.x;
    self.detailTextLabel.y = self.textLabel.bottom + 15;
    self.detailTextLabel.width = kScreenWidth - 60;
    
    
    if (!_isOpen) {
        self.textLabel.textColor = kRedColor;
        self.imageView.image = [UIImage imageNamed:@"add"];
    }else
    {
        self.textLabel.textColor = kBlackLightColor;
        self.imageView.image = [UIImage imageNamed:@"sub"];
    }
    

}

- (void)setIsOpen:(BOOL)isOpen
{
    _isOpen = isOpen;
    if (!_isOpen) {
        self.textLabel.textColor = kRedColor;
        self.imageView.image = [UIImage imageNamed:@"add"];
    }else
    {
        self.textLabel.textColor = kBlackLightColor;
        self.imageView.image = [UIImage imageNamed:@"sub"];
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
