//
//  BaseEventViewController.h
//  Thomson
//
//  Created by 何助金 on 10/20/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseEventViewController : BaseViewController
@property (nonatomic, strong) NSString *iconName;
@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) NSString *titleString;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *checkButton;
@property (nonatomic, strong) UISegmentedControl *setGetSegmet;

- (void)checkButton:(UIButton *)sender;
- (void)setGetSegmet:(UISegmentedControl *)sender;

@end
