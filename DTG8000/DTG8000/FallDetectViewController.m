//
//  FallDetectViewController.m
//  Thomson
//
//  Created by 何助金 on 10/22/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "FallDetectViewController.h"
#import "GetMNumberTableViewCell.h"
#import "HistoryTableViewCell.h"
#import "FallDetectModel.h"
#define kFall @"FallEnable"

@interface FallDetectViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    UIView *_setBgView;
    UIView *_getBgView;
    UITableView *_tableView;
    NSMutableArray *_dataArray;
}


@end

@implementation FallDetectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArray = [NSMutableArray array];

    _setBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 -64)];
    _setBgView.backgroundColor = kWhiteColor;
    _setBgView.hidden = NO;
    [self.view addSubview:_setBgView];
    
    _getBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 - 64)];
    _getBgView.backgroundColor = kWhiteColor;
    _getBgView.hidden = YES;
    [self.view addSubview:_getBgView];

    UILabel *infoLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,0, kScreenWidth/2 - kLeftMark , 35)
                                                     backgroundColor:[UIColor clearColor]
                                                                text:NSLocalizedStringFromTable(@"Fall Detect", LanguageFileName, nil)
                                                           textColor:kBlackLightColor
                                                                font:kHelveticaLightFont(16)
                                                       textAlignment:NSTextAlignmentLeft
                                                       lineBreakMode:NSLineBreakByWordWrapping
                                                       numberOfLines:0];
    
    infoLabel.adjustsFontSizeToFitWidth = YES;
    [_setBgView addSubview: infoLabel];
    
    UILabel *enableLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kScreenWidth/2.0,0, kScreenWidth/2 - kLeftMark - 55 , 35)
                                                     backgroundColor:[UIColor clearColor]
                                                                text:NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil)
                                                           textColor:kBlackLightColor
                                                                font:kHelveticaLightFont(13)
                                                       textAlignment:NSTextAlignmentRight
                                                       lineBreakMode:NSLineBreakByWordWrapping
                                                       numberOfLines:0];
    
    enableLabel.adjustsFontSizeToFitWidth = YES;
    [_setBgView addSubview:enableLabel];

    
    UISwitch *enableSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(kScreenWidth - kLeftMark - 50,5, 50 , 20)];
    enableSwitch.onTintColor = kRedColor;
    
    [enableSwitch addTarget:self action:@selector(enableSwitch:) forControlEvents:UIControlEventValueChanged];
    enableSwitch.on = [kUserDef boolForKey:kFall];
    enableSwitch.tag = 300;
    [_setBgView addSubview:enableSwitch];

    enableSwitch.centerY = infoLabel.centerY;
    enableLabel.centerY = infoLabel.centerY;
    
    UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2 *kLeftMark, 1.0)];
    line.y = infoLabel.bottom + 10;
    line.x = kLeftMark;
    [_setBgView addSubview:line];
    
    //Number
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(kLeftMark, line.bottom + 10, kScreenWidth - 2 *kLeftMark, 80)];
    bgView.layer.cornerRadius = 3;
    bgView.layer.backgroundColor = kGrayLightColor.CGColor;
    bgView.layer.borderColor = KClearColor.CGColor;
    bgView.layer.masksToBounds = YES;
    [_setBgView addSubview:bgView];
    
    UILabel *titleLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(5 ,5, bgView.width - 10, 35)
                                                      backgroundColor:[UIColor clearColor]
                                                                 text:NSLocalizedStringFromTable(@"Fall Detect Alert Number", LanguageFileName, nil)
                                                            textColor:kBlackLightColor
                                                                 font:kHelveticaLightFont(16)
                                                        textAlignment:NSTextAlignmentLeft
                                                        lineBreakMode:NSLineBreakByWordWrapping
                                                        numberOfLines:0];
    
    titleLabel.adjustsFontSizeToFitWidth = YES;
    [bgView addSubview:titleLabel];
    
    self.numberField = [[ObjectCTools shared]getACustomTextFiledFrame:CGRectMake(5, titleLabel.bottom , bgView.width - 2*5 - 55, 35)
                                                      backgroundColor:kWhiteColor placeholder:NSLocalizedStringFromTable(@"Insert phone number", LanguageFileName, nil)
                                                            textColor:kBlackLightColor
                                                                 font:kHelveticaLightFont(16)
                                                          borderStyle:UITextBorderStyleRoundedRect
                                                        textAlignment:NSTextAlignmentLeft
                                                   accessibilityLabel:nil autocorrectionType:UITextAutocorrectionTypeDefault clearButtonMode:UITextFieldViewModeWhileEditing tag:100 withIsPassword:NO];
    self.numberField.delegate = self;
    self.numberField.keyboardType = UIKeyboardTypeNumberPad;
    self.numberField.text = [kUserDef objectForKey:@"fallnumer"];
    [bgView addSubview:self.numberField];
    
    self.editButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake(self.numberField.right + 5, _numberField.y  ,50, 35)
                                                                backgroudColor:kRedColor
                                                              titleNormalColor:[UIColor whiteColor]
                                                         titleHighlightedColor:[UIColor grayColor]
                                                                         title:NSLocalizedStringFromTable(@"Set", LanguageFileName, nil)
                                                                          font:kHelveticaLightFont(14)
                                                                  cornerRadius:5
                                                                   borderWidth:0.5
                                                                   borderColor:KClearColor.CGColor
                                                            accessibilityLabel:nil];
    [self.editButton addTarget: self action:@selector(editButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [bgView addSubview:self.editButton];
    self.isEdit = YES;

    
    
    //
    UILabel *infoLabel1 = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,bgView.bottom+30, kScreenWidth - 2*kLeftMark , 35)
                                                     backgroundColor:[UIColor clearColor]
                                                                text:NSLocalizedStringFromTable(@" Sensitivity step", LanguageFileName, nil)
                                                           textColor:kBlackLightColor
                                                                font:kHelveticaLightFont(16)
                                                       textAlignment:NSTextAlignmentLeft
                                                       lineBreakMode:NSLineBreakByWordWrapping
                                                       numberOfLines:0];
    
    infoLabel1.adjustsFontSizeToFitWidth = YES;
    
    [_setBgView addSubview: infoLabel1];
    
    NSArray *titleArray = @[NSLocalizedStringFromTable(@" low", LanguageFileName, nil),NSLocalizedStringFromTable(@" normal", LanguageFileName, nil),NSLocalizedStringFromTable(@" high", LanguageFileName, nil)];
    int n = 0;
    for (int i = 0 ; i < 3; ++i) {
        
            
            UIButton *ringSceneButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake( kLeftMark +i* (kScreenWidth/3), infoLabel1.bottom+ 40, kScreenWidth/3 - 2*kLeftMark, 35)
                                                                                  backgroudColor:KClearColor
                                                                                titleNormalColor:kBlackLightColor
                                                                           titleHighlightedColor:[UIColor grayColor]
                                                                                           title:titleArray[n ]
                                                                                            font:kHelveticaLightFont(11)
                                                                                    cornerRadius:0
                                                                                     borderWidth:0
                                                                                     borderColor:KClearColor.CGColor
                                                                              accessibilityLabel:nil];
            [ringSceneButton addTarget: self action:@selector(ringSceneButton:) forControlEvents:UIControlEventTouchUpInside];
            [ringSceneButton setImage:[UIImage imageNamed:@"point"] forState:UIControlStateNormal];
            [ringSceneButton setImage:[UIImage imageNamed:@"point_p"] forState:UIControlStateSelected];
            ringSceneButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [ringSceneButton setContentEdgeInsets:(UIEdgeInsetsMake(0, 0, 0, 0))];
            ringSceneButton.tag = 200 + n;
            [kUserDef integerForKey:@"FALLSENS"];
            if (n== [kUserDef integerForKey:@"FALLSENS"]) {
                ringSceneButton.selected = YES;
            }
            n ++;
            [_setBgView  addSubview:ringSceneButton];
            
            if (n == 4) {
                UIView *lineView = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2*kLeftMark, 1)];
                lineView.y = ringSceneButton.bottom + 5;
                lineView.x = kLeftMark;
                [_setBgView addSubview:lineView];
            }
            
        
        
    }
    
    
    
    [self addTableView];
}


- (void) addTableView
{
    _tableView = [[UITableView alloc] initWithFrame:_getBgView.bounds style:UITableViewStylePlain];
    
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    _tableView.separatorColor = [UIColor grayColor];
    [_getBgView addSubview:_tableView];
}
#pragma mark -tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1 + _dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0){
        
        GetMNumberTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GetMNumberTableViewCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"GetMNumberTableViewCell" owner:self options:nil] lastObject];
        }
        
        cell.mlable.text = NSLocalizedStringFromTable(@"Update History", LanguageFileName, nil);
        cell.numberLable.text = nil;
        return cell;
        
    }else
    {
        HistoryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"HistoryTableViewCell" owner:self options:nil] lastObject];
            UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2 *15, 1.0)];
            [cell.contentView addSubview:line];
            line.y = 29;
            line.x = 15;
        }
        
        if (_dataArray.count) {
            
            FallDetectModel *model = _dataArray[indexPath.row - 1];
            cell.mLable.text = model.name;
            cell.dateLabel.text = [DateHelper timeSPToTimeStringWithString:model.updateTime];
            
        }else {
            cell.mLable.text = @"";
            cell.dateLabel.text = @"";
        }
        return cell;
    }
    
}


- (void)editButton:(UIButton *)sender{
    
    if (_isEdit) {
        //send set low battery warning number
        
        if (self.numberField.text.length) {
            NSLog(@"set Low Battery number");
            
            [self.editButton setTitle:NSLocalizedStringFromTable(@"Change", LanguageFileName, nil) forState:UIControlStateNormal];
            [self.editButton setBackgroundColor:kGrayDarkColor];
            [self.numberField resignFirstResponder];
            //            NSString *body = [NSString stringWithFormat:@"SET9999#SWECI#%d#",enableSwitch.on];
            //            [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Set Detect", LanguageFileName, nil) body:body withDelegate:self];
            
            
        }else
        {
            [self.numberField shake];
            return;
        }
    }else
    {//change number
        NSLog(@"change  number");
        if (self.numberField.text.length) {
            [self.editButton setTitle:NSLocalizedStringFromTable(@"Set", LanguageFileName, nil) forState:UIControlStateNormal];
            [self.editButton setBackgroundColor:kRedColor];
            [self.numberField becomeFirstResponder];
            
        }
        
    }
    
    self.isEdit = !_isEdit;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField.text.length) {
        self.isEdit = YES;
    }
    return YES;
}

- (void)setIsEdit:(BOOL)isEdit
{
    _isEdit = isEdit;
    if (isEdit) {
        [self.editButton setBackgroundColor:kRedColor];
    }else
    {
        [self.editButton setBackgroundColor:kGrayDarkColor];
        
    }
    
    [kUserDef setObject:self.numberField.text forKey:@"fallnumer"];
    [kUserDef synchronize];
}


- (void)enableSwitch:(UISwitch *)sender
{
    if (sender.on) {
        [[ObjectCTools shared] showAlertViewAndDissmissAutomatic:NSLocalizedStringFromTable(@"Fall Detect", LanguageFileName, nil) andMessage:NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil) withDissmissTime:KDissmissTime withDelegate:nil withAction:nil];
    }
    
    [kUserDef setBool:sender.on forKey:kFall];
    [kUserDef synchronize];
}

#pragma mark 切换 SET GET

- (void)setGetSegmet:(UISegmentedControl *)sender
{
    [super setGetSegmet:sender];
    if (sender.selectedSegmentIndex == 0) {
        _setBgView.hidden = NO;
        _getBgView.hidden = YES;
    }else
    {
        _setBgView.hidden = YES;
        _getBgView.hidden = NO;
        
        LKDBHelper* globalHelper = [FallDetectModel getUsingLKDBHelper];
        
        //异步 asynchronous
        [globalHelper search:[FallDetectModel class] where:nil orderBy:@"updateTime desc" offset:0 count:50 callback:^(NSMutableArray *array) {
            for (FallDetectModel *model in array) {
                NSLog(@"%@ -- %@",model.name,model.updateTime);
            }
            _dataArray.count?([_dataArray removeAllObjects]):nil;
            [_dataArray addObjectsFromArray:array];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_tableView reloadData];
            });
            
            
        }];
        
    }
 

}

- (void)checkButton:(UIButton *)sender
{
    [super checkButton:sender];

    UISwitch *enableSwitch = (UISwitch *)[self.view viewWithTag:300];
    
    if (self.setGetSegmet.selectedSegmentIndex == 0) {
        NSString *body = nil;
        
        if (self.numberField.text.length) {
            
            body =  [NSString stringWithFormat:@"SET%@#SWFALL#%d#FALLPHONE#%@#FALLSENS#%d#",kMessagePassword,enableSwitch.on,self.numberField.text,ringScene];
        }else
        {
            body = [NSString stringWithFormat:@"SET%@#SWFALL#%d#FALLSENS#%d#",kMessagePassword,enableSwitch.on,ringScene];
        }
        NSLog(@"sms:%@",body);
        
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Set Detect", LanguageFileName, nil) body:body withDelegate:self];

        [kUserDef setObject:self.numberField.text forKey:@"fallnumer"];
        [kUserDef synchronize];
       
    }else{
        NSString *body = [NSString stringWithFormat:@"GET%@#SWFALL#FALLPHONE#FALLSENS#",kMessagePassword];
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Get Detect", LanguageFileName, nil) body:body withDelegate:self];
    }

}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    switch (result) {
        case MessageComposeResultSent:
        {//信息传送成功
            NSLog(@"信息传送成功");
            UISwitch *enableSwitch = (UISwitch *)[self.view viewWithTag:300];

            FallDetectModel *model = [[FallDetectModel alloc]init];
            model.name = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTable(@"Fall Detect", LanguageFileName, nil),enableSwitch.on ? NSLocalizedStringFromTable(@"Enabled", LanguageFileName, nil):NSLocalizedStringFromTable(@"Disabled", LanguageFileName, nil)];
                        model.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
            [model saveToDB];
            
            if (self.numberField.text.length) {
                
                FallDetectModel *model1 = [[FallDetectModel alloc]init];
                model1.name = [NSString stringWithFormat:@"%@:%@",NSLocalizedStringFromTable(@"Fall Detect Number", LanguageFileName, nil),self.numberField.text];
                model1.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
                [model1 saveToDB];
                
            }
            
            NSString *ringSceneSTR = nil;
            if(ringScene == 0)
            {
                ringSceneSTR = @"low";
            }
            else if(ringScene == 1)
            {
                ringSceneSTR = @"normal";
            }
            else if(ringScene == 2)
            {
                ringSceneSTR = @"high";
            }
            
            
            FallDetectModel *model2 = [[FallDetectModel alloc]init];
            model2.name = [NSString stringWithFormat:@"%@:%@",NSLocalizedStringFromTable(@"Sensitivity step", LanguageFileName, nil),ringSceneSTR];
            model2.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
            [model2 saveToDB];
            
            

            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case MessageComposeResultFailed:
            //信息传送失败
            NSLog(@"信息传送失败");
            [self.navigationController popViewControllerAnimated:YES];

            break;
        case MessageComposeResultCancelled:
            //信息被用户取消传送
            NSLog(@"信息被用户取消传送");
            break;
        default:
            break;
    }
    
}


static int ringScene = 0;
- (void)ringSceneButton:(UIButton *)sender{
    
    for (int i = 0 ; i < 3 ; ++i) {
        UIButton *button  = (UIButton *)[_setBgView viewWithTag:200 + i];
        if (button.tag == sender.tag ) {
            button.selected = YES;
            ringScene = i;
            [[ObjectCTools shared] showAlertViewAndDissmissAutomatic:NSLocalizedStringFromTable(@"Sensitivity step", LanguageFileName, nil) andMessage:button.titleLabel.text withDissmissTime:KDissmissTime withDelegate:nil withAction:nil];
            [kUserDef setInteger:i forKey:@"FALLSENS"];
            [kUserDef synchronize];
        }else{
            button.selected = NO;
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
