//
//  ContactModel.m
//  Thomson
//
//  Created by 何助金 on 10/26/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "ContactModel.h"

@implementation ContactModel
+(NSString *)getTableName
{
    return @"LKContactTable";
}

//主键
+(NSString *)getPrimaryKey
{
    return @"name";
}
///复合主键  这个优先级最高
+(NSArray *)getPrimaryKeyUnionArray
{
    return @[@"name",@"number"];
}
//重载选择 使用的LKDBHelper
+(LKDBHelper *)getUsingLKDBHelper
{
    static LKDBHelper* db;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //        NSString* dbpath = [NSHomeDirectory() stringByAppendingPathComponent:@"asd/asd.db"];
        //        db = [[LKDBHelper alloc]initWithDBPath:dbpath];
        //or
        db = [[LKDBHelper alloc]init];
    });
    return db;
}
// 将要插入数据库
+(BOOL)dbWillInsert:(NSObject *)entity
{
    LKErrorLog(@"will insert : %@",NSStringFromClass(self));
    return YES;
}
//已经插入数据库
+(void)dbDidInserted:(NSObject *)entity result:(BOOL)result
{
    LKErrorLog(@"did insert : %@",NSStringFromClass(self));
}


@end
