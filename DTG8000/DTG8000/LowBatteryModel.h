//
//  LowBatteryModel.h
//  Thomson
//
//  Created by 何助金 on 11/9/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LowBatteryModel : NSObject
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *number;
@property (nonatomic,strong) NSString *updateTime;
@end
