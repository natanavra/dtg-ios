//
//  SwitchDevicTableViewCell.m
//  Thomson
//
//  Created by 何助金 on 10/20/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "SwitchDevicTableViewCell.h"

@implementation SwitchDevicTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
