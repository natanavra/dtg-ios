//
//  ValueView.m
//  Thomson
//
//  Created by 何助金 on 10/25/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "ValueView.h"

@implementation ValueView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.image = [UIImage imageNamed:@"paopao"];
        self.valueLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 1, self.width, self.height - 7.5)];
        self.valueLabel.font = kHelveticaLightFont(12);
        self.valueLabel.textColor = kRedColor;
        [self addSubview:self.valueLabel];
        self.valueLabel.textAlignment = NSTextAlignmentCenter;
    }
    return self;
}

- (void)setValue:(NSString *)value
{
    _value = value;
    self.valueLabel.text = value;
}

@end
