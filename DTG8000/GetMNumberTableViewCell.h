//
//  GetMNumberTableViewCell.h
//  Thomson
//
//  Created by 何助金 on 10/22/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GetMNumberTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mlable;
@property (weak, nonatomic) IBOutlet UILabel *numberLable;

@end
