//
//  ContactModel.h
//  Thomson
//
//  Created by 何助金 on 10/26/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactModel : NSObject
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *number;
@property (nonatomic,strong) NSString *password;
@property (nonatomic,assign) BOOL isOpen;
@end
