//
//  BaseNavigationController.m
//  Payou
//
//  Created by 何助金 on 15/7/14.
//  Copyright (c) 2015年 MissionSky. All rights reserved.
//

#import "BaseNavigationController.h"

@interface BaseNavigationController ()<UIGestureRecognizerDelegate>

@end

@implementation BaseNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.interactivePopGestureRecognizer.enabled = YES;
    self.interactivePopGestureRecognizer.delegate = self;
    [self setNavigationBarApperance];

}

- (void)setNavigationBarApperance
{
    [self.navigationBar setTranslucent:NO];

    UIImage *image = [UIImage imageWithColor:kNavigationBarColor size:CGSizeMake(kScreenWidth, 64) andRoundSize:0];
    [self.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setTintColor:kNavigationBarTitleColor];//设置字体颜色
    
    //隐藏导航返回btn的文字
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];

    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
        

//    //设置title属性
//    NSMutableDictionary *titleAttributes = [NSMutableDictionary dictionaryWithDictionary: [[UINavigationBar appearance] titleTextAttributes]];
//    [titleAttributes setObject:kNavigationTitleFont forKey:NSFontAttributeName];
//    [titleAttributes setObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
//    [[UINavigationBar appearance] setTitleTextAttributes:titleAttributes];
//    
//    NSMutableDictionary *barItemAttributes = [NSMutableDictionary dictionaryWithDictionary: [[UIBarButtonItem appearance] titleTextAttributesForState:UIControlStateNormal]];
//    [barItemAttributes setObject:kNavigationBarItemFont forKey:NSFontAttributeName];
//    [[UIBarButtonItem appearance] setTitleTextAttributes:barItemAttributes forState:UIControlStateNormal];
    
    //设置输入关标颜色
//    [[UITextField appearance] setTintColor:kHexRGB(0xFC3768)];
//    [[UITextView appearance] setTintColor:kHexRGB(0xFC3768)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
