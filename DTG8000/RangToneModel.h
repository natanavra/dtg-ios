//
//  RangToneModel.h
//  Thomson
//
//  Created by 何助金 on 11/11/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RangToneModel : NSObject
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *updateTime;

@end
