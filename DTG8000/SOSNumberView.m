//
//  SOSNumberView.m
//  Thomson
//
//  Created by 何助金 on 10/21/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "SOSNumberView.h"
#define kLeft 5
@implementation SOSNumberView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = kGrayLightColor;
        self.layer.cornerRadius = 3;
        self.layer.borderColor = KClearColor.CGColor;
        
        self.titleLabel = [[ObjectCTools shared]getACustomLableFrame:CGRectMake(kLeft, kLeft, self.width - 2*kLeft, 20)
                                                     backgroundColor:KClearColor
                                                                text:NSLocalizedStringFromTable(@"M1 NUMBER", LanguageFileName, nil)
                                                           textColor:kBlackLightColor
                                                                font:kHelveticaRegularFont
                           (14)
                                                       textAlignment:NSTextAlignmentLeft
                                                       lineBreakMode:NSLineBreakByWordWrapping
                                                       numberOfLines:0];
        [self addSubview:self.titleLabel];
        
        
        
        self.numberLabel = [[ObjectCTools shared]getACustomLableFrame:CGRectMake(kLeft, _titleLabel.bottom + kLeft, self.width - 2*kLeft, 30)
                                                     backgroundColor:KClearColor
                                                                text:nil
                                                           textColor:kBlueColor
                                                                font:kHelveticaRegularFont
                           (14)
                                                       textAlignment:NSTextAlignmentLeft
                                                       lineBreakMode:NSLineBreakByWordWrapping
                                                       numberOfLines:0];
        [self addSubview:self.numberLabel];
        
        self.numberField = [[ObjectCTools shared]getACustomTextFiledFrame:CGRectMake(kLeft, _titleLabel.bottom + kLeft, self.width - 2*kLeft, 30)
                                                          backgroundColor:kWhiteColor placeholder:NSLocalizedStringFromTable(@"Insert phone number", LanguageFileName, nil)
                                                                textColor:kBlackLightColor
                                                                     font:kHelveticaLightFont(14)
                                                              borderStyle:UITextBorderStyleRoundedRect
                                                            textAlignment:NSTextAlignmentLeft
                                                       accessibilityLabel:nil autocorrectionType:UITextAutocorrectionTypeDefault clearButtonMode:UITextFieldViewModeWhileEditing tag:100 withIsPassword:NO];
        self.numberField.delegate = self;
        self.numberField.adjustsFontSizeToFitWidth = YES;
        self.numberField.keyboardType = UIKeyboardTypeNumberPad;
        [self addSubview:self.numberField];
        
        self.editButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake(kLeft, _numberField.bottom + kLeft ,self.width - 2*kLeft, 30)
                                                                                                    backgroudColor:kBlueColor
                                                                                                  titleNormalColor:[UIColor whiteColor]
                                                                                             titleHighlightedColor:[UIColor grayColor]
                                                                                                             title:NSLocalizedStringFromTable(@"Set", LanguageFileName, nil)
                                                                                                              font:kHelveticaLightFont(14)
                                                                                                      cornerRadius:5
                                                                                                       borderWidth:0.5
                                                                                                       borderColor:KClearColor.CGColor
                                                                                                accessibilityLabel:nil];
        [self.editButton addTarget: self action:@selector(editButton:) forControlEvents:UIControlEventTouchUpInside];


        [self addSubview:self.editButton];
        self.isEdit = YES;
        
    }
    return self;
}

- (void)editButton:(UIButton *)sender{
    
    if (_isEdit) {
        //send set
        
        if (self.numberField.text.length) {
            NSLog(@"set M1/M2 number");
            self.numberField.hidden = YES;
            self.numberLabel.hidden = NO;
            self.numberLabel.text = self.numberField.text;

            [self.editButton setTitle:NSLocalizedStringFromTable(@"Change", LanguageFileName, nil) forState:UIControlStateNormal];
            [self.editButton setBackgroundColor:kGrayDarkColor];
            
            if (self.numberLabel.text.length && self.delegate && [self.delegate respondsToSelector:@selector(sosNumberView:)]) {
                [self.delegate sosNumberView:self];
            }
        }else
        {
            [self.numberField shake];
            return;
        }
    }else
    {//change number
        NSLog(@"change M1/M2 number");
        
        if (self.numberField.text.length) {
            self.numberLabel.hidden = YES;
            self.numberField.hidden = NO;
            self.numberField.text = self.numberLabel.text;
            [self.editButton setTitle:NSLocalizedStringFromTable(@"Set", LanguageFileName, nil) forState:UIControlStateNormal];
            [self.editButton setBackgroundColor:kBlueColor];
        }
        
    }
    
    self.isEdit = !_isEdit;
}

//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
//{
//    if (textField.text.length) {
//        self.isEdit = YES;
//    }
//    return YES;
//}
//
- (void)setIsEdit:(BOOL)isEdit
{
    _isEdit = isEdit;
    if (isEdit) {
        [self.editButton setBackgroundColor:kBlueColor];
    }else
    {
        [self.editButton setBackgroundColor:kGrayDarkColor];

    }
}
@end
