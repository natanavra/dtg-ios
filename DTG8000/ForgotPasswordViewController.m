//
//  ForgotPasswordViewController.m
//  Thomson
//
//  Created by 何助金 on 10/18/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "ForgotPasswordViewController.h"

//#define kLeftMark 30*kScale

@interface ForgotPasswordViewController ()
{
    UITextField *_email;
}
@property (nonatomic, strong) UIScrollView *scrollview;

@end

@implementation ForgotPasswordViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createViews];
    
}

- (void)createViews{
    self.scrollview = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    self.scrollview.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.scrollview];
    
    UIImageView *headImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 353*320/414)];
    headImageView.backgroundColor = KClearColor;
    headImageView.image = [UIImage imageNamed:@"headView"];
    [self.scrollview addSubview:headImageView];
    
    
    UILabel *title = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark, kScreenHeight - 350*kScale + 80, kScreenWidth - 2*kLeftMark, 30) backgroundColor:KClearColor text:NSLocalizedStringFromTable(@"FORGOT PASSWORD", LanguageFileName, nil) textColor:kBlackLightColor font:kHelveticaLightFont(16) textAlignment:NSTextAlignmentLeft lineBreakMode:NSLineBreakByWordWrapping numberOfLines:0];
    [self.scrollview addSubview:title];
    
    
    _email = [[ObjectCTools shared] getACustomTextFiledFrame:CGRectMake(kLeftMark, title.bottom + 30, (kScreenWidth - 2*kLeftMark), 35)
                                                backgroundColor:[UIColor clearColor]
                                                    placeholder:NSLocalizedStringFromTable(@"Email", LanguageFileName, nil)
                                                      textColor:kHexRGB(0x054f7a)
                                                           font:kHelveticaRegularFont(15)
                                                    borderStyle:UITextBorderStyleRoundedRect
                                                  textAlignment:NSTextAlignmentLeft
                                             accessibilityLabel:nil
                                             autocorrectionType:UITextAutocorrectionTypeDefault
                                                clearButtonMode:UITextFieldViewModeWhileEditing
                                                            tag:100
                                                 withIsPassword:NO];
    _email.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, _email.height)];
    _email.leftViewMode = UITextFieldViewModeAlways;
    _email.tintColor = [UIColor blackColor];
    [_scrollview addSubview:_email];
    
    
    UIButton *resetButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake((kScreenWidth - 2*kLeftMark)*0.5 + kLeftMark, _email.bottom + 10, (kScreenWidth - 2*kLeftMark)*0.5, 35)
                                                                       backgroudColor:kBlueColor
                                                                     titleNormalColor:kWhiteColor
                                                                titleHighlightedColor:[UIColor grayColor]
                                                                                title:NSLocalizedStringFromTable(@"RESET PASSWORD", LanguageFileName, nil)
                                                                                 font:kHelveticaLightFont(15)
                                                                         cornerRadius:0
                                                                          borderWidth:0
                                                                          borderColor:KClearColor.CGColor
                                                                   accessibilityLabel:nil];
    [resetButton addTarget: self action:@selector(resetButton:) forControlEvents:UIControlEventTouchUpInside];

    [_scrollview addSubview:resetButton];
    
    
    //infoLabel
    UILabel *infoLable = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,resetButton.bottom + 10, kScreenWidth - 2*kLeftMark, 40) backgroundColor:KClearColor text:NSLocalizedStringFromTable(@"Check your email and follow the instructions to reset your password.", LanguageFileName, nil) textColor:kBlackLightColor font:kHelveticaLightFont(16) textAlignment:NSTextAlignmentLeft lineBreakMode:NSLineBreakByWordWrapping numberOfLines:0];
    [self.scrollview addSubview:infoLable];
    [infoLable sizeToFit];

    
    if (infoLable.bottom + 40 > kScreenHeight) {
        _scrollview.contentSize = CGSizeMake(kScreenWidth, infoLable.bottom + 40);
    }

}

- (void)resetButton:(UIButton *)sender
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
