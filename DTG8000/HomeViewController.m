//
//  HomeViewController.m
//  Thomson
//
//  Created by 何助金 on 10/19/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "HomeViewController.h"
#import "EventButton.h"
#import "M1M2ViewController.h"
#import "GPSViewController.h"
#import "FallDetectViewController.h"
#import "AutoAnswerViewController.h"
#import "LanguageViewController.h"
#import "AlertNotificationViewController.h"
#import "SOSMessageViewController.h"
#import "SOSNumberViewController.h"
#import "ToneProfileViewController.h"
#import "LowBatteryViewController.h"
#import "PeriodicDeviceCheckViewController.h"
#import "HelpViewController.h"

#define kLeftMark 20
@interface HomeViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UIView *_leftView;
    UITableView *_tableView;
    NSMutableArray *_dataArray;
}
@end

@implementation HomeViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(
                                                               NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *documentFolderPath = [searchPaths objectAtIndex:0];
    NSLog(@"Path:%@",documentFolderPath);
}




- (void)setupData {
    _dataArray = [NSMutableArray array];
    
    
    [_dataArray addObjectsFromArray:@[NSLocalizedStringFromTable(@"About", LanguageFileName, nil),NSLocalizedStringFromTable(@"Contact Us", LanguageFileName, nil),NSLocalizedStringFromTable(@"Privacy & Terms", LanguageFileName, nil),NSLocalizedStringFromTable(@"Setings", LanguageFileName, nil),NSLocalizedStringFromTable(@"SOS Number", LanguageFileName, nil),NSLocalizedStringFromTable(@"M1/M2 number", LanguageFileName, nil),NSLocalizedStringFromTable(@"GPS Setting", LanguageFileName, nil),NSLocalizedStringFromTable(@"Fall detect", LanguageFileName, nil),NSLocalizedStringFromTable(@"Tone & Profile", LanguageFileName, nil),NSLocalizedStringFromTable(@"Low battery number", LanguageFileName, nil),NSLocalizedStringFromTable(@"Auto-Answer", LanguageFileName, nil),NSLocalizedStringFromTable(@"Language", LanguageFileName, nil),NSLocalizedStringFromTable(@"Alerts Notification", LanguageFileName, nil),NSLocalizedStringFromTable(@"Help", LanguageFileName, nil)]];
    [self createViews];
}

- (void)leftBarButtonPress:(UIBarButtonItem *)sender
{
    if (!_leftView) {
        _leftView = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
        _leftView.backgroundColor = kHexRGBAlpha(0x000000, 0.5);
        [[UIApplication sharedApplication].keyWindow addSubview:_leftView];
        _leftView.alpha = 0.1;
        [UIView animateWithDuration:1 animations:^{
            _leftView.alpha = 1;
        }completion:^(BOOL finished) {

        }];

        [self addTableView];
    }
}


- (void) addTableView
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(70,0 ,kScreenWidth - 70, kScreenHeight) style:UITableViewStylePlain];
    
    [_tableView setBackgroundColor:[UIColor whiteColor]];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    _tableView.separatorColor = [UIColor grayColor];
    [_leftView addSubview:_tableView];
}
#pragma mark -tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
   return 70;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth - 70, 80)];
    v.backgroundColor = kWhiteColor;
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = CGRectMake(kLeftMark, 30, 60, 35);
    [closeButton setImage:[UIImage imageNamed:@"close_btn"] forState:UIControlStateNormal];
    [closeButton setImage:[UIImage imageNamed:@"close_btn"] forState:UIControlStateHighlighted];
    [closeButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [closeButton addTarget:self action:@selector(closeLeftView:) forControlEvents:UIControlEventTouchUpInside];
    [v addSubview:closeButton];
    
    return v;
    
}

- (void)closeLeftView:(UIButton *)sender
{
    [UIView animateWithDuration:1 animations:^{
        _leftView.alpha = .1;
    }completion:^(BOOL finished) {
        [_leftView removeFromSuperview];
        _leftView = nil;
    }];
}

- (void)closeLeftView
{
//    [UIView animateWithDuration:1.5 animations:^{
//        _leftView.alpha = .1;
//    }completion:^(BOOL finished) {
//        [_leftView removeFromSuperview];
//        _leftView = nil;
//    }];
    [_leftView removeFromSuperview];
    _leftView = nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.textLabel.textColor = kBlueColor;
        UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 70 -2*10, 1)];
        line.x = 10;
        line.y = 44;
        [cell.contentView addSubview:line];
    }
    
    if (indexPath.row == 3) {
        cell.textLabel.textColor = kBlackLightColor;
    }else
    {
        cell.textLabel.textColor = kBlueColor;


    }

    if (indexPath.row != 0 && indexPath.row != 1 && indexPath.row != 2
        && indexPath.row != 3 && indexPath.row != 14) {
        NSString *s = [@"    " stringByAppendingString:_dataArray[indexPath.row]];
        cell.textLabel.text = s;
    }else
    {
        cell.textLabel.text = _dataArray[indexPath.row];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            
        }
            break;
        case 2:
        {
            
        }
            break;
        case 3:
        {
            
        }
            break;
        case 4:
        {
            [self closeLeftView];

            SOSNumberViewController *VC = [[SOSNumberViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"sos_number_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"SOS Number", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];

        }
            break;
        case 5:
        {
            [self closeLeftView];

            M1M2ViewController *m1m2VC = [[M1M2ViewController alloc]init];
            m1m2VC.titleString = NSLocalizedStringFromTable(@"M1/M2 Number", LanguageFileName, nil);
            m1m2VC.iconName = @"m1m2_number_icon";
            [self.navigationController pushViewController:m1m2VC animated:YES];
        }
            break;
        case 6:
        {
            [self closeLeftView];

            GPSViewController *gpsVC = [[GPSViewController alloc]init];
            gpsVC.titleString = NSLocalizedStringFromTable(@"GPS", LanguageFileName, nil);
            gpsVC.iconName = @"gps_icon";
            [self.navigationController pushViewController:gpsVC animated:YES];
            
        }
            break;
        case 7:
        {
            [self closeLeftView];

            FallDetectViewController *VC = [[FallDetectViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"fall_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"Fall Detect", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];
        }
            break;
        case 8:
        {
            [self closeLeftView];

            ToneProfileViewController *VC = [[ToneProfileViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"tone&profile_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"Ring & Tone", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];

        }
            break;
        case 9:
        {
            [self closeLeftView];

            LowBatteryViewController *VC = [[LowBatteryViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"low_battery_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"Low Battery", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];

        }
            break;
        case 10:
        {
            [self closeLeftView];

            AutoAnswerViewController *VC = [[AutoAnswerViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"auto_answer_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"Auto Answer", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];

        }
            break;
        case 11:
        {
            [self closeLeftView];

            LanguageViewController *VC = [[LanguageViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"language_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"Language", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];
        }
            break;
        case 12:
        {
            [self closeLeftView];

            AlertNotificationViewController *VC = [[AlertNotificationViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"alerts_nitification_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"Alert Notification", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];

        }
            break;
        case 13:
        {
            [self closeLeftView];

            HelpViewController *VC = [[HelpViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"help_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"Help", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];
        }
            break;
            
        default:
            break;
    }
}

- (void)createViews
{
    [self.scrollview removeAllSubviews];
    
    [super createHeadViews];
    
    UIButton *findLocationButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake(kLeftMark, self.headBaseView.bottom + 5, (kScreenWidth -2 * kLeftMark -20)/2.0, 35)
                                                                          backgroudColor:kBlueColor
                                                                        titleNormalColor:[UIColor whiteColor]
                                                                   titleHighlightedColor:[UIColor grayColor]
                                                                                   title:NSLocalizedStringFromTable(@"Find  Location", LanguageFileName, nil)
                                                                                    font:kHelveticaRegularFont(16)
                                                                            cornerRadius:5
                                                                             borderWidth:0.5
                                                                             borderColor:kBlueColor.CGColor
                                                                      accessibilityLabel:nil];
    [findLocationButton addTarget: self action:@selector(findeLocationButton:) forControlEvents:UIControlEventTouchUpInside];

    [self.scrollview addSubview:findLocationButton];
    
    UIButton *callNowButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake(findLocationButton.right + 20 ,  self.headBaseView.bottom + 5, (kScreenWidth -2 * kLeftMark -20 )/2.0, 35)
                                                                             backgroudColor:kBlueColor
                                                                           titleNormalColor:[UIColor whiteColor]
                                                                      titleHighlightedColor:[UIColor grayColor]
                                                                                      title:NSLocalizedStringFromTable(@"Call Now", LanguageFileName, nil)
                                                                                       font:kHelveticaRegularFont(16)
                                                                               cornerRadius:5
                                                                                borderWidth:0.5
                                                                                borderColor:kBlueColor.CGColor
                                                                         accessibilityLabel:nil];
    [callNowButton addTarget: self action:@selector(callNowButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollview addSubview:callNowButton];

    NSArray *titleArray = @[NSLocalizedStringFromTable(@"SOS Number", LanguageFileName, nil),NSLocalizedStringFromTable(@"M1/M2", LanguageFileName, nil),NSLocalizedStringFromTable(@"SOS Message", LanguageFileName, nil),NSLocalizedStringFromTable(@"GPS", LanguageFileName, nil),NSLocalizedStringFromTable(@"Tone&Profile", LanguageFileName, nil),NSLocalizedStringFromTable(@"Fall Detect", LanguageFileName, nil),NSLocalizedStringFromTable(@"Low Battery", LanguageFileName, nil),NSLocalizedStringFromTable(@"Auto-Answer", LanguageFileName, nil),NSLocalizedStringFromTable(@"Periodic Device Check", LanguageFileName, nil),NSLocalizedStringFromTable(@"Language", LanguageFileName, nil)];
    
    //NSLocalizedStringFromTable(@"Help", LanguageFileName, nil),NSLocalizedStringFromTable(@"Alerts Notification", LanguageFileName, nil)];
    
    NSArray *imageNameArray = @[@"sosnumber",@"m1-m2",@"sosmessage",@"gps",@"toneprofile",@"falldetect",@"lowbattery",@"auto-answer",@"periodicdevicecheck",@"language",@"help",@"alertsnotification"];
    //总
    NSInteger t = 0;
    //行
    NSInteger n = (titleArray.count/2 )? ((titleArray.count/2)+1 ): (titleArray.count/2);
    for (int i = 0 ; i < n; i++) {
        for (int j = 0 ;j < 2; j ++) {
            if (t >= titleArray.count) {
                break;
            }
            
            EventButton *button = nil;
            if (j%2) {
                button = [[EventButton alloc]initWithFrame:CGRectMake( kScreenWidth*0.5 + kLeftMark, (callNowButton.bottom + 10 + 80 * i), kScreenWidth*0.5 - 2*kLeftMark, 73)];
                
            }else {
                
               // button = [[EventButton alloc]initWithFrame:CGRectMake(kLeftMark, (callNowButton.bottom + 10 + 90 *i), kScreenWidth*0.5 - 2*kLeftMark, 80)];
                button = [[EventButton alloc]initWithFrame:CGRectMake(kLeftMark, (callNowButton.bottom + 10 + 80 *i), kScreenWidth*0.5 - 2*kLeftMark, 73)];

            }
            
            [button setTitle:titleArray[t] forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:imageNameArray[t]] forState:UIControlStateNormal];
            
            [button addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchUpInside];
            button.tag = 100 + t;
            
            UIControl *control = [[UIControl alloc]initWithFrame:button.bounds];
            [control addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchUpInside];
            control.tag = 100 + t;

            [button addSubview:control];
            ++t;
            
            [self.scrollview addSubview:button];
            [self.scrollview setScrollEnabled:YES];
            
            if (t == titleArray.count) {
                self.scrollview.contentSize = CGSizeMake(kScreenWidth, button.bottom + 20 + 64);
            }
        }
    }

}


- (void)findeLocationButton:(UIButton *)sender
{
    NSString *body = [NSString stringWithFormat:@"GET#GPSINFO#"];
    [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Setup M1 number", LanguageFileName, nil) body:body withDelegate:self];
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    switch (result) {
        case MessageComposeResultSent:{
            //信息传送成功
            NSLog(@"信息传送成功");
            
        }
            
            break;
        case MessageComposeResultFailed:
            //信息传送失败
            NSLog(@"信息传送失败");
            
            break;
        case MessageComposeResultCancelled:
            //信息被用户取消传送
            NSLog(@"信息被用户取消传送");
            break;
        default:
            break;
    }
    
}



- (void)callNowButton:(UIButton *)sender
{
    NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"tel:%@",kMessageNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];  }


- (void)buttonPress:(UIButton *)sender
{
    switch (sender.tag - 100) {
        case 0:
        {
            SOSNumberViewController *VC = [[SOSNumberViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"sos_number_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"SOS Number", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];
        }
            break;
        case 1:
        {
            M1M2ViewController *m1m2VC = [[M1M2ViewController alloc]init];
            m1m2VC.titleString = NSLocalizedStringFromTable(@"M1/M2 Number", LanguageFileName, nil);
            m1m2VC.iconName = @"m1m2_number_icon";
            [self.navigationController pushViewController:m1m2VC animated:YES];
            
        }
            break;
        case 2:
        {
            SOSMessageViewController *VC = [[SOSMessageViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"sos_message_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"SOS Message", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];
        }
            break;
        case 3:
        {
            GPSViewController *gpsVC = [[GPSViewController alloc]init];
            gpsVC.titleString = NSLocalizedStringFromTable(@"GPS", LanguageFileName, nil);
            gpsVC.iconName = @"gps_icon";
            [self.navigationController pushViewController:gpsVC animated:YES];

        }
            break;
        case 4:
        {
            ToneProfileViewController *VC = [[ToneProfileViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"tone&profile_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"Ring & Tone", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];
        }
            break;
        case 5:
        {
            FallDetectViewController *VC = [[FallDetectViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"fall_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"Fall Detect", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];
        }
            break;
        case 6:
        {
            LowBatteryViewController *VC = [[LowBatteryViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"low_battery_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"Low Battery", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];
        }
            break;
        case 7:
        {
            AutoAnswerViewController *VC = [[AutoAnswerViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"auto_answer_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"Auto Answer", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];
        }
            break;
        case 8:
        {
            PeriodicDeviceCheckViewController *VC = [[PeriodicDeviceCheckViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"device_check_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"Periodic Device Check", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];
        }
            break;
        case 9:
        {
            LanguageViewController *VC = [[LanguageViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"language_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"Language", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];
        }
            break;
        case 10:
        {
            HelpViewController *VC = [[HelpViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"help_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"Help", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];

        }
            break;
        case 11:
        {
            AlertNotificationViewController *VC = [[AlertNotificationViewController alloc]init];
            VC.icon.image = [UIImage imageNamed:@"alerts_nitification_icon"];
            VC.titleString = NSLocalizedStringFromTable(@"Alert Notification", LanguageFileName, nil);
            [self.navigationController pushViewController:VC animated:YES];
        }
            break;
        default:
            break;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
