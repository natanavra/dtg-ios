//
//  AddDeviceTableViewCell.m
//  Thomson
//
//  Created by 何助金 on 10/25/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "AddDeviceTableViewCell.h"

@implementation AddDeviceTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];

}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.textLabel.textColor = kBlueColor;
        self.textLabel.font = kHelveticaLightFont(16);
        self.detailTextLabel.font = kHelveticaLightFont(12);
        self.detailTextLabel.textColor = kBlackLightColor;
        UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2*15, 1)];
        line.x = 15;
        line.y = self.contentView.bottom-1;
        line.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        line.tag = 99;
        [self.contentView addSubview:line];
        
        
        self.passwordField.hidden = YES;
        
        self.imageView.image = [UIImage imageNamed:@"add"];
        self.imageView.contentMode = UIViewContentModeCenter;
        self.detailTextLabel.numberOfLines = 1;
        
        
        self.passwordField = [[ObjectCTools shared]getACustomTextFiledFrame:CGRectMake(kLeftMark,70, 60, 30)
                                                          backgroundColor:kWhiteColor placeholder:NSLocalizedStringFromTable(@"XXXX", LanguageFileName, nil)
                                                                textColor:kBlackLightColor
                                                                     font:kHelveticaLightFont(14)
                                                              borderStyle:UITextBorderStyleRoundedRect
                                                            textAlignment:NSTextAlignmentCenter
                                                       accessibilityLabel:nil autocorrectionType:UITextAutocorrectionTypeDefault clearButtonMode:UITextFieldViewModeNever tag:100 withIsPassword:YES];

        self.passwordField.adjustsFontSizeToFitWidth = YES;
        self.passwordField.keyboardType = UIKeyboardTypeNumberPad;
        [self.contentView addSubview:self.passwordField];
        
        self.addButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake(15, _passwordField.y ,80, 30)
                                                                    backgroudColor:kBlueColor
                                                                  titleNormalColor:[UIColor whiteColor]
                                                             titleHighlightedColor:[UIColor grayColor]
                                                                             title:NSLocalizedStringFromTable(@"Add", LanguageFileName, nil)
                                                                              font:kHelveticaLightFont(14)
                                                                      cornerRadius:5
                                                                       borderWidth:0.5
                                                                       borderColor:KClearColor.CGColor
                                                                accessibilityLabel:nil];
        [self.addButton addTarget: self action:@selector(addButton:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [self addSubview:self.addButton];
       
        self.passwordField.hidden = YES;
        self.addButton.hidden = YES;
    }
    return self;
}

- (void)addButton:(UIButton *)sender
{
    self.editing = NO;
    [self.passwordField resignFirstResponder];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(addDeviceTableViewCell:)]) {
        [self.delegate addDeviceTableViewCell:self];
    }
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    self.imageView.x = kScreenWidth - kLeftMark - 20;
    self.imageView.y = 20;
    self.imageView.bounds = CGRectMake(0, 0, 20, 20);
    self.textLabel.x = kLeftMark;
    self.textLabel.y =  self.imageView.y;
    self.textLabel.height = 20;
    self.detailTextLabel.x = self.textLabel.x;
    self.detailTextLabel.y = self.textLabel.bottom + 10;
    self.detailTextLabel.width = kScreenWidth - 60;
    self.detailTextLabel.height = 15;
    self.passwordField.y = self.detailTextLabel.bottom + 5;
    self.addButton.y = self.passwordField.y;
}

- (void)setIsOpen:(BOOL)isOpen
{
    _isOpen = isOpen;
    if (_isOpen) {
       // self.detailTextLabel.text = NSLocalizedStringFromTable(@"Set 4-digit Access Code", LanguageFileName, nil);
        self.passwordField.hidden = YES;
        self.addButton.hidden = NO;
        self.imageView.image = [UIImage imageNamed:@"sub"];

        
    }else
    {
        self.detailTextLabel.text = nil;
        self.passwordField.hidden = YES;
        self.addButton.hidden = YES;
        self.imageView.image = [UIImage imageNamed:@"add"];

    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.selectionStyle = UITableViewCellSeparatorStyleNone;
    // Configure the view for the selected state
}

@end
