//
//  GPSViewController.m
//  Thomson
//
//  Created by 何助金 on 10/21/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "GPSViewController.h"
#import "ValueView.h"
#import "HistoryTableViewCell.h"
#import "GeofenceModel.h"
#import "GetMNumberTableViewCell.h"
@import GoogleMaps;
#define kGPSEnable @"GPSEnable"
#define kGEOEnable @"GEOEnable"
#define KGEORange  @"GEORange"
#define kGPSAutoReportEnable @"GPSAutoReportEnable"
#define KReportTime      @"ReportTime"

@interface GPSViewController ()<GMSMapViewDelegate,UITextFieldDelegate,CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate>

{
    UIScrollView *_scrollView;
    ValueView *_valueView;
    GMSMapView *mapView_;
    UISwitch *_geofenceEnableSwitch;
    GMSCircle *_circ;
    CLLocationCoordinate2D _coordinate;
    UIView *_setBgView;
    UIView *_getBgView;
    UITableView *_tableView;
    NSMutableArray *_dataArray;

}
@property (nonatomic ,strong) UISlider *rangelider;
@property (nonatomic ,strong) CLLocationManager *locationManager;
@property (nonatomic ,assign) BOOL didFindMyLocation;
@property (nonatomic ,strong) UISlider *volumeSlider;

@end

@implementation GPSViewController

- (void)mapView:(GMSMapView *)mapView
didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    
    NSLog(@"You tapped at %f,%f", coordinate.latitude, coordinate.longitude);
   
    if (_geofenceEnableSwitch.on && self.rangelider.value>0) {
        _circ.map = nil;
        _coordinate = coordinate;
        _circ = [GMSCircle circleWithPosition:coordinate
                                       radius:lroundf(self.rangelider.value)];
        
        _circ.fillColor = [UIColor colorWithRed:0.25 green:0 blue:0 alpha:0.05];
        _circ.strokeColor = [UIColor redColor];
        _circ.strokeWidth = 3;
        _circ.map = mapView_;
        
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        mapView_.myLocationEnabled = YES;

    }
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {

    CLLocation * currLocation = [locations lastObject];
    
    
    NSLog(@"%@",[NSString stringWithFormat:@"%.3f",currLocation.coordinate.latitude]);
    
    NSLog(@"%@",[NSString stringWithFormat:@"%.3f",currLocation.coordinate.longitude]);
    NSLog(@"User's location: %@", mapView_.myLocation);
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currLocation.coordinate.latitude
                                                            longitude:currLocation.coordinate.longitude
                                                                 zoom:15];
    GMSCameraUpdate *update = [GMSCameraUpdate setCamera:camera];

    [mapView_ moveCamera:update];
}

- (void)_initMapLocationManager
{
    self.locationManager = [[CLLocationManager alloc]init];
    self.locationManager.delegate = self;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        
        [_locationManager requestWhenInUseAuthorization];  //调用了这句,就会弹出允许框了.
    }
    
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest; //控制定位精度,越高耗电量越大。
    
    _locationManager.distanceFilter = 100; //控制定位服务更新频率。单位是“米”
    
    [_locationManager startUpdatingLocation];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self _initMapLocationManager];
    
    _dataArray = [NSMutableArray array];

    //
    _setBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 11,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 11 - 64)];
    _setBgView.backgroundColor = kWhiteColor;
    _setBgView.hidden = NO;
    [self.view addSubview:_setBgView];
    
    _getBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 - 64)];
    _getBgView.backgroundColor = kWhiteColor;
    _getBgView.hidden = YES;
    [self.view addSubview:_getBgView];


    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, _setBgView.height)];
//    _scrollView.backgroundColor = kBlueColor;
    [_setBgView addSubview:_scrollView];
    
    UIImageView *lineView = (UIImageView *)[self.view viewWithTag:999];
    [self.view bringSubviewToFront:lineView];

    UIView *mapView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 200)];
    mapView.backgroundColor = kWhiteColor;
    [_scrollView addSubview:mapView];
    
    
    //    - See more at: http://vikrambahl.com/google-maps-ios-xcode-storyboards/#sthash.6N6sbyPM.dpuf
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:22.25
                                                            longitude:113.53
                                                                 zoom:15];
    mapView_ = [GMSMapView mapWithFrame:mapView.bounds camera:camera];
    mapView_.myLocationEnabled = YES;
    mapView_.delegate = self;
    
    mapView_.mapType = kGMSTypeNormal;
    //Shows the compass button on the map
    mapView_.settings.compassButton = YES;
    //Shows the my location button on the map
    mapView_.settings.myLocationButton = YES;
//    NSLog(@"User's location: %@", mapView_.myLocation);
    [mapView addSubview:mapView_];

    
    UILabel *infoLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,mapView.bottom + 15, kScreenWidth/2 - kLeftMark , 35)
                                                     backgroundColor:[UIColor clearColor]
                                                                text:NSLocalizedStringFromTable(@"AGPS setup", LanguageFileName, nil)
                                                           textColor:kBlackLightColor
                                                                font:kHelveticaLightFont(16)
                                                       textAlignment:NSTextAlignmentLeft
                                                       lineBreakMode:NSLineBreakByWordWrapping
                                                       numberOfLines:0];
    
    infoLabel.adjustsFontSizeToFitWidth = YES;
    [_scrollView addSubview:infoLabel];
    
    UILabel *enableLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kScreenWidth/2.0,infoLabel.y , kScreenWidth/2 - kLeftMark - 55 , 35)
                                                       backgroundColor:[UIColor clearColor]
                                                                  text:NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil)
                                                             textColor:kBlackLightColor
                                                                  font:kHelveticaLightFont(13)
                                                         textAlignment:NSTextAlignmentRight
                                                         lineBreakMode:NSLineBreakByWordWrapping
                                                         numberOfLines:0];
    
    enableLabel.adjustsFontSizeToFitWidth = YES;
    [_scrollView addSubview:enableLabel];
    
    
    UISwitch *enableSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(kScreenWidth - kLeftMark - 50,self.setGetSegmet.bottom + 35, 50 , 20)];
    enableSwitch.onTintColor = kBlueColor;
    
    [enableSwitch addTarget:self action:@selector(enableSwitch:) forControlEvents:UIControlEventValueChanged];
    enableSwitch.on = [kUserDef boolForKey:kGPSEnable];
    enableSwitch.tag = 300;
    [_scrollView addSubview:enableSwitch];
    
    enableSwitch.centerY = infoLabel.centerY;
    enableLabel.centerY = infoLabel.centerY;
    
    NSArray *titleArray = @[NSLocalizedStringFromTable(@" GPS", LanguageFileName, nil),NSLocalizedStringFromTable(@" Control Plane", LanguageFileName, nil)];
    for (int j = 0; j < 2; ++ j) {
        
        UIButton *aGPSButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake( kLeftMark + j * ((kScreenWidth - 2*kLeftMark)/3.0), infoLabel.bottom, ((kScreenWidth - 2*kLeftMark)/3.0) -10, 35)
                                                                              backgroudColor:KClearColor
                                                                            titleNormalColor:kBlackLightColor
                                                                       titleHighlightedColor:[UIColor grayColor]
                                                                                       title:titleArray[j]
                                                                                        font:kHelveticaLightFont(13)
                                                                                cornerRadius:0
                                                                                 borderWidth:0
                                                                                 borderColor:KClearColor.CGColor
                                                                          accessibilityLabel:nil];
        [aGPSButton addTarget: self action:@selector(aGPSButton:) forControlEvents:UIControlEventTouchUpInside];
        [aGPSButton setImage:[UIImage imageNamed:@"point"] forState:UIControlStateNormal];
        [aGPSButton setImage:[UIImage imageNamed:@"point_p"] forState:UIControlStateSelected];
        aGPSButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [aGPSButton setContentEdgeInsets:(UIEdgeInsetsMake(0, 0, 0, 0))];
        aGPSButton.tag = 200 + j;
        if (j==0) {
            //aGPSButton.selected = YES;
        }
//        aGPSButton.userInteractionEnabled = enableSwitch.on;
        if (j == 0) {
            aGPSButton.width -= 30;
        }
        if (j == 1) {
            aGPSButton.x -=30;
            aGPSButton.width += 30;
        }
        if (j == 2) {
            aGPSButton.x -=20;
            aGPSButton.width += 30;
        }
        if([kUserDef integerForKey:@"SWGPS"] ==j)
        {
            aGPSButton.selected = YES;
        }
        
        
        [_scrollView  addSubview:aGPSButton];
        
        
        
        
        
        
       
    }
    
    
    UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2 *kLeftMark, 1.0)];
    line.y = infoLabel.bottom + 40;
    line.x = kLeftMark;
    [_scrollView addSubview:line];
    
    
    //Geofence Setup
    UILabel *geofenceLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,line.bottom + 15, kScreenWidth/2 - kLeftMark , 35)
                                                     backgroundColor:[UIColor clearColor]
                                                                text:NSLocalizedStringFromTable(@"Geofence Setup", LanguageFileName, nil)
                                                           textColor:kBlackLightColor
                                                                font:kHelveticaLightFont(16)
                                                       textAlignment:NSTextAlignmentLeft
                                                       lineBreakMode:NSLineBreakByWordWrapping
                                                       numberOfLines:0];
    
    geofenceLabel.adjustsFontSizeToFitWidth = YES;
    [_scrollView addSubview:geofenceLabel];
    
    UILabel *enableLabel1 = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kScreenWidth/2.0,geofenceLabel.y, kScreenWidth/2 - kLeftMark - 55 , 35)
                                                       backgroundColor:[UIColor clearColor]
                                                                  text:NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil)
                                                             textColor:kBlackLightColor
                                                                  font:kHelveticaLightFont(13)
                                                         textAlignment:NSTextAlignmentRight
                                                         lineBreakMode:NSLineBreakByWordWrapping
                                                         numberOfLines:0];
    
    enableLabel1.adjustsFontSizeToFitWidth = YES;
    [_scrollView addSubview:enableLabel1];
    
    
    _geofenceEnableSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(kScreenWidth - kLeftMark - 50,self.setGetSegmet.bottom + 35, 50 , 20)];
    _geofenceEnableSwitch.onTintColor = kBlueColor;
    
    [_geofenceEnableSwitch addTarget:self action:@selector(geofenceEnableSwitch:) forControlEvents:UIControlEventValueChanged];
    _geofenceEnableSwitch.on = [kUserDef boolForKey:kGEOEnable];
    _geofenceEnableSwitch.tag = 301;
    [_scrollView addSubview:_geofenceEnableSwitch];
    
    _geofenceEnableSwitch.centerY = geofenceLabel.centerY;
    enableLabel1.centerY = geofenceLabel.centerY;
    //Number
    
    UIView *line1 = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2 *kLeftMark, 1.0)];
    line1.y = _geofenceEnableSwitch.bottom + 10;
    line1.x = kLeftMark;
    [_setBgView addSubview:line1];
    [_scrollView addSubview:line1];
    //Number
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(kLeftMark, line1.bottom + 10, kScreenWidth - 2 *kLeftMark, 80)];
    bgView.layer.cornerRadius = 3;
    bgView.layer.backgroundColor = kGrayLightColor.CGColor;
    bgView.layer.borderColor = KClearColor.CGColor;
    bgView.layer.masksToBounds = YES;
    [_setBgView addSubview:bgView];
    
    UILabel *titleLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(5 ,5, bgView.width - 10, 35)
                                                      backgroundColor:[UIColor clearColor]
                                                                 text:NSLocalizedStringFromTable(@"Geofence Alert Phone Number", LanguageFileName, nil)
                                                            textColor:kBlackLightColor
                                                                 font:kHelveticaLightFont(16)
                                                        textAlignment:NSTextAlignmentLeft
                                                        lineBreakMode:NSLineBreakByWordWrapping
                                                        numberOfLines:0];
    
    titleLabel.adjustsFontSizeToFitWidth = YES;
    [bgView addSubview:titleLabel];
    
    self.numberField = [[ObjectCTools shared]getACustomTextFiledFrame:CGRectMake(5, titleLabel.bottom , bgView.width - 2*5 - 80, 35)
                                                      backgroundColor:kWhiteColor placeholder:NSLocalizedStringFromTable(@"Insert phone number", LanguageFileName, nil)
                                                            textColor:kBlackLightColor
                                                                 font:kHelveticaLightFont(16)
                                                          borderStyle:UITextBorderStyleRoundedRect
                                                        textAlignment:NSTextAlignmentLeft
                                                   accessibilityLabel:nil autocorrectionType:UITextAutocorrectionTypeDefault clearButtonMode:UITextFieldViewModeWhileEditing tag:100 withIsPassword:NO];
    self.numberField.delegate = self;
    self.numberField.keyboardType = UIKeyboardTypeNumberPad;
    
    self.numberField.text = [kUserDef stringForKey:@"GEOFENCEPHONE"];
    
    [bgView addSubview:self.numberField];
    
    self.editButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage:CGRectMake(self.numberField.right + 5, _numberField.y  ,75, 35)
                                                                backgroudColor:kBlueColor
                                                              titleNormalColor:[UIColor whiteColor]
                                                         titleHighlightedColor:[UIColor grayColor]
                                                                         title:NSLocalizedStringFromTable(@"Set", LanguageFileName, nil)
                                                                          font:kHelveticaLightFont(14)
                                                                  cornerRadius:5
                                                                   borderWidth:0.5
                                                                   borderColor:KClearColor.CGColor
                                                            accessibilityLabel:nil];
    [self.editButton addTarget: self action:@selector(editButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [bgView addSubview:self.editButton];
    self.isEdit = YES;
[_scrollView addSubview:bgView];
    
    //Geofence Range
    UILabel *geofenceRangLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,bgView.bottom + 25, kScreenWidth/2 - 2*kLeftMark , 20)
                                                         backgroundColor:[UIColor clearColor]
                                                                    text:NSLocalizedStringFromTable(@"Geofence Range", LanguageFileName, nil)
                                                               textColor:kBlackLightColor
                                                                    font:kHelveticaLightFont(16)
                                                           textAlignment:NSTextAlignmentLeft
                                                           lineBreakMode:NSLineBreakByWordWrapping
                                                           numberOfLines:0];
    
    geofenceRangLabel.adjustsFontSizeToFitWidth = YES;
    [_scrollView addSubview:geofenceRangLabel];
    UILabel *geofenceRangSubLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,geofenceRangLabel.bottom , 80 , 15)
                                                             backgroundColor:[UIColor clearColor]
                                                                        text:NSLocalizedStringFromTable(@"In Meters", LanguageFileName, nil)
                                                                   textColor:kBlackLightColor
                                                                        font:kHelveticaLightFont(10)
                                                               textAlignment:NSTextAlignmentLeft
                                                               lineBreakMode:NSLineBreakByWordWrapping
                                                               numberOfLines:0];
    
    geofenceRangSubLabel.adjustsFontSizeToFitWidth = YES;
    [_scrollView addSubview:geofenceRangSubLabel];

    //rangeSlider
    
    self.rangelider = [[UISlider alloc]initWithFrame:CGRectMake(geofenceRangLabel.right + 5, geofenceRangLabel.y, kScreenWidth - geofenceLabel.right - 5 - kLeftMark, 10)];
    self.rangelider.minimumValue = 0;
    self.rangelider.maximumValue = 1000;
    self.rangelider.value = 0;
    self.rangelider.userInteractionEnabled = _geofenceEnableSwitch.on;
    self.rangelider.minimumTrackTintColor = kBlueColor;
    self.rangelider.maximumTrackTintColor = kBlackLightColor;
    
    [self.rangelider setThumbImage: [UIImage imageNamed:@"point_red_big"] forState:UIControlStateNormal];
    [self.rangelider addTarget:self action:@selector(rangelider:) forControlEvents:UIControlEventValueChanged];
    [_scrollView addSubview:self.rangelider];
    self.rangelider.centerY = geofenceRangLabel.centerY+ 2;
    
    UILabel *valueLabe = [[UILabel alloc]initWithFrame:CGRectMake(self.rangelider.x  , self.rangelider.bottom+5, 10, 15)];
    valueLabe.text = @"0";
    
    valueLabe.backgroundColor = KClearColor;
    valueLabe.font = kHelveticaLightFont(14);
    [_scrollView addSubview: valueLabe];

    UILabel *valueMaxLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.rangelider.right - 30 , self.rangelider.bottom + 5, 50, 15)];
    valueMaxLabel.text = @"1000";
    valueMaxLabel.backgroundColor = KClearColor;
    valueMaxLabel.font = kHelveticaLightFont(14);
    [_scrollView addSubview: valueMaxLabel];
    
    _rangelider.value = [kUserDef floatForKey:KGEORange];
    
    _valueView = [[ValueView alloc]initWithFrame:CGRectMake(self.rangelider.x - 25 + 7.5 + (self.rangelider.width - 15)*(self.rangelider.value/1000.0),self.rangelider.y - 27 , 50, 22.5)];
    _valueView.value = [NSString stringWithFormat:@"%ld m",lroundf(self.rangelider.value)];
    [_scrollView addSubview:_valueView];
    
    if (valueLabe.bottom + 20 > _scrollView.height) {
        _scrollView.contentSize = CGSizeMake(kScreenWidth, valueLabe.bottom + 20);
    }
    
    
    UILabel *reportLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,_valueView.bottom + 50, kScreenWidth/2 - kLeftMark , 35)
                                                     backgroundColor:[UIColor clearColor]
                                                                text:NSLocalizedStringFromTable(@"GPS auto report", LanguageFileName, nil)
                                                           textColor:kBlackLightColor
                                                                font:kHelveticaLightFont(16)
                                                       textAlignment:NSTextAlignmentLeft
                                                       lineBreakMode:NSLineBreakByWordWrapping
                                                       numberOfLines:0];
    
    reportLabel.adjustsFontSizeToFitWidth = YES;
    [_scrollView addSubview:reportLabel];
    
    UILabel *enableReportLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kScreenWidth/2.0,reportLabel.y , kScreenWidth/2 - kLeftMark - 55 , 35)
                                                       backgroundColor:[UIColor clearColor]
                                                                  text:NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil)
                                                             textColor:kBlackLightColor
                                                                  font:kHelveticaLightFont(13)
                                                         textAlignment:NSTextAlignmentRight
                                                         lineBreakMode:NSLineBreakByWordWrapping
                                                         numberOfLines:0];
    
    enableReportLabel.adjustsFontSizeToFitWidth = YES;
    [_scrollView addSubview:enableReportLabel];
    
    
    UISwitch *enableReportSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(kScreenWidth - kLeftMark - 50,_valueView.bottom + 50, 50 , 20)];
    enableReportSwitch.onTintColor = kBlueColor;
    
    [enableReportSwitch addTarget:self action:@selector(enableReportSwitch:) forControlEvents:UIControlEventValueChanged];
    enableReportSwitch.on = [kUserDef boolForKey:kGPSAutoReportEnable];
    enableReportSwitch.tag = 308;
    [_scrollView addSubview:enableReportSwitch];

    
    
    //Volume setupint
    UILabel *volumeLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,enableReportSwitch.bottom + 20, kScreenWidth - 2*kLeftMark , 35)
                                                       backgroundColor:[UIColor clearColor]
                                                                  text:NSLocalizedStringFromTable(@" GPS auto report time(min)", LanguageFileName, nil)
                                                             textColor:kBlackLightColor
                                                                  font:kHelveticaLightFont(16)
                                                         textAlignment:NSTextAlignmentLeft
                                                         lineBreakMode:NSLineBreakByWordWrapping
                                                         numberOfLines:0];
    
    volumeLabel.adjustsFontSizeToFitWidth = YES;
    
    [_scrollView addSubview: volumeLabel];
    
    self.volumeSlider = [[UISlider alloc]initWithFrame:CGRectMake(kLeftMark, volumeLabel.bottom + 15, kScreenWidth -2*kLeftMark, 10)];
    
    self.volumeSlider.value = 2;
    
    self.volumeSlider.minimumValue = 1;
    self.volumeSlider.maximumValue = 6;
    self.volumeSlider.minimumTrackTintColor = kBlueColor;
    self.volumeSlider.maximumTrackTintColor = kBlackLightColor;
    self.volumeSlider.continuous = NO;
    
    [self.volumeSlider setThumbImage: [UIImage imageNamed:@"point_red_big"] forState:UIControlStateNormal];
    [_scrollView addSubview:self.volumeSlider];
    
    
    for (int i = 0; i < 6; i ++) {
        
        UILabel *valueLabe = [[UILabel alloc]initWithFrame:CGRectMake((self.volumeSlider.width/5.0)*i - 7 , 0, 30, 15)];
        if(i==0)
        {
            valueLabe.text = [NSString stringWithFormat:@"%d",5];
        }
        if(i==1)
        {
            valueLabe.text = [NSString stringWithFormat:@"%d",15];
        }
        if(i==2)
        {
            valueLabe.text = [NSString stringWithFormat:@"%d",30];
        }
        if(i==3)
        {
            valueLabe.text = [NSString stringWithFormat:@"%d",60];
        }
        if(i==4)
        {
            valueLabe.text = [NSString stringWithFormat:@"%d",120];
        }
        if(i==5)
        {
            valueLabe.text = [NSString stringWithFormat:@"%d",240];
        }
        //valueLabe.text = [NSString stringWithFormat:@"%d",i + 1];
        valueLabe.backgroundColor = KClearColor;
        valueLabe.font = kHelveticaLightFont(13);
        
        UIImageView *pointView = [[UIImageView alloc]initWithFrame:CGRectMake((self.volumeSlider.width/5.0)*i -7, 0, 7, 7)];
        pointView.centerY = self.volumeSlider.height/2.0;
        valueLabe.y = self.volumeSlider.height/2.0 + 8;
        
        if (i == 0) {
            pointView.x = 0;
            valueLabe.x = 2;
            pointView.image = [UIImage imageNamed:@"point_red_small"];
        }else
        {
            pointView.image = [UIImage imageNamed:@"point_black"];
            
        }
        if (i == 1) {
            pointView.x += 4;
            valueLabe.x += 4;
        }
        
        if (i == 2) {
            pointView.x += 2;
            valueLabe.x += 2;
        }
        pointView.tag = 400 + i;
        [self.volumeSlider addSubview:pointView];
        [self.volumeSlider bringSubviewToFront:pointView];
        
        [self.volumeSlider addSubview:valueLabe];
        [self.volumeSlider bringSubviewToFront:valueLabe];
        
        
    }
    
    [self.volumeSlider addTarget:self action:@selector(volumeSlider:) forControlEvents:UIControlEventValueChanged];
    
    if (self.volumeSlider.bottom + 40 + 64> _scrollView.height) {
        _scrollView.contentSize = CGSizeMake(kScreenWidth, self.volumeSlider.bottom + 40 + 64);
    }
    //self.volumeSlider.value = [kUserDef floatForKey:KVol];
    //[self volumeSlider:self.volumeSlider];
    
    
    
    [self addTableView];
}


- (void) addTableView
{
    _tableView = [[UITableView alloc] initWithFrame:_getBgView.bounds style:UITableViewStylePlain];
    
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    _tableView.separatorColor = [UIColor grayColor];
    [_getBgView addSubview:_tableView];
}
#pragma mark -tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1 + _dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0){
        
        GetMNumberTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GetMNumberTableViewCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"GetMNumberTableViewCell" owner:self options:nil] lastObject];
        }
        
        cell.mlable.text = NSLocalizedStringFromTable(@"Update History", LanguageFileName, nil);    
        cell.numberLable.text = nil;
        return cell;
        
    }else
    {
        HistoryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"HistoryTableViewCell" owner:self options:nil] lastObject];
            UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2 *15, 1.0)];
            [cell.contentView addSubview:line];
            line.y = 29;
            line.x = 15;
        }
        
        if (_dataArray.count) {

            GeofenceModel *model = _dataArray[indexPath.row - 1];
            cell.mLable.text = model.name;
            cell.dateLabel.text = [DateHelper timeSPToTimeStringWithString:model.updateTime];
        
        }else {
            cell.mLable.text = @"";
            cell.dateLabel.text = @"";
        }
        return cell;
    }
    
}


- (void)rangelider:(UISlider *)sender
{

    _valueView.frame = CGRectMake(self.rangelider.x - 25 + 7.5 + (self.rangelider.width - 15)*(self.rangelider.value/1000.0),self.rangelider.y - 27 , 50, 22.5);
    _valueView.value = [NSString stringWithFormat:@"%ld m",lroundf(self.rangelider.value)];
    [kUserDef setFloat:lroundf(self.rangelider.value) forKey:KGEORange];
    [kUserDef synchronize];
}

#pragma mark - 单发
- (void)aGPSButton:(UIButton *)sender
{
    for (int i = 0 ; i < 3 ; ++i) {
        UIButton *button  = (UIButton *)[_scrollView viewWithTag:200 + i];
        if (button.tag == sender.tag ) {
            button.selected = YES;
            NSString *body = [NSString stringWithFormat:@"SET#AGPS#%d#",i];
//            [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"AGPS setup", LanguageFileName, nil) body:body withDelegate:self];
        }else{
            button.selected = NO;
        }
    }

}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    [self dismissViewControllerAnimated:YES completion:nil];
    switch (result) {
        case MessageComposeResultSent:
        {
            //信息传送成功
            NSLog(@"信息传送成功");
            
//            if (self.rangelider.value>0) {
                GeofenceModel *model = [[GeofenceModel alloc]init];
                model.name = [NSString stringWithFormat:@"%@ %ld %@",NSLocalizedStringFromTable(@"Geofence Range", LanguageFileName, nil),lroundf(self.rangelider.value),NSLocalizedStringFromTable(@"meters", LanguageFileName, nil)];
                model.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
                [model saveToDB];
                
                GeofenceModel *model1 = [[GeofenceModel alloc]init];

                model1.name = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTable(@"AGPS", LanguageFileName, nil),[kUserDef boolForKey:kGPSEnable]?NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil):NSLocalizedStringFromTable(@"Disable", LanguageFileName, nil)];
                model1.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
                [model1 saveToDB];
    
                
                GeofenceModel *model2 = [[GeofenceModel alloc]init];
                
                model2.name = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTable(@"Geofence", LanguageFileName, nil),[kUserDef boolForKey:kGEOEnable]?NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil):NSLocalizedStringFromTable(@"Disable", LanguageFileName, nil)];
                model2.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
                [model2 saveToDB];
            
            if (self.numberField.text.length) {
                GeofenceModel *model2 = [[GeofenceModel alloc]init];
                
                model2.name = [NSString stringWithFormat:@"%@:%@",NSLocalizedStringFromTable(@"Geofence Alert Number", LanguageFileName, nil),self.numberField.text];
                model2.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
                [model2 saveToDB];
            }
            
            int xtime = 5;
            if(self.volumeSlider.value>=0 && self.volumeSlider.value<1.5)
            {
                xtime = 5;
            }
            if(self.volumeSlider.value>=1.5 && self.volumeSlider.value<2.5)
            {
                xtime = 15;
            }
            
            if(self.volumeSlider.value>=2.5 && self.volumeSlider.value<3.5)
            {
                xtime = 30;
            }
            
            if(self.volumeSlider.value>=3.5 && self.volumeSlider.value <4.5)
            {
                xtime = 60;
            }
            
            if(self.volumeSlider.value>=4.5 && self.volumeSlider.value<5.5)
            {
                xtime = 120;
            }
            if(self.volumeSlider.value>=5.5)
            {
                xtime = 240;
            }

            
            GeofenceModel *model3 = [[GeofenceModel alloc]init];
            model3.name = [NSString stringWithFormat:@"%@:%d",NSLocalizedStringFromTable(@"REPORT TIME", LanguageFileName, nil),xtime];
            model3.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
            [model3 saveToDB];
            
            
            GeofenceModel *model4 = [[GeofenceModel alloc]init];
            model4.name = [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTable(@"GPS Auto Report", LanguageFileName, nil),[kUserDef boolForKey:kGPSAutoReportEnable]?NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil):NSLocalizedStringFromTable(@"Disable", LanguageFileName, nil)];
            model4.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
            [model4 saveToDB];
            
            
//            }
            
            [self.navigationController popViewControllerAnimated:YES];

        }
            
            break;
        case MessageComposeResultFailed:{
            //信息传送失败
            NSLog(@"信息传送失败");
            [self.navigationController popViewControllerAnimated:YES];

        }
            
            break;
        case MessageComposeResultCancelled:{
            
            //信息被用户取消传送
            NSLog(@"信息被用户取消传送");

        }
            break;
        default:
            break;
    }

}

#pragma mark    AGPS Setup
/*
 5.1	 AGPS setup
 Enable AGPS
 send：SET_PW_#AGPS#x#
 return：+AGPS=X#
 X 0: gps, 1: user plane, 2: control plane
 */
- (void)enableSwitch:(UISwitch *)sender
{
    if (sender.on) {
//        for (int i = 0 ; i < 3 ; ++i) {
//            UIButton *button  = (UIButton *)[_scrollView viewWithTag:200 + i];
//            button.userInteractionEnabled = YES;
//        }
        [[ObjectCTools shared] showAlertViewAndDissmissAutomatic:NSLocalizedStringFromTable(@"AGPS is", LanguageFileName, nil) andMessage:NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil) withDissmissTime:KDissmissTime withDelegate:nil withAction:nil];
    }else{
//        for (int i = 0 ; i < 3 ; ++i) {
//            UIButton *button  = (UIButton *)[_scrollView viewWithTag:200 + i];
//            button.userInteractionEnabled = NO;
//            button.selected = NO;
//        }
    }
    [kUserDef setBool:sender.on forKey:kGPSEnable];
    [kUserDef synchronize];
}
- (void)enableReportSwitch:(UISwitch *)sender
{
    if (sender.on) {
        //        for (int i = 0 ; i < 3 ; ++i) {
        //            UIButton *button  = (UIButton *)[_scrollView viewWithTag:200 + i];
        //            button.userInteractionEnabled = YES;
        //        }
        [[ObjectCTools shared] showAlertViewAndDissmissAutomatic:NSLocalizedStringFromTable(@"GPS auto report is", LanguageFileName, nil) andMessage:NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil) withDissmissTime:KDissmissTime withDelegate:nil withAction:nil];
    }else{
        //        for (int i = 0 ; i < 3 ; ++i) {
        //            UIButton *button  = (UIButton *)[_scrollView viewWithTag:200 + i];
        //            button.userInteractionEnabled = NO;
        //            button.selected = NO;
        //        }
    }
    [kUserDef setBool:sender.on forKey:kGPSAutoReportEnable];
    [kUserDef synchronize];
}



#pragma mark    Geofence Setup 地理围栏 SEND：SET_PW_#SWGEOFENCE#X#
- (void)geofenceEnableSwitch:(UISwitch *)sender
{
    if (sender.on) {
        
        [[ObjectCTools shared] showAlertViewAndDissmissAutomatic:NSLocalizedStringFromTable(@"Geofence is", LanguageFileName, nil) andMessage:NSLocalizedStringFromTable(@"Enable", LanguageFileName, nil) withDissmissTime:KDissmissTime withDelegate:nil withAction:nil];
        self.rangelider.userInteractionEnabled = YES;
        
    }else {
        
        self.rangelider.userInteractionEnabled = NO;
        self.rangelider.value = 0;
        _valueView.frame = CGRectMake(self.rangelider.x - 25 + 7.5 + (self.rangelider.width - 15)*(self.rangelider.value/1000.0),self.rangelider.y - 27 , 50, 22.5);
        _valueView.value = [NSString stringWithFormat:@"%ld m",lroundf(self.rangelider.value)];

        _circ.map = nil;
    }
    
    [kUserDef setBool:sender.on forKey:kGEOEnable];
    [kUserDef synchronize];
    //地理围栏 SEND：SET_PW_#SWGEOFENCE#X#

//    NSString *body = [NSString stringWithFormat:@"SET%@#SWGEOFENCE#%d#",kMessagePassword,sender.on];
//    [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"AGPS setup", LanguageFileName, nil) body:body withDelegate:self];

}

#pragma mark - send GET SET

- (void)checkButton:(UIButton *)sender
{
    [super checkButton:sender];

    int x1 = 0;
    if (self.setGetSegmet.selectedSegmentIndex == 0) {
        
        for (int i = 0 ; i < 3 ; ++i) {
            UIButton *button  = (UIButton *)[_scrollView viewWithTag:200 + i];
            if (button.selected) {
                x1 = i;
                break;
            }
        }
        UISwitch *as = (UISwitch *)[self.view viewWithTag:300];
        UISwitch *geos = (UISwitch *)[self.view viewWithTag:301];
        UISwitch *gg = (UISwitch *)[self.view viewWithTag:308];
        
        int xtime = 5;
        if(self.volumeSlider.value>=0 && self.volumeSlider.value<1.5)
        {
            xtime = 5;
        }
        if(self.volumeSlider.value>=1.5 && self.volumeSlider.value<2.5)
        {
            xtime = 15;
        }

        if(self.volumeSlider.value>=2.5 && self.volumeSlider.value<3.5)
        {
            xtime = 30;
        }

        if(self.volumeSlider.value>=3.5 && self.volumeSlider.value <4.5)
        {
            xtime = 60;
        }

        if(self.volumeSlider.value>=4.5 && self.volumeSlider.value<5.5)
        {
            xtime = 120;
        }
        if(self.volumeSlider.value>=5.5)
        {
            xtime = 240;
        }

        
        
        NSString *body = nil;
        if (geos.on) {
         body = [NSString stringWithFormat:@"SET#AGPS#%d#SWGPS#%d#SWGEOFENCE#%d#GPSRANGE#%ld#HOMESITE#%.4f,%.4f#SWGPSREPORT#%d#GPSREPORTTIME#%d#",x1,as.on,geos.on,lroundf(self.rangelider.value),_coordinate.latitude,_coordinate.longitude,gg.on,xtime];
            if (self.numberField.text.length) {
                body = [body stringByAppendingString:[NSString stringWithFormat:@"GEOFENCEPHONE#%@#",self.numberField.text]];
            }
            
        }else
        {
            body = [NSString stringWithFormat:@"SET#AGPS#%d#SWGPS#%d#SWGEOFENCE#%d#SWGPSREPORT#%d#GPSREPORTTIME#%d#",x1,as.on,geos.on,gg.on,xtime];
            if (self.numberField.text.length) {
                body = [body stringByAppendingString:[NSString stringWithFormat:@"GEOFENCEPHONE#%@#",self.numberField.text]];
            }
            
        }
        NSLog(@"ssss%@",body);
        
        //保存临时数据
        [kUserDef setObject:[NSString stringWithFormat:@"%d",x1] forKey:@"AGPS"];
        [kUserDef setObject:[NSString stringWithFormat:@"%d",as.on] forKey:@"SWGPS"];
        [kUserDef setObject:[NSString stringWithFormat:@"%d",geos.on] forKey:@"SWGEOFENCE"];
        [kUserDef setObject:[NSString stringWithFormat:@"%ld",lroundf(self.rangelider.value)] forKey:@"GPSRANGE"];
        [kUserDef setObject:[NSString stringWithFormat:@"%@",self.numberField.text] forKey:@"GEOFENCEPHONE"];
        [kUserDef setObject:[NSString stringWithFormat:@"%d",gg.on] forKey:kGPSAutoReportEnable];
        
        [kUserDef synchronize];
        
        NSLog(@"保存临时数据成功");

      
        
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"SET About GPS", LanguageFileName, nil) body:body withDelegate:self];
        
    }else
    {
        NSString *body = [NSString stringWithFormat:@"GET#AGPS#SWGPS#SWGEOFENCE#GPSRANGE#HOMESITE#GEOFENCEPHONE#GPSREPORTTIME#SWGPSREPORT"];
        
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"GET About GPS", LanguageFileName, nil) body:body withDelegate:self];
    }
    
    
}

#pragma mark 切换 SET GET
- (void)setGetSegmet:(UISegmentedControl *)sender
{
    [super setGetSegmet:sender];
    if (sender.selectedSegmentIndex == 0) {
        _setBgView.hidden = NO;
        _getBgView.hidden = YES;
    }else
    {
        _setBgView.hidden = YES;
        _getBgView.hidden = NO;
        
        LKDBHelper* globalHelper = [GeofenceModel getUsingLKDBHelper];
        
        //异步 asynchronous
        [globalHelper search:[GeofenceModel class] where:nil orderBy:@"updateTime desc" offset:0 count:50 callback:^(NSMutableArray *array) {
            for (GeofenceModel *model in array) {
                NSLog(@"%@ -- %@",model.name,model.updateTime);
            }
            
            _dataArray.count?([_dataArray removeAllObjects]):nil;
            [_dataArray addObjectsFromArray:array];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_tableView reloadData];
            });
            
        }];
        
    }
}

- (void)editButton:(UIButton *)sender{
    
    if (_isEdit) {
        //send set low battery warning number
        
        if (self.numberField.text.length) {
            NSLog(@"set Low Battery number");
            
            [self.editButton setTitle:NSLocalizedStringFromTable(@"Change", LanguageFileName, nil) forState:UIControlStateNormal];
            [self.editButton setBackgroundColor:kGrayDarkColor];
            [self.numberField resignFirstResponder];
            //            NSString *body = [NSString stringWithFormat:@"SET9999#SWECI#%d#",enableSwitch.on];
            //            [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Set Detect", LanguageFileName, nil) body:body withDelegate:self];
            
            
        }else
        {
            [self.numberField shake];
            return;
        }
    }else
    {//change number
        NSLog(@"change  number");
        if (self.numberField.text.length) {
            [self.editButton setTitle:NSLocalizedStringFromTable(@"Set", LanguageFileName, nil) forState:UIControlStateNormal];
            [self.editButton setBackgroundColor:kBlueColor];
            [self.numberField becomeFirstResponder];
            
        }
        
    }
    
    self.isEdit = !_isEdit;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField.text.length) {
        self.isEdit = YES;
    }
    return YES;
}

- (void)setIsEdit:(BOOL)isEdit
{
    _isEdit = isEdit;
    if (isEdit) {
        [self.editButton setBackgroundColor:kBlueColor];
    }else
    {
        [self.editButton setBackgroundColor:kGrayDarkColor];
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)volumeSlider:(UISlider *)sender
{
    
    NSLog(@"%ld -- %f",lroundf(sender.value),sender.value);
    float f = lroundf(sender.value);
    [UIView animateWithDuration:0.5 animations:^{
        sender.value = f;
        
    }];
    [kUserDef setFloat:f forKey:KReportTime];
    [kUserDef synchronize];
    
    if (sender.value >= 1 && sender.value < 1.5 ) {
        for (int i = 0 ; i < 6; i ++) {
            UIImageView *pointView = [_scrollView viewWithTag:400 + i];
            if (i+1 <= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];
                
            }
        }
    }else if (sender.value >= 1.5 && sender.value < 2.5 ) {
        for (int i = 0 ; i < 6; i ++) {
            UIImageView *pointView = [_scrollView viewWithTag:400 + i];
            if (i+1<= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];
                
            }
        }
        
    }else if(sender.value >= 2.5 && sender.value < 3.5){
        for (int i = 0 ; i < 6; i ++) {
            UIImageView *pointView = [_scrollView viewWithTag:400 + i];
            if (i+1<= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];
                
            }
        }
        
    }else if(sender.value >= 3.5 && sender.value < 4.5){
        for (int i = 0 ; i < 6; i ++) {
            UIImageView *pointView = [_scrollView viewWithTag:400 + i];
            if (i+1<= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];
                
            }
        }
        
    }else if(sender.value >= 4.5 && sender.value<5.5){
        for (int i = 0 ; i < 6; i ++) {
            UIImageView *pointView = [_scrollView viewWithTag:400 + i];
            if (i+1<= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];
                
            }
        }
        
    }
    else if(sender.value >= 5.5){
        for (int i = 0 ; i < 6; i ++) {
            UIImageView *pointView = [_scrollView viewWithTag:400 + i];
            if (i+1<= sender.value) {
                pointView.image = [UIImage imageNamed:@"point_red_small"];
            }else
            {
                pointView.image = [UIImage imageNamed:@"point_black"];
                
            }
        }
        
    }
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
