//
//  LanguageViewController.m
//  Thomson
//
//  Created by 何助金 on 10/22/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "LanguageViewController.h"
#import "HistoryTableViewCell.h"
#import "GetMNumberTableViewCell.h"
#import "LanguageModel.h"

@interface LanguageViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    NSMutableArray *_dataArray;
    UITableView *_getTableView;
    NSMutableArray *_getDataArray;
    UIView *_setBgView;
    UIView *_getBgView;
}
@end

@implementation LanguageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _getDataArray = [NSMutableArray array];
    
    [self setupLanguagesArray];
    
    _setBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 - 64 )];
    _setBgView.backgroundColor = kWhiteColor;
    _setBgView.hidden = NO;
    [self.view addSubview:_setBgView];
    
    _getBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 - 64)];
    _getBgView.backgroundColor = kWhiteColor;
    _getBgView.hidden = YES;
    [self.view addSubview:_getBgView];
    
    UILabel *infoLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,0, kScreenWidth - 2 * kLeftMark , 35)
                                                     backgroundColor:[UIColor clearColor]
                                                                text:NSLocalizedStringFromTable(@"Set language for the voice prompt announcement", LanguageFileName, nil)
                                                           textColor:kBlackLightColor
                                                                font:kHelveticaLightFont(15)
                                                       textAlignment:NSTextAlignmentLeft
                                                       lineBreakMode:NSLineBreakByWordWrapping
                                                       numberOfLines:0];
    
    infoLabel.adjustsFontSizeToFitWidth = YES;
    [_setBgView addSubview:infoLabel];
    [self addTableView];
    [self addGetTableView];
}

- (void)setupLanguagesArray {
    _dataArray = [NSMutableArray arrayWithArray:@[NSLocalizedStringFromTable(@"English", LanguageFileName, nil),
                                                 NSLocalizedStringFromTable(@"Dutch", LanguageFileName, nil),
                                                 NSLocalizedStringFromTable(@"French", LanguageFileName, nil),
                                                 NSLocalizedStringFromTable(@"German", LanguageFileName, nil),
                                                 NSLocalizedStringFromTable(@"Portuguese", LanguageFileName, nil),
                                                 NSLocalizedStringFromTable(@"Spanish", LanguageFileName, nil),
                                                 NSLocalizedStringFromTable(@"Turkish", LanguageFileName, nil),
                                                 NSLocalizedStringFromTable(@"Italian", LanguageFileName, nil),
                                                  NSLocalizedStringFromTable(@"Hebrew", LanguageFileName, nil)]];
}


- (void) addTableView
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 35, kScreenWidth, _setBgView.height - 35) style:UITableViewStylePlain];
    
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    _tableView.separatorColor = [UIColor grayColor];
    [_setBgView addSubview:_tableView];
}

- (void) addGetTableView
{
    _getTableView = [[UITableView alloc] initWithFrame:_getBgView.bounds style:UITableViewStylePlain];
    
    [_getTableView setBackgroundColor:[UIColor clearColor]];
    [_getTableView setDelegate:self];
    [_getTableView setDataSource:self];
    _getTableView.tableFooterView = [UIView new];
    _getTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_getBgView addSubview:_getTableView];
}
#pragma mark -tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _tableView) {
        return _dataArray.count;
    }else
    {
        return 1+ _getDataArray.count;
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _tableView) {
        return 40;
    }
    return 30;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tableView) {
        HistoryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"HistoryTableViewCell" owner:self options:nil] lastObject];
            UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2 *15, 1.0)];
            [cell.contentView addSubview:line];
            cell.mLable.textColor = kBlackLightColor;
            cell.dateLabel.textColor = kBlackColor;
            line.y = 39;
            line.x = 15;
        }
        
        cell.mLable.text = _dataArray[indexPath.row];
        if ([kUserDef integerForKey:kLan]>= 0) {
            selectIndex = [NSIndexPath indexPathForRow:[kUserDef integerForKey:kLan] inSection:0] ;
        }
        if ([indexPath isEqual: selectIndex]) {
            cell.dateLabel.text = @"√";
            cell.dateLabel.textColor = kBlueColor;
        }else
        {
            cell.dateLabel.text = @">";
            cell.dateLabel.textColor = kBlackColor;
        }
        
        return cell;
        
    }else
    {
        if (indexPath.row == 0){
            
            GetMNumberTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GetMNumberTableViewCell"];
            if (cell == nil) {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"GetMNumberTableViewCell" owner:self options:nil] lastObject];
            }
            
            cell.mlable.text = NSLocalizedStringFromTable(@"Update History", LanguageFileName, nil);
            cell.numberLable.text = nil;
            return cell;
            
        }else
        {
            HistoryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryCell"];
            if (cell == nil) {
                cell = [[[NSBundle mainBundle] loadNibNamed:@"HistoryTableViewCell" owner:self options:nil] lastObject];
                UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2 *15, 1.0)];
                [cell.contentView addSubview:line];
                line.y = 29;
                line.x = 15;
            }
            
            if (_getDataArray.count) {
                
                LanguageModel *model = _getDataArray[indexPath.row - 1];
                cell.mLable.text = model.name;
                cell.dateLabel.text = [DateHelper timeSPToTimeStringWithString:model.updateTime];
                
            }else {
                cell.mLable.text = @"";
                cell.dateLabel.text = @"";
            }
            return cell;
        }
        
    }
}

static NSIndexPath *selectIndex = nil;

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    selectIndex = indexPath;
    
    [[ObjectCTools shared] showAlertViewAndDissmissAutomatic: nil andMessage: [NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTable(@"Language has been replaced to", LanguageFileName, nil), _dataArray[selectIndex.row]] withDissmissTime: KDissmissTime withDelegate: nil withAction: nil];
    [kUserDef setInteger:indexPath.row forKey:kLan];
    [kUserDef synchronize];
    
    
    switch (selectIndex.row) {
        case 0:
            LanguageFileName = @"en";
            break;
        case 1:
            LanguageFileName = @"dutch";
            break;
        case 2:
            LanguageFileName = @"french";
            break;
        case 3:
            LanguageFileName = @"german";
            break;
        case 4:
            LanguageFileName = @"portuguese";
            break;
        case 5:
            LanguageFileName = @"spanish";
            break;
        case 6:
            LanguageFileName = @"turkish";
            break;
        case 7:
            LanguageFileName = @"italian";
            break;
        case 8:
            LanguageFileName = @"hebrew";
            break;
            
        default:
            break;
    }
    
    
    
    [kUserDef setObject:LanguageFileName forKey:KLanguageFileName];
    [kUserDef synchronize];
    
    [self setupLanguagesArray];
    [tableView reloadData];
}


#pragma mark 切换 SET GET

- (void)setGetSegmet:(UISegmentedControl *)sender
{
    [super setGetSegmet:sender];
    if (sender.selectedSegmentIndex == 0) {
        _setBgView.hidden = NO;
        _getBgView.hidden = YES;
    }else
    {
        _setBgView.hidden = YES;
        _getBgView.hidden = NO;
        
        LKDBHelper* globalHelper = [LanguageModel getUsingLKDBHelper];
        
        //异步 asynchronous
        [globalHelper search:[LanguageModel class] where:nil orderBy:@"updateTime desc" offset:0 count:50 callback:^(NSMutableArray *array) {
            for (LanguageModel *model in array) {
                NSLog(@"%@ -- %@",model.name,model.updateTime);
            }
            _getDataArray.count?([_getDataArray removeAllObjects]):nil;
            [_getDataArray addObjectsFromArray:array];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_getTableView reloadData];
            });
            
            
        }];
        
    }
    
    
}


- (void)checkButton:(UIButton *)sender
{
    [super checkButton:sender];
    
    if (self.setGetSegmet.selectedSegmentIndex == 0) {
        NSString *body = [NSString stringWithFormat:@"SET#LANG#%ld#",(long)selectIndex.row];
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Set Language", LanguageFileName, nil) body:body withDelegate:self];
        //        LanguageModel *model = [[LanguageModel alloc]init];
        //        model.name = _dataArray[selectIndex.row];
        //        model.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
        //        [model saveToDB];
    }else{
        NSString *body = [NSString stringWithFormat:@"GET#LANG#"];
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Get Language", LanguageFileName, nil) body:body withDelegate:self];
    }
    
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    switch (result) {
        case MessageComposeResultSent:{
            //信息传送成功
            NSLog(@"信息传送成功");
            
            LanguageModel *model = [[LanguageModel alloc]init];
            model.name = _dataArray[selectIndex.row];
            model.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
            [model saveToDB];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case MessageComposeResultFailed:
            //信息传送失败
            NSLog(@"信息传送失败");
            [self.navigationController popViewControllerAnimated:YES];
            
            break;
        case MessageComposeResultCancelled:
            //信息被用户取消传送
            NSLog(@"信息被用户取消传送");
            break;
        default:
            break;
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
