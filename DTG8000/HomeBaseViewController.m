//
//  HomeBaseViewController.m
//  Thomson
//
//  Created by 何助金 on 10/20/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "HomeBaseViewController.h"
#import "SwitchDeviceViewController.h"
#import "AddDeviceViewController.h"

@interface HomeBaseViewController ()

@end

@implementation HomeBaseViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (![NSString isNilOrEmpty:kMessageName]) {
        self.titleLable.text = [NSString stringWithFormat:@"%@%@",kMessageName,NSLocalizedStringFromTable(@"'s Conecto", LanguageFileName, nil)];
        //        self.titleLable.minimumScaleFactor = 0.6;
        //        self.titleLable.adjustsFontSizeToFitWidth = YES;
    }
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (self.navigationController.viewControllers.count <2) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }else{
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createHeadViews];
}


- (void)createHeadViews
{
    self.scrollview = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    self.scrollview.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.scrollview];
    
    _headBaseView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 60)];
    _headBaseView.backgroundColor = kGrayLightColor;
    [self.scrollview addSubview:_headBaseView];
    
    _titleLable = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark, 0, kScreenWidth/2 - kLeftMark - 10, 30) backgroundColor:KClearColor text:kMessageName textColor:kBlackLightColor font:kHelveticaLightFont(15) textAlignment:NSTextAlignmentLeft lineBreakMode:NSLineBreakByTruncatingTail numberOfLines:1];
    [_titleLable sizeToFit];
    
    _titleLable.centerY = _headBaseView.centerY;
    [_headBaseView addSubview:_titleLable];
    
    CGFloat margin = 10;
    CGFloat buttonWidth = (kScreenWidth - kLeftMark - _titleLable.right - margin * 2) / 2;
    UIButton *addDeviceButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage: CGRectMake(_titleLable.right + margin, 0, buttonWidth, 35)
                                                                          backgroudColor:kGrayDarkColor
                                                                        titleNormalColor:[UIColor whiteColor]
                                                                   titleHighlightedColor:[UIColor grayColor]
                                                                                   title:NSLocalizedStringFromTable(@"Add Device", LanguageFileName, nil)
                                                                                    font:kHelveticaLightFont(12)
                                                                            cornerRadius:5
                                                                             borderWidth:0.5
                                                                             borderColor:KClearColor.CGColor
                                                                      accessibilityLabel:nil];
    [addDeviceButton addTarget: self action:@selector(addDeviceButton:) forControlEvents:UIControlEventTouchUpInside];
    [addDeviceButton.titleLabel sizeToFit];
    addDeviceButton.centerY = _headBaseView.centerY;
    [_headBaseView addSubview:addDeviceButton];
    
    UIButton *switchDeviceButton = [[ObjectCTools shared] getACustomButtonNoBackgroundImage: CGRectMake(addDeviceButton.right + margin , 0, buttonWidth, 35)
                                                                             backgroudColor:kGrayDarkColor
                                                                           titleNormalColor:[UIColor whiteColor]
                                                                      titleHighlightedColor:[UIColor grayColor]
                                                                                      title:NSLocalizedStringFromTable(@"Switch Device", LanguageFileName, nil)
                                                                                       font:kHelveticaLightFont(12)
                                                                               cornerRadius:5
                                                                                borderWidth:0.5
                                                                                borderColor:KClearColor.CGColor
                                                                         accessibilityLabel:nil];
    [switchDeviceButton addTarget: self action:@selector(switchDeviceButton:) forControlEvents:UIControlEventTouchUpInside];
    [switchDeviceButton.titleLabel sizeToFit];
    switchDeviceButton.centerY = _headBaseView.centerY;
    [_headBaseView addSubview:switchDeviceButton];
}

- (void)addDeviceButton:(UIButton *)sender
{
    AddDeviceViewController *switchVC = [[AddDeviceViewController alloc]init];
    [self.navigationController pushViewController:switchVC animated:YES];
}

- (void)switchDeviceButton:(UIButton *)sender
{
    SwitchDeviceViewController *switchVC = [[SwitchDeviceViewController alloc]init];
    [self.navigationController pushViewController:switchVC animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
