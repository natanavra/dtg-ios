//
//  SOSMessageTableViewCell.m
//  Thomson
//
//  Created by 何助金 on 10/24/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "SOSMessageTableViewCell.h"

@implementation SOSMessageTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self _initViews];
    }
    
    return self;
}


- (void)_initViews{
    
}
- (void)awakeFromNib {
    
    [super awakeFromNib];

    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(kLeftMark, 0, kScreenWidth - 2*kLeftMark, 165)];
    bgView.layer.backgroundColor = kGrayLightColor.CGColor;
    bgView.layer.cornerRadius = 3.0;
    [self.contentView addSubview:bgView];
    [self.contentView sendSubviewToBack:bgView];
    _selectedArray = [NSMutableArray array];

    
    for (int i = 0 ; i < 8 ; ++i ) {
        UIButton *button = [self.contentView viewWithTag:100 + i ];
         //button.titleLabel.text = [NSString stringWithFormat:@"%@ %d",NSLocalizedStringFromTable(@"PHONE", LanguageFileName, nil),i+1];
        [button setTitle:[NSString stringWithFormat:@"%@ %d",NSLocalizedStringFromTable(@"PHONE", LanguageFileName, nil),i+1] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(selected:) forControlEvents:UIControlEventTouchUpInside];
//        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        
        
    }
    
    self.messageTitleLabel.textColor = kBlackLightColor;
    self.messageTitleLabel.font = kHelveticaRegularFont(14);
    self.editButton.backgroundColor = kBlueColor;
    self.editButton.layer.cornerRadius= 3.0;
    [self.editButton setTitleColor:[UIColor  grayColor] forState:UIControlStateHighlighted];
    self.messageField.delegate = self;
    self.isEdit = YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (self.delegate  && [self.delegate respondsToSelector:@selector(SOSMessageTableViewCellUpdateMessage: withSelectedArray:)]) {
        [self.delegate SOSMessageTableViewCellUpdateMessage:self withSelectedArray:_selectedArray];
    }

    return YES;
}

- (void)selected:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if (!_selectedArray) {
        _selectedArray = [NSMutableArray array];
    }
   
    if ([_selectedArray containsObject:@(sender.tag-100+1)]) {
        [_selectedArray removeObject:@(sender.tag-100+1)];
    }else
    {
        [_selectedArray addObject:@(sender.tag-100+1)];
    }
    
    NSMutableArray *selectedArray = [[NSMutableArray alloc] init];
    
    for (int i = 0 ; i < 8 ; ++i ) {
        UIButton *button = [self.contentView viewWithTag:100 + i];
        [button.titleLabel setAdjustsFontSizeToFitWidth:YES];
        if (button.selected) {
            [selectedArray addObject:@(i+1)];
        }
        
    }
    
    if (self.delegate  && [self.delegate respondsToSelector:@selector(SOSMessageTableViewCellUpdateMessage: withSelectedArray:)]) {
        [self.delegate SOSMessageTableViewCellUpdateMessage:self withSelectedArray:selectedArray];
    }
}

- (void)setSelectedArray:(NSMutableArray *)selectedArray
{
    _selectedArray = selectedArray;
    
    for (int i = 0 ; i < 8 ; ++i ) {
        UIButton *button = [self.contentView viewWithTag:100 + i ];
        
        if ([_selectedArray containsObject:@(button.tag-100+1)]) {
            button.selected = YES;
        }else
        {
            button.selected = NO;
        }
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Configure the view for the selected state
}

- (IBAction)editButtonPress:(UIButton *)sender {
    
    
    if (_isEdit) {
        //send set
        
        if (self.messageField.text.length) {
            
            self.isEdit = !_isEdit;

            [self.editButton setTitle:NSLocalizedStringFromTable(@"Change", LanguageFileName, nil) forState:UIControlStateNormal];
            [self.editButton setBackgroundColor:kGrayDarkColor];
 
            NSMutableArray *selectedArray = [[NSMutableArray alloc] init];
            
            for (int i = 0 ; i < 8 ; ++i ) {
                UIButton *button = [self.contentView viewWithTag:100 + i ];
                button.userInteractionEnabled = NO;
                self.messageField.userInteractionEnabled = NO;
                [self.messageField resignFirstResponder];
                if (button.selected) {
                    [selectedArray addObject:@(i+1)];
                }
                
            }
            
            NSLog(@"selectedArray:%@",selectedArray);
            
            
            if (self.delegate  && [self.delegate respondsToSelector:@selector(SOSMessageTableViewCell: withSelectedArray:)]) {
                [self.delegate SOSMessageTableViewCell:self withSelectedArray:selectedArray];
            }

            
        }else
        {
            [self.messageField shake];
            return;
        }
    }else
    {//change
        
        if (self.messageField.text.length) {
            [self.editButton setTitle:NSLocalizedStringFromTable(@"Set", LanguageFileName, nil) forState:UIControlStateNormal];
            [self.editButton setBackgroundColor:kBlueColor];
            [self.messageField becomeFirstResponder];
        }
        for (int i = 0 ; i < 8 ; ++i ) {
            UIButton *button = [self.contentView viewWithTag:100 + i ];
            button.userInteractionEnabled = YES;
            self.messageField.userInteractionEnabled = YES;
        }
        self.isEdit = !_isEdit;
        
    }
    
//    self.isEdit = !_isEdit;
}

@end
