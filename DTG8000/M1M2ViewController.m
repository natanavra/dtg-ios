//
//  M1M2ViewController.m
//  Thomson
//
//  Created by 何助金 on 10/20/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "M1M2ViewController.h"
#import "SOSNumberView.h"
#import "GetMNumberTableViewCell.h"
#import "HistoryTableViewCell.h"
#import "M1M2Model.h"
@interface M1M2ViewController ()<SOSNumberViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIView *_setBgView;
    UIView *_getBgView;
    UITableView *_tableView;
    NSMutableArray *_dataArray;
}
@end

@implementation M1M2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _dataArray = [NSMutableArray array];
    
    _setBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 )];
    _setBgView.backgroundColor = kWhiteColor;
    _setBgView.hidden = NO;
    [self.view addSubview:_setBgView];
    
    _getBgView  = [[UIView alloc]initWithFrame:CGRectMake(0, self.setGetSegmet.bottom + 30,kScreenWidth , kScreenHeight - self.setGetSegmet.bottom - 30 - 64)];
    _getBgView.backgroundColor = kWhiteColor;
    _getBgView.hidden = YES;
    [self.view addSubview:_getBgView];

    
    UILabel *infoLabel = [[ObjectCTools shared] getACustomLableFrame:CGRectMake(kLeftMark,0, kScreenWidth - 2*kLeftMark , 35)
                                backgroundColor:[UIColor clearColor]
                                           text:NSLocalizedStringFromTable(@"Setup Energency Call SOS Number", LanguageFileName, nil)
                                      textColor:kBlackLightColor
                                           font:kHelveticaLightFont(16)
                                  textAlignment:NSTextAlignmentLeft
                                  lineBreakMode:NSLineBreakByWordWrapping
                                  numberOfLines:0];
    
    infoLabel.adjustsFontSizeToFitWidth = YES;
    
    [_setBgView addSubview: infoLabel];
    
    SOSNumberView *m1 = [[SOSNumberView alloc]initWithFrame:CGRectMake(kLeftMark, infoLabel.bottom, 0.5*(kScreenWidth - 3*kLeftMark), 100)];
    m1.titleLabel.text = NSLocalizedStringFromTable(@"M1 NUMBER", LanguageFileName, nil);
    m1.delegate = self;
    m1.tag = 200;
    
    NSString *keydata =@"M1";
    //保存临时数据
    if([kUserDef stringForKey:keydata])
    {
        m1.numberField.text =[kUserDef objectForKey:keydata];
    }
    
    [_setBgView addSubview:m1];
    
    SOSNumberView *m2 = [[SOSNumberView alloc]initWithFrame:CGRectMake(m1.right + kLeftMark, infoLabel.bottom, 0.5*(kScreenWidth - 3*kLeftMark), 100)];
    m2.tag = 201;
    m2.titleLabel.text = NSLocalizedStringFromTable(@"M2 NUMBER", LanguageFileName, nil);
    m2.delegate = self;
    
    NSString *keydata1 =@"M2";
    //保存临时数据
    if([kUserDef stringForKey:keydata1])
    {
        m2.numberField.text =[kUserDef objectForKey:keydata1];
    }
    
    [_setBgView addSubview:m2];
    
    [self addTableView];
}


- (void) addTableView
{
    _tableView = [[UITableView alloc] initWithFrame:_getBgView.bounds style:UITableViewStylePlain];
    
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //    _tableView.separatorColor = [UIColor grayColor];
    [_getBgView addSubview:_tableView];
}
#pragma mark -tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5 + _dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0 || indexPath.row == 1) {
        return 50;
    }else{
        return 30;
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0 || indexPath.row == 1) {
        GetMNumberTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GetMNumberTableViewCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"GetMNumberTableViewCell" owner:self options:nil] lastObject];
            UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2 *15, 1.0)];
            [cell.contentView addSubview:line];
            line.y = 49;
            line.x = 15;
            
        }
        
        if (indexPath.row == 0) {
           
            for (M1M2Model *mode in _dataArray) {
                if ([mode.name isEqualToString:NSLocalizedStringFromTable(@"M1 Number", LanguageFileName, nil)]) {
                    if (![NSString isNilOrEmpty: mode.number]) {
                        cell.numberLable.text = mode.number;
                    }else
                    {
                        cell.numberLable.text = NSLocalizedStringFromTable(@"Unset", LanguageFileName, nil);

                    }
                    break;
                }else {
                    cell.numberLable.text = NSLocalizedStringFromTable(@"Unset", LanguageFileName, nil);
                }
                
            }
            cell.mlable.text = NSLocalizedStringFromTable(@"M1 Number", LanguageFileName, nil);
            if (_dataArray.count == 0) {
                cell.numberLable.text = NSLocalizedStringFromTable(@"Unset", LanguageFileName, nil);
            }

        }else
        {
            cell.mlable.text = NSLocalizedStringFromTable(@"M2 Number", LanguageFileName, nil);
            for (M1M2Model *mode in _dataArray) {
                if ([mode.name isEqualToString:NSLocalizedStringFromTable(@"M2 Number", LanguageFileName, nil)]) {
                    if (![NSString isNilOrEmpty: mode.number]) {
                        cell.numberLable.text = mode.number;
                    }else
                    {
                        cell.numberLable.text = NSLocalizedStringFromTable(@"Unset", LanguageFileName, nil);
                        
                    }
                    break;
                }else{
                        cell.numberLable.text = NSLocalizedStringFromTable(@"Unset", LanguageFileName, nil);
                    }
                }
        }
        if (_dataArray.count == 0) {
            cell.numberLable.text = NSLocalizedStringFromTable(@"Unset", LanguageFileName, nil);
        }
        
        return cell;
    }else if (indexPath.row == 2 || indexPath.row == 4){
        
        GetMNumberTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GetMNumberTableViewCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"GetMNumberTableViewCell" owner:self options:nil] lastObject];
        }
        if (indexPath.row == 2) {
            cell.mlable.text = NSLocalizedStringFromTable(@"Last Update", LanguageFileName, nil);
    
        }else
        {
            cell.mlable.text = NSLocalizedStringFromTable(@"Update History", LanguageFileName, nil);
            
        }
        cell.numberLable.text = nil;
        return cell;

    }else
    {
        HistoryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryCell"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"HistoryTableViewCell" owner:self options:nil] lastObject];
            UIView *line = [ZJLineView returnLineViewWithColor:kGrayColor withSize:CGSizeMake(kScreenWidth - 2 *15, 1.0)];
            [cell.contentView addSubview:line];
            line.y = 29;
            line.x = 15;
        }
        if (_dataArray.count) {
            if (indexPath.row == 3 || indexPath.row == 5) {
                M1M2Model *model = _dataArray[0];

                cell.mLable.text = [NSString stringWithFormat:@"%@:%@",model.name,model.number];
                cell.dateLabel.text = [DateHelper timeSPToTimeStringWithString:model.updateTime];
            }else
            {
                M1M2Model *model = _dataArray[indexPath.row-5];
                cell.mLable.text = [NSString stringWithFormat:@"%@:%@",model.name,model.number];
                cell.dateLabel.text = [DateHelper timeSPToTimeStringWithString:model.updateTime];
            }
        }else {
            cell.mLable.text = @"";
            cell.dateLabel.text = @"";
        }
        return cell;
    }

}


static bool isM1 = YES;
static bool isM1M2 = NO;
#pragma mark  单发 SET
- (void)sosNumberView:(SOSNumberView *)sosNumber
{
    //send set number
    if (sosNumber.tag == 200) {
        isM1 = YES;
//#warning test
//        M1M2Model *model1 = [[M1M2Model alloc]init];
//        model1.name = @"M1 Number";
//        model1.number = sosNumber.numberLabel.text;
//        model1.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
//        [model1 saveToDB];
//
//#warning test
        NSString *body = [NSString stringWithFormat:@"SET#M1#%@#",sosNumber.numberLabel.text];
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Setup M1 number", LanguageFileName, nil) body:body withDelegate:self];
        
        NSString *keydata =[NSString stringWithFormat:@"M1"];
        //保存临时数据
        [kUserDef setObject:sosNumber.numberField.text forKey:keydata];
        [kUserDef synchronize];
        
    }else
    {
        isM1 = NO;
        NSString *body = [NSString stringWithFormat:@"SET#M2#%@#",sosNumber.numberLabel.text];
        
        NSString *keydata =[NSString stringWithFormat:@"M2"];
        //保存临时数据
        [kUserDef setObject:sosNumber.numberField.text forKey:keydata];
        [kUserDef synchronize];
        
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Setup M2 number", LanguageFileName, nil) body:body withDelegate:self];


    }
    
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    SOSNumberView *m1 = (SOSNumberView *)[self.view viewWithTag:200];
    SOSNumberView *m2 = (SOSNumberView *)[self.view viewWithTag:201];
    [self dismissViewControllerAnimated:YES completion:nil];
    switch (result) {
        case MessageComposeResultSent:
        {
            //信息传送成功
            NSLog(@"信息传送成功");
            if (!isM1M2) {
                if (isM1) {
                    [[ObjectCTools shared] showAlertViewAndDissmissAutomatic:m1.numberLabel.text andMessage:NSLocalizedStringFromTable(@"Added to the M1 Number", LanguageFileName, nil) withDissmissTime:KDissmissTime withDelegate:nil withAction:nil];
                    
                    M1M2Model *model1 = [[M1M2Model alloc]init];
                    model1.name = @"M1 Number";
                    model1.number = m1.numberLabel.text;
                    model1.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
                    [model1 saveToDB];



                }else
                {
                    [[ObjectCTools shared] showAlertViewAndDissmissAutomatic:m2.numberLabel.text andMessage:NSLocalizedStringFromTable(@"Added to the M2 Number", LanguageFileName, nil) withDissmissTime:KDissmissTime withDelegate:nil withAction:nil];
                    
                    M1M2Model *model2 = [[M1M2Model alloc]init];
                    model2.name = @"M2 Number";
                    model2.number = m1.numberLabel.text;
                    model2.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
                    [model2 saveToDB];
                }

            }
            if (isM1M2) {
                [self.navigationController popViewControllerAnimated:YES];
                if (m1.numberLabel.text.length) {
                    M1M2Model *model1 = [[M1M2Model alloc]init];
                    model1.name = @"M1 Number";
                    model1.number = m1.numberLabel.text;
                    model1.updateTime = [DateHelper timeSpFromDate:[NSDate date]];
                    [model1 saveToDB];
                }
                if (m2.numberLabel.text.length) {
                    M1M2Model *model2 = [[M1M2Model alloc]init];
                    model2.name = @"M2 Number";
                    model2.number = m2.numberLabel.text;
                    model2.updateTime = [DateHelper timeSpFromDate:[NSDate date]];;
                    [model2 saveToDB];
                }
            }
        }
            break;
        case MessageComposeResultFailed:
            //信息传送失败
            NSLog(@"信息传送失败");
            if (isM1M2) {
                [self.navigationController popViewControllerAnimated:YES];
            }

            break;
        case MessageComposeResultCancelled:
            //信息被用户取消传送
            NSLog(@"信息被用户取消传送");
            break;
        default:
            break;
    }
    
}
#pragma mark 切换 SET GET
- (void)setGetSegmet:(UISegmentedControl *)sender
{
    [super setGetSegmet:sender];
    if (sender.selectedSegmentIndex == 0) {
        _setBgView.hidden = NO;
        _getBgView.hidden = YES;
    }else
    {
        _setBgView.hidden = YES;
        _getBgView.hidden = NO;
        
        LKDBHelper* globalHelper = [M1M2Model getUsingLKDBHelper];
        
        //异步 asynchronous
        [globalHelper search:[M1M2Model class] where:nil orderBy:@"updateTime desc" offset:0 count:50 callback:^(NSMutableArray *array) {
            for (M1M2Model *model in array) {
                NSLog(@"password?%@ %@ %@",model.name,model.number,model.updateTime);
            }
            _dataArray.count?([_dataArray removeAllObjects]):nil;
            [_dataArray addObjectsFromArray:array];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_tableView reloadData];
            });

            
        }];

    }
}

#pragma mark 多发 SET /GET
- (void)checkButton:(UIButton *)sender
{
    [super checkButton:sender];

    isM1M2 = YES;
    SOSNumberView *m1 = (SOSNumberView *)[self.view viewWithTag:200];
    SOSNumberView *m2 = (SOSNumberView *)[self.view viewWithTag:201];

    if (self.setGetSegmet.selectedSegmentIndex == 0) {
        //SET_PW_#M1#13430366028#M2#10086#
        NSString *body = [NSString stringWithFormat:@"SET#M1#%@#M2#%@#",m1.numberField.text,m2.numberField.text];
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Setup M1&M2 number", LanguageFileName, nil) body:body withDelegate:self];
        
        //保存临时数据
        [kUserDef setObject:m1.numberField.text forKey:@"M1"];
        [kUserDef setObject:m2.numberField.text forKey:@"M2"];
        [kUserDef synchronize];
    }else
    {
        NSString *body = [NSString stringWithFormat:@"GET#M1#M2#"];
        
        
        
        [self showMessageView:@[kMessageNumber] title:NSLocalizedStringFromTable(@"Get M1&M2 number", LanguageFileName, nil) body:body withDelegate:self];

    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
