//
//  EventButton.m
//  Thomson
//
//  Created by 何助金 on 10/19/15.
//  Copyright © 2015 MissionSky. All rights reserved.
//

#import "EventButton.h"

@implementation EventButton

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setTitleColor:kBlueColor forState:UIControlStateNormal];
        [self setTitleColor:kGrayColor forState:UIControlStateHighlighted];

        self.titleLabel.font = kHelveticaLightFont(15);
        self.titleLabel.frame = CGRectMake(self.titleLabel.frame.origin.x, self.titleLabel.frame.origin.y + 10, self.titleLabel.frame.size.width, self.titleLabel.frame.size.height);
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.numberOfLines = 0;
        
        self.titleLabel.adjustsFontSizeToFitWidth = YES;
        _imageBgview = [[UIView alloc]initWithFrame:CGRectZero];
        _imageBgview.layer.backgroundColor = kBlackLightColor.CGColor;
        _imageBgview.userInteractionEnabled = YES;

        [self addSubview:_imageBgview];
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.titleLabel.x = 0;
    self.titleLabel.y = self.height - 20;
    self.titleLabel.width = self.width;
    self.titleLabel.height = 0.25 * self.height;
    
    _imageBgview.y = 0;
    _imageBgview.x = 0;
    _imageBgview.height = self.height*0.75;
    _imageBgview.width = self.width;
    _imageBgview.layer.cornerRadius = 5.0;
    _imageBgview.layer.borderWidth = 1.0;
    _imageBgview.layer.borderColor = KClearColor.CGColor;

    
    self.imageView.bounds = CGRectMake(0, 0, _imageBgview.height-10, _imageBgview.height-10);
    self.imageView.centerX = self.width/2.0;
    self.imageView.centerY = _imageBgview.height/2.0;
    self.imageView.contentMode = UIViewContentModeCenter;
    self.imageView.userInteractionEnabled = YES;
//    [self bringSubviewToFront:self.imageView];
    [self sendSubviewToBack:_imageBgview];

}


@end
