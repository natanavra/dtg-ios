# ZJPublicPod

[![CI Status](http://img.shields.io/travis/Jason.He/ZJPublicPod.svg?style=flat)](https://travis-ci.org/Jason.He/ZJPublicPod)
[![Version](https://img.shields.io/cocoapods/v/ZJPublicPod.svg?style=flat)](http://cocoapods.org/pods/ZJPublicPod)
[![License](https://img.shields.io/cocoapods/l/ZJPublicPod.svg?style=flat)](http://cocoapods.org/pods/ZJPublicPod)
[![Platform](https://img.shields.io/cocoapods/p/ZJPublicPod.svg?style=flat)](http://cocoapods.org/pods/ZJPublicPod)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ZJPublicPod is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ZJPublicPod"
```

## Author

Jason.He, jason.he@missionsky.com

## License

ZJPublicPod is available under the MIT license. See the LICENSE file for more info.
