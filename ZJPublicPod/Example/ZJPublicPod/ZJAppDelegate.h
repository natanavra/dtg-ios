//
//  ZJAppDelegate.h
//  ZJPublicPod
//
//  Created by Jason.He on 08/01/2015.
//  Copyright (c) 2015 Jason.He. All rights reserved.
//

@import UIKit;

@interface ZJAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
