//
//  ZJViewController.m
//  ZJPublicPod
//
//  Created by Jason.He on 08/01/2015.
//  Copyright (c) 2015 Jason.He. All rights reserved.
//

#import "ZJViewController.h"
#import <ZJPublicPod/ZJPublic.h>
@interface ZJViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *_tableView;
    NSMutableArray *_dataArray;
    
}
@end

@implementation ZJViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    UIImageView *image =[[UIImageView alloc]initWithFrame:self.view.bounds];
//    [image sd_setImageWithURL:@""];

    _dataArray = [NSMutableArray array];
    [_dataArray addObjectsFromArray:@[@"拍照/多选",@"二维码扫描/识别相册"]];
    [self addTableView];
}

- (void) addTableView
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,0 ,kScreenWidth, kScreenHeight- 64) style:UITableViewStylePlain];
    
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorColor = [UIColor grayColor];
    [self.view addSubview:_tableView];
}
#pragma mark -tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.textLabel.text = _dataArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
        {
//            [self takePhtoto];
        }
        break;
        case 1:
        {
            
        }
            break;
            
        default:
            break;
    }
}

//- (void)takePhtoto
//{
//    if (!_takePhotoAS) {
//        _takePhotoAS = [[ZJTakePhotoActionSheet alloc]init];
//    }
//    _takePhotoAS.maxNumber = 3;
//    _takePhotoAS.takePhotoDelegate = self;
//    _takePhotoAS.owner = self;
//    [_takePhotoAS showInView:self.view];
//    
//}

//#pragma  mark - ZJTakePhotoActionSheet
//
//- (void)takePhotoActionSheet:(ZJTakePhotoActionSheet *)actionSheet didSelectedImageAtPath:(NSString *)path
//{
//    NSLog(@"照片路径：%@",path);
//}
//
//- (void)takePhotoActionSheet:(ZJTakePhotoActionSheet *)actionSheet didSelectedImagesPath:(NSMutableArray *)arrPaths
//{
//    NSLog(@"照片路径arrPaths：%@",arrPaths);
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
