//
//  main.m
//  ZJPublicPod
//
//  Created by Jason.He on 08/01/2015.
//  Copyright (c) 2015 Jason.He. All rights reserved.
//

@import UIKit;
#import "ZJAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ZJAppDelegate class]));
    }
}
