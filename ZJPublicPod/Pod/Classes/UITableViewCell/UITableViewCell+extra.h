//
//  UITableViewCell+extra.h
//  guru
//
//  Created by jamie on 15/3/30.
//  Copyright (c) 2015年 Missionsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (extra)

/**
 * @brief 抖动
 */
- (void) shake;

@end
