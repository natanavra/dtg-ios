//
//  SegmentTableViewCell.h
//  ChengXie
//
//  Created by jamie on 15/6/27.
//  Copyright (c) 2015年 Missionsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwithTableViewCell : UITableViewCell

@property(nonatomic,strong)UILabel *titleLabel;

@property (nonatomic, strong)UISwitch *optionSwitch;
@end
