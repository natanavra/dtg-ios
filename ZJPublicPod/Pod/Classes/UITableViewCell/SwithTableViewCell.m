//
//  SegmentTableViewCell.m
//  ChengXie
//
//  Created by jamie on 15/6/27.
//  Copyright (c) 2015年 Missionsky. All rights reserved.
//

#import "SwithTableViewCell.h"
#import "UIView_extra.h"
#define kLeftMargin 15.0
@implementation SwithTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self addSubview: self.titleLabel];
        [self addSubview:self.optionSwitch];
        self.backgroundColor =[UIColor whiteColor];
    }
    
    return self;
}
- (void)awakeFromNib {
    // Initialization code
    [self addSubview: self.titleLabel];
    [self addSubview:self.optionSwitch];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

 -(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.optionSwitch.x = self.width-51-kLeftMargin;
    self.optionSwitch.y = (self.height - 31)/2.0;
    
    self.titleLabel.frame = CGRectMake(kLeftMargin, 0, self.width-self.optionSwitch.width-10, self.height);
    
    
}

    

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
//        _titleLabel.textColor = KPageBlackFontColor;
    
    }
    
    return  _titleLabel;
}

- (UISwitch *)optionSwitch
{

    if (!_optionSwitch) {
        _optionSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
    }
    
    return  _optionSwitch;
}
@end
