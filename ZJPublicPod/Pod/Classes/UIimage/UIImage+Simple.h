//
//  UIImage+Simple.h
//  wgc_ios
//
//  Created by 何助金 on 15/4/26.
//  Copyright (c) 2015年 Shenzhen Wanggouchao Technology Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Simple)
+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size andRoundSize:(CGFloat)roundSize;
- (UIImage *)scaleDown:(CGFloat)scale;
- (UIImage *)reverseAlpha:(UIColor *)tintColor;
- (UIImage *)fillMaskImage;
// black and clear color, black is show, clear is masked out.
- (UIImage *)maskImage:(UIImage *)mask;

@end
