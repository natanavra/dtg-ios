//
//  ZJNoPasteTextField.m
//  Pods
//
//  Created by 何助金 on 8/2/15.
//
//

#import "ZJNoPasteTextField.h"

@implementation ZJNoPasteTextField
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    if (menuController) {
        [UIMenuController sharedMenuController].menuVisible = NO;
    }
    return NO;
}

@end
