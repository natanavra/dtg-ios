//
//  NSAttributedString+Simple.h
//  Pods
//
//  Created by 何助金 on 8/1/15.
//
//

#import <Foundation/Foundation.h>

@interface NSAttributedString (Simple)
/**
 *  返回消息 只有一个部分加粗的ATString
 *
 *  @param atString     需要加粗的String
 *  @param normalString 正常状态的String
 *  @param attri        设置项
 *  @return 返回消息用ATString
 */

+ (NSAttributedString *)attributedStringWithAtsting:(NSString *)atString withAttributes:(NSDictionary *)attri withNormalString:(NSString *)normalString;

/**
 *  返回消息 两部分加粗的ATString
 *
 *  @param atString     第一部分加粗
 *  @param otherAtsting 第二部分
 *  @param 完整的String
 *
 *  @return 返回消息用ATString
 */
+  (NSAttributedString *)attributedStringWithAtsting:(NSString *)atString withOtherAtsting:(NSString *)otherAtsting withAttributes:(NSDictionary *)attri withNormalString:(NSString *)string;
@end
