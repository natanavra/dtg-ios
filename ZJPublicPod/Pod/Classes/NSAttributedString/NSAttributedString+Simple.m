//
//  NSAttributedString+Simple.m
//  Pods
//
//  Created by 何助金 on 8/1/15.
//
//

#import "NSAttributedString+Simple.h"

@implementation NSAttributedString (Simple)



/**
 *  返回消息 只有一个部分加粗的ATString
 *
 *  @param atString     需要加粗的String
 *  @param normalString 正常状态的String
 *  @param attri        设置项
 *  @return 返回消息用ATString
 */

+ (NSAttributedString *)attributedStringWithAtsting:(NSString *)atString withAttributes:(NSDictionary *)attri withNormalString:(NSString *)normalString
{
    NSString *string = [NSString stringWithFormat:@"%@ %@",atString,normalString];
    
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:string];
    NSRange range = [string rangeOfString:atString];
    [attributeStr addAttributes:attri range:range];
//     [attributeStr addAttribute:attri range:range];
    //    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:range];
//    [attributeStr addAttribute:NSFontAttributeName value:kBoldFont(14) range:range];
    //    [attributeStr addAttribute:NSFontAttributeName value:kLightFont(14) range:NSMakeRange(range.length, string.length - range.length)];
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.lineSpacing = 2.0;
    [attributeStr addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, string.length)];
    return attributeStr;
}


/**
 *  返回消息 两部分加粗的ATString
 *
 *  @param atString     第一部分加粗
 *  @param otherAtsting 第二部分
 *  @param 完整的String
 *
 *  @return 返回消息用ATString
 */
+  (NSAttributedString *)attributedStringWithAtsting:(NSString *)atString withOtherAtsting:(NSString *)otherAtsting withAttributes:(NSDictionary *)attri withNormalString:(NSString *)string;
{
    
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:string];
    NSRange range1 = [string rangeOfString:atString];
//    [attributeStr addAttribute:NSFontAttributeName value:kBoldFont(14) range:range1];
    [attributeStr addAttributes:attri range:range1];
    NSRange range2 = [string rangeOfString:otherAtsting];
    [attributeStr addAttributes:attri range:range2];
//    [attributeStr addAttribute:NSFontAttributeName value:kBoldFont(14) range:range2];
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.lineSpacing = 2.0;
    [attributeStr addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, string.length)];
    return attributeStr;
    
}

@end
