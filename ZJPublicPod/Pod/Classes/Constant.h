//
//  Constant.h
//  Pods
//
//  Created by 何助金 on 8/2/15.
//
//

#ifndef Pods_Constant_h
#define Pods_Constant_h

#pragma mark ---------------- 屏幕适配 ------------

#define kIOSVersions [[[UIDevice currentDevice] systemVersion] floatValue] //获得iOS版本
#define kUIWindow    [[[UIApplication sharedApplication] delegate] window] //获得window
#define kUnderStatusBarStartY (kIOSVersions>=7.0 ? 20 : 0)                 //7.0以上stautsbar不占位置，内容视图的起始位置要往下20

#define kIPhone6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242,2208), [[UIScreen mainScreen] currentMode].size) : NO)

#define kIPhone4s ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640,960), [[UIScreen mainScreen] currentMode].size) : NO)

#define kIPhone5s ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640,1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define kScreenSize           [[UIScreen mainScreen] bounds].size                 //(e.g. 320,480)
#define kScreenWidth          [[UIScreen mainScreen] bounds].size.width           //(e.g. 320)
#define kScreenHeight  [[UIScreen mainScreen] bounds].size.height
#define kIOS7OffHeight (kIOSVersions>=7.0 ? 64 : 0)     //设置

#define kApplicationSize      [[UIScreen mainScreen] applicationFrame].size       //(e.g. 320,460)
#define kApplicationWidth     [[UIScreen mainScreen] applicationFrame].size.width //(e.g. 320)
#define kApplicationHeight    [[UIScreen mainScreen] applicationFrame].size.height//不包含状态bar的高度(e.g. 460)

#define kScale      kScreenWidth/375.0 //比例系数

#define kStatusBarHeight         20
#define kNavigationBarHeight     44
#define kNavigationheightForIOS7 64
#define kContentHeight           (kApplicationHeight - kNavigationBarHeight)
#define kTabBarHeight            49
#define kTableRowTitleSize       14
#define maxPopLength             215





#pragma mark ---------------- 第三方函数 ------------
//比较字符串是否相等（忽略大小写），相等的话返回YES，否则返回NO。
#define kCompareStringCaseInsenstive(thing1, thing2) [thing1 compare:thing2 options:NSCaseInsensitiveSearch|NSNumericSearch] == NSOrderedSame
#define kCenterTheView(view) view.center = CGPointMake(kScreenWidth / 2.0, view.center.y)  //设置x方向屏幕居中


#pragma mark ----------------颜色 ------------
#define kRGB(r, g, b)             [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]
#define kRGBAlpha(r, g, b, a)     [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:(a)]

#define kHexRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define kHexRGBAlpha(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:(a)]

//公共顔色值
#define kWhiteColor kHexRGB(0xFFFFFF)
//holdPlacer字体颜色
#define kHoldPlacerColor kRGB(166.0, 166.0, 166.0)


#pragma mark -  定义字体方式
#define kFont(kSize)     [UIFont systemFontOfSize:kSize]
#define kBoldFont(kSize) [UIFont boldSystemFontOfSize:kSize]
//[UIFont fontWithName:@"Helvetica" size:kSize]
//#define kRegularFont(kSize) [UIFont fontWithName:@"Helvetica Regular" size:kSize]
//#define kBoldFont(kSize) [UIFont fontWithName:@"Helvetica Bold" size:kSize]
//#define kLightFont(kSize) [UIFont fontWithName:@"Helvetica Light" size:kSize]
//
//#define kStatementFontSize    [UIFont systemFontOfSize:12]            //陈述字体大小
//#define kTextFontSize         [UIFont systemFontOfSize:16]            //正文字体大小
//#define kButtonFontSize       [UIFont systemFontOfSize:19]            //按钮字体大小
//#define kTitleFontSize          kLightFont(12)    //输入框标题字体
//#define kTextFeildFontSize      kLightFont(16)     //输入框内字体大小




//默认图片占位
#define kDefaultPlaceholderImage      @"DefaultPlaceholderImage"

#define kLastHomePageInfo             @"LastHomePageInfo"

#define kLastLoginUserInfo            @"LastLoginUserInfo"


//用于判断是否第一次使用
#define kFIRST_TIME_USE  [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

//当前版本Version
#define kShotVersion  [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
//或者[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];

//Build
#define kVersionCode  [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]


#define kUserDef [NSUserDefaults standardUserDefaults]


#pragma mark ---------------- 枚举、类型------------
/*
 接收、发送、更新数据
 */
typedef enum{
    kAPI_GET,       //get data from sever
    kAPI_POST,      //post data to sever
    kAPI_PUT,       //update the data to sever
    kAPI_DELETE     //delete
}kAPI_PROTO;

#endif
