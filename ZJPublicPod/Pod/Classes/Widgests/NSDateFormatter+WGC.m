//
//  NSDateFormatter+WGC.m
//  wgc_ios
//
//  Created by Gary on 14/12/22.
//  Copyright (c) 2014年 Shenzhen Wanggouchao Technology Co., Ltd. All rights reserved.
//

#import "NSDateFormatter+WGC.h"

@implementation NSDateFormatter (WGC)

+ (NSDateFormatter *)shortFormatter {
    static dispatch_once_t onceToken;
    static NSDateFormatter *fmt = nil;
    dispatch_once(&onceToken, ^{
        fmt = [[NSDateFormatter alloc] init];
        fmt.timeZone = [NSTimeZone systemTimeZone];
        fmt.dateFormat = @"yyyy.MM.dd";
    });
    return fmt;
}

+ (NSDateFormatter *)defaultFormatter {
    static dispatch_once_t onceToken;
    static NSDateFormatter *dateFormat = nil;
    dispatch_once(&onceToken, ^{
        dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy'-'MM'-'dd HH':'mm':'ss Z"];
    });
    return dateFormat;
}

@end
