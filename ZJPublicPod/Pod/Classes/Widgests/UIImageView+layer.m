//
//  UIImageView+layer.m
//  Payou
//
//  Created by 何助金 on 8/6/15.
//  Copyright (c) 2015 MissionSky. All rights reserved.
//

#import "UIImageView+layer.h"

@implementation UIImageView (layer)
+ (UIImageView *)returnImageViewWithFrame:(CGRect)frame withImage:(UIImage *)image withCornerRadius:(float)radius withBorderWidth:(float)borderWidth withBorderColor:(CGColorRef)color withLayerBackgroudColor:(CGColorRef) bgColor{
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:frame];
    if (image) {
        imageView.image = image;
    }
    imageView.layer.cornerRadius = radius;
    if (color) {
        imageView.layer.backgroundColor = color;

    }else{
        imageView.layer.backgroundColor = [UIColor clearColor].CGColor;

    }
    
    if (bgColor) {
        imageView.layer.borderColor = bgColor;
    }else
    {
        imageView.layer.borderColor = [UIColor clearColor].CGColor;
    }
    
    imageView.layer.borderWidth = borderWidth;
    imageView.layer.masksToBounds = YES;
    return imageView;
}
@end
