//
//  NSDictionary+SaferFromNet.h
//  wgc_ios
//
//  Created by 何助金 on 15/4/26.
//  Copyright (c) 2015年 Shenzhen Wanggouchao Technology Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (SaferFromNet)
- (NSString*) getString:(NSString*)name;
- (BOOL) getBool:(NSString*)name;
- (int) getInt:(NSString*)name;
- (NSInteger) getInteger:(NSString*)name;
- (double) getDouble:(NSString*)name;
- (NSDictionary*) getDictionaryObj:(NSString*)name;
- (NSArray*) getArray:(NSString*)name;
- (NSDate *) getDate:(NSString *)name;
@end
