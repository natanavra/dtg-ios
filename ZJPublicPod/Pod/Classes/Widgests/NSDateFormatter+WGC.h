//
//  NSDateFormatter+WGC.h
//  wgc_ios
//
//  Created by Gary on 14/12/22.
//  Copyright (c) 2014年 Shenzhen Wanggouchao Technology Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (WGC)

+ (NSDateFormatter *)shortFormatter;
+ (NSDateFormatter *)defaultFormatter;

@end
