//
//  UIImageView+layer.h
//  Payou
//
//  Created by 何助金 on 8/6/15.
//  Copyright (c) 2015 MissionSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (layer)
+ (UIImageView *)returnImageViewWithFrame:(CGRect)frame withImage:(UIImage *)image withCornerRadius:(float)radius withBorderWidth:(float)borderWidth withBorderColor:(CGColorRef)color withLayerBackgroudColor:(CGColorRef) bgColor;
@end
