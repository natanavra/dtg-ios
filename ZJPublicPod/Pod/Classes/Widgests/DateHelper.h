//
//  DateHelper.h
//  CodeOfficer
//
//  Created by li carver on 6/4/13.
//  Copyright (c) 2013 Achievo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateHelper : NSObject
#pragma mark 获得时间戳
+ (NSString *)timeSpFromDate:(NSDate *)date;//获得时间戳
#pragma mark 时间戳格式化年月
+ (NSString *)timeSPToTimeStringWithString:(NSString *)timeSpString;//时间戳格式化年月

+ (NSString *)convertDateToUIDateTimeString:(NSDate *)date;
+ (NSString *)convertDateToUIDateString:(NSDate *)date;
+ (NSString *)convertDateToUITimeString:(NSDate *)date;

+ (NSString *)convertStringToUIDateTimeString:(NSString *)dateString;
+ (NSString *)convertStringToUIDateString:(NSString *)dateString;
+ (NSString *)convertStringToUITimeString:(NSString *)dateString;

+ (NSDate *)convertToUIDateTime:(NSString *)dateString;
+ (NSDate *)convertToUIDate:(NSString *)dateString;
+ (NSDate *)convertToUITime:(NSString *)dateString;

+ (NSString *)convertToFormateDateTimeString:(NSDate *)date dateFormat:(NSString *)dateFormat;  //if dateFormat is nil, default is yyyy-MM-dd hh:mm:ss
+ (NSString *)convertToFormateDateString:(NSDate *)date dateFormat:(NSString *)dateFormat;  //if dateFormat is nil, default is yyyy-MM-dd
+ (NSString *)convertToFormateTimeString:(NSDate *)date dateFormat:(NSString *)dateFormat;  //if dateFormat is nil, default is hh:mm:ss

+ (NSString *)convertStringToFormateDateTimeString:(NSString *)dateString dateFormat:(NSString *)dateFormat;  //if dateFormat is nil, default is yyyy-MM-dd hh:mm:ss
+ (NSString *)convertStringToFormateDateString:(NSString *)dateString dateFormat:(NSString *)dateFormat;  //if dateFormat is nil, default is yyyy-MM-dd
+ (NSString *)convertStringToFormateTimeString:(NSString *)dateString dateFormat:(NSString *)dateFormat;  //if dateFormat is nil, default is hh:mm:ss

@end
