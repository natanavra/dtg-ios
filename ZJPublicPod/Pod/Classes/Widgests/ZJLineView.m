//
//  ZJLineView.m
//  Pods
//
//  Created by 何助金 on 8/2/15.
//
//

#import "ZJLineView.h"
#import "Constant.h"
#import "UIView_extra.h"
#import "UIImage+Simple.h"
@implementation ZJLineView
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setDefaultImage];
        
    }
    
    return self;
}


- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setDefaultImage];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setDefaultImage];
        
    }
    
    return self;
}

- (void)setDefaultImage
{
    self.contentMode = UIViewContentModeScaleToFill;
    self.image = [UIImage imageNamed:@"separatorLine"];
    //self.height = 1.0;
    
}

+ (void)addHightLightLineViewAtTopForView:(UIView *)parantView
{
    ZJLineView *line = [[ZJLineView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 1)];
    line.image = [UIImage imageNamed:@"hightlight_line"];
    [parantView addSubview:line];
    line.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    
}

+ (void)addDarkLineViewAtButtomForView:(UIView *)parantView
{
    ZJLineView *line = [[ZJLineView alloc] initWithFrame:CGRectMake(0, parantView.height-0.5, kScreenWidth, 0.5)];
    line.image = [UIImage imageNamed:@"dark_line"];
    [parantView addSubview:line];
    line.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
}

+ (void)addLineViewAtTopForView:(UIView *)parantView
{
    ZJLineView *line = [[ZJLineView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 1)];
    [parantView addSubview:line];
    line.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    
}

+ (void)addLineViewAtBottomForView:(UIView *)parantView
{
    ZJLineView *line = [[ZJLineView alloc] initWithFrame:CGRectMake(0, parantView.height-1, kScreenWidth, 1)];
    
    [parantView addSubview:line];
    line.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
}

+ (void)addLineViewAtTopForView:(UIView *)parantView withColor:(UIColor *)color withHeight:(CGFloat)height
{
    ZJLineView *line = [[ZJLineView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, height)];
    line.image = [UIImage imageWithColor:color];
    [parantView addSubview:line];
    line.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    
}

+ (void)addLineViewAtBottomForView:(UIView *)parantView withColor:(UIColor *)color withHeight:(CGFloat)height
{
    ZJLineView *line = [[ZJLineView alloc] initWithFrame:CGRectMake(0, parantView.height - height, kScreenWidth, height)];
    line.image = [UIImage imageWithColor:color];
    [parantView addSubview:line];
    line.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
}

+ (void)addLineViewAtLeftForView:(UIView *)parantView withColor:(UIColor *)color withWidth:(CGFloat)width
{
    ZJLineView *line = [[ZJLineView alloc] initWithFrame:CGRectMake(0, 0, width, parantView.height)];
    line.image = [UIImage imageWithColor:color];
    [parantView addSubview:line];
    line.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
}

+ (void)addLineViewAtRightForView:(UIView *)parantView withColor:(UIColor *)color withWidth:(CGFloat)width
{
    ZJLineView *line = [[ZJLineView alloc] initWithFrame:CGRectMake(parantView.right - width, 0, width, parantView.height)];
    line.image = [UIImage imageWithColor:color];
    [parantView addSubview:line];
    line.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
}

+ (UIView *)returnLineViewWithColor:(UIColor *)color withSize:(CGSize )size
{
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    line.backgroundColor = color;
    return line;
}


@end
