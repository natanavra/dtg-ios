//
//  NSDictionary+SaferFromNet.m
//  wgc_ios
//
//  Created by 何助金 on 15/4/26.
//  Copyright (c) 2015年 Shenzhen Wanggouchao Technology Co., Ltd. All rights reserved.
//
#import "NSDateFormatter+WGC.h"

#import "NSDictionary+SaferFromNet.h"

@implementation NSDictionary (SaferFromNet)

- (NSString*) getString:(NSString*)name{
    
    id a = [self objectForKey:name];
    if ((__bridge CFNullRef)a == kCFNull)
        return nil;
    if (![a isKindOfClass:[NSString class]]) {
        return [a description];
    }
    return (NSString*) a;
}

- (BOOL) getBool:(NSString*)name{
    id a = [self objectForKey:name];
    if ((__bridge CFNullRef)a == kCFNull) {
        return false;
    }
    return [a boolValue];
}

- (int) getInt:(NSString*)name{
    id a = [self objectForKey:name];
    if ((__bridge CFNullRef)a == kCFNull) {
        return 0;
    }
    return [a intValue];
}

- (NSInteger) getInteger:(NSString*)name
{
    id a = [self objectForKey:name];
    if ((__bridge CFNullRef)a == kCFNull) {
        return 0;
    }
    return [a integerValue];
}

- (double) getDouble:(NSString*)name{
    id a = [self objectForKey:name];
    if ((__bridge CFNullRef)a == kCFNull) {
        return false;
    }
    return [a doubleValue];
}

- (NSDictionary*) getDictionaryObj:(NSString*)name{
    
    id a = [self objectForKey:name];
    if ((__bridge CFNullRef)a == kCFNull)
        return nil;
    return (NSDictionary*) a;
}

- (NSArray*) getArray:(NSString*)name{
    id a = [self objectForKey:name];
    if ((__bridge CFNullRef)a == kCFNull)
        return nil;
    return (NSArray*) a;
}

- (NSDate*) getDate:(NSString *)name{
    NSString * str = [self getString:name];
    if (!str)
        return nil;
    NSDate *date = [[NSDateFormatter defaultFormatter] dateFromString:str];
    return date;
}
@end
