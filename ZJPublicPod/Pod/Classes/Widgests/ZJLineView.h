//
//  ZJLineView.h
//  Pods
//
//  Created by 何助金 on 8/2/15.
//
//

#import <UIKit/UIKit.h>

@interface ZJLineView : UIImageView
+ (void)addDarkLineViewAtButtomForView:(UIView *)parantView;

+ (void)addLineViewAtTopForView:(UIView *)parantView;

+ (void)addLineViewAtBottomForView:(UIView *)parantView;

//设置颜色线条 add top bottom
+ (void)addLineViewAtTopForView:(UIView *)parantView withColor:(UIColor*)color withHeight:(CGFloat)height;

+ (void)addLineViewAtBottomForView:(UIView *)parantView withColor:(UIColor*)color withHeight:(CGFloat)height;

+ (void)addLineViewAtRightForView:(UIView *)parantView withColor:(UIColor *)color withWidth:(CGFloat)width;

+ (void)addLineViewAtLeftForView:(UIView *)parantView withColor:(UIColor *)color withWidth:(CGFloat)width;
/**
 *  生成一条线
 *
 *  @param color <#color description#>
 *  @param size  size description
 *
 */
+ (UIView *)returnLineViewWithColor:(UIColor *)color withSize:(CGSize )size;
@end
