//
//  DateHelper.m
//  CodeOfficer
//
//  Created by li carver on 6/4/13.
//  Copyright (c) 2013 Achievo. All rights reserved.
//

#import "DateHelper.h"

#define DATE_TIME_FORMATTER @"yyyy-MM-dd HH:mm:ss"
#define DATE_FORMATTER @"yyyy-MM-dd"
#define TIME_FORMATTER @"HH:mm:ss"
#define TIME_12HOURS_FORMATTER @"HH:mm"

@implementation DateHelper

+ (NSString *)timeSpFromDate:(NSDate *)date{
    return [NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]];
}

+ (NSString *)timeSPToTimeStringWithString:(NSString *)timeSpString
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timeSpString integerValue]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd MM,yyyy"];
    return [formatter stringFromDate:date];
    
}

#pragma mark - NSDate to date string
+ (NSString *)convertDateToUIDateTimeString:(NSDate *)date
{
    NSString *uiDateString = @"";
    
    if (date == nil) {
        return uiDateString;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    
    uiDateString = [dateFormatter stringFromDate:date];
    return uiDateString;
}

+ (NSString *)convertDateToUIDateString:(NSDate *)date
{
    NSString *uiDateString = @"";
    
    if (date == nil) {
        return uiDateString;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    
    uiDateString = [dateFormatter stringFromDate:date];
    
    return uiDateString;
}

+ (NSString *)convertDateToUITimeString:(NSDate *)date
{
    NSString *uiDateString = @"";
    
    if (date == nil) {
        return uiDateString;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    
    uiDateString = [dateFormatter stringFromDate:date];
    
    return uiDateString;
}

+ (NSString *)convertStringToUIDateTimeString:(NSString *)dateString
{
    NSDate *date = [self convertToUIDateTime:dateString];
    return [self convertDateToUIDateTimeString:date];
}

+ (NSString *)convertStringToUIDateString:(NSString *)dateString
{
    NSDate *date = [self convertToUIDate:dateString];
    return [self convertDateToUIDateString:date];
}

+ (NSString *)convertStringToUITimeString:(NSString *)dateString
{
    NSDate *date = [self convertToUITime:dateString];
    return [self convertDateToUITimeString:date];
}

#pragma mark - string to NSDate
+ (NSDate *)convertToUIDateTime:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    if (date == nil) {
        NSDateFormatter *dateFormatterUI = [[NSDateFormatter alloc]init];
        [dateFormatterUI setDateFormat:DATE_TIME_FORMATTER];
        date = [dateFormatterUI dateFromString:dateString];
    }
    
    return date;
}

+ (NSDate *)convertToUIDate:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    if (date == nil) {
        NSDateFormatter *dateFormatterUI = [[NSDateFormatter alloc]init];
        [dateFormatterUI setDateFormat:DATE_FORMATTER];
        date = [dateFormatterUI dateFromString:dateString];
    }
    
    return date;
}

+ (NSDate *)convertToUITime:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    if (date == nil) {
        NSDateFormatter *dateFormatterUI = [[NSDateFormatter alloc]init];
        [dateFormatterUI setDateFormat:TIME_FORMATTER];
        date = [dateFormatterUI dateFromString:dateString];
        
        if (date == nil) {
            [dateFormatterUI setDateFormat:TIME_12HOURS_FORMATTER];
            date = [dateFormatterUI dateFromString:dateString];
        }
    }
    
    return date;
}

+ (NSString *)convertToFormateDateTimeString:(NSDate *)date dateFormat:(NSString *)dateFormat
{
    if (date == nil) {
        return nil;
    }
    
    NSString *resultString = @"";
    
    if (dateFormat == nil) {
        dateFormat = DATE_TIME_FORMATTER;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setLocale:[[NSLocale  alloc]initWithLocaleIdentifier:@"en_US"]];
    [dateFormatter setDateFormat:dateFormat];
    resultString = [dateFormatter stringFromDate:date];
    
    return resultString;
}

+ (NSString *)convertToFormateDateString:(NSDate *)date dateFormat:(NSString *)dateFormat
{
    if (date == nil) {
        return nil;
    }
    
    NSString *resultString = @"";
    
    if (dateFormat == nil) {
        dateFormat = DATE_FORMATTER;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setLocale:[[NSLocale  alloc]initWithLocaleIdentifier:@"en_US"]];
    [dateFormatter setDateFormat:dateFormat];
    resultString = [dateFormatter stringFromDate:date];
    
    return resultString;
}

+ (NSString *)convertToFormateTimeString:(NSDate *)date dateFormat:(NSString *)dateFormat
{
    if (date == nil) {
        return nil;
    }
    
    NSString *resultString = @"";
    
    if (dateFormat == nil) {
        dateFormat = TIME_FORMATTER;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setLocale:[[NSLocale  alloc]initWithLocaleIdentifier:@"en_US"]];
    [dateFormatter setDateFormat:dateFormat];
    resultString = [dateFormatter stringFromDate:date];
    
    return resultString;
}

+ (NSString *)convertStringToFormateDateTimeString:(NSString *)dateString dateFormat:(NSString *)dateFormat
{
    NSString *resultString = @"";
    
    NSDate *date = [self convertToUIDateTime:dateString];
    resultString = [self convertToFormateDateTimeString:date dateFormat:dateFormat];
    return resultString;
}

+ (NSString *)convertStringToFormateDateString:(NSString *)dateString dateFormat:(NSString *)dateFormat
{
    NSString *resultString = @"";
    
    NSDate *date = [self convertToUIDate:dateString];
    resultString = [self convertToFormateDateString:date dateFormat:dateFormat];
    return resultString;
}

+ (NSString *)convertStringToFormateTimeString:(NSString *)dateString dateFormat:(NSString *)dateFormat
{
    NSString *resultString = @"";
    
    NSDate *date = [self convertToUITime:dateString];
    resultString = [self convertToFormateTimeString:date dateFormat:dateFormat];
    return resultString;
}

@end
